The VLinde Library
==================

What is it?
-----------

The VLinde library is a collection of components for the C++11
language. Some of these components already have equivalents. For
instance, `vlinde::data::maybe` is essentially the same as
`boost::optional`. Other components try to be at least somewhat unique
by providing a more powerful interface, a cleaner or faster
implementation, or just by being new. Given the youth of the new C++
language, there are still many libraries to be written in modern C++
style!

Who is it for?
--------------

It isn't really for anyone in particular. Mostly, it's just some
tinkering with code that looks like it would be useful. Individual
projects will likely appeal to certain task (e.g., `vlinde::thread` is
useful for providing synchronization primitives not provided by the
standard library).

Where is it?
------------

The library is hosted as a Git repository on
[Bitbucket](https://bitbucket.org/kwvanderlinde/vlinde). Library
versions are exposed as tags, which can be accessed via Git, or from the
[Downloads page](https://bitbucket.org/kwvanderlinde/vlinde/downloads).

Documentation
-------------

A fixed location for documentation is not available yet. Hopefully the
Wiki page on the Bitbucket will become populated with something useful
soon. In the interim, Doxygen generated documentation can be built by
checking out the source code and running `scons --enable-docs` at the
source's root directory.

Installation
------------

Since the VLinde library is a header only library for the moment, no
special build action is required. Installation is also not supported,
though it will be in the near future. The library can be installed
manually be copying the `./include/vlinde/` directory into
`/usr/local/include` or some other directory on the system include
path.