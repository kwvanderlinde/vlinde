// TODO We need ranges which can fill a container, like
// ::std::back_inserter does.

#include <vlinde/algorithm.hpp>
#include <vlinde/concepts.hpp>
#include <vlinde/functional.hpp>
#include <vlinde/range/iterator_range.hpp>
#include <vlinde/range.hpp>

#include <vlinde/range/repeat.hpp>

#include <cassert>
#include <forward_list>
#include <iostream>
#include <iterator>
#include <random>
#include <typeinfo>
#include <vector>

using namespace vlinde::concepts;
using namespace vlinde::range;
using namespace vlinde::tmp;

template <typename It, typename Distance>
void my_advance_impl(InputIterator, It & it, Distance n)
{
	::std::cout << "Slow!" << ::std::endl;

	assert(n >= 0);

	while (n-- != 0) {
		++it;
	}
}

template <typename It, typename Distance>
void my_advance_impl(BidirectionalIterator, It & it, Distance n)
{
	::std::cout << "Two-way! (but still slow)" << ::std::endl;
	if (n < 0) {
		while (n++ != 0) {
			--it;
		}
	} else {
		while (n-- != 0) {
			++it;
		}
	}
}

template <typename It, typename Distance>
void my_advance_impl(RandomAccessIterator, It & it, Distance n)
{
	::std::cout << "Fast!" << ::std::endl;
	it += n;
}

template <typename It, typename Distance>
void my_advance(It & it, Distance n)
{
	my_advance_impl(most_refined<It, RandomAccessIterator>{}, it, n);
}

template <typename Container>
iterator_range<typename Container::iterator> range(Container & container)
{
	return {container.begin(), container.end()};
}

template <typename Container>
iterator_range<typename Container::const_iterator>
range(Container const & container)
{
	return {container.cbegin(), container.cend()};
}

template <typename T>
struct handle
{
	T * m_t;

	handle(T & t)
	: m_t{::std::addressof(t)}
	{
	}

	operator T() const
	{
		return *m_t;
	}

	template <typename U>
	handle & operator=(U && value)
	{
		*m_t = ::std::forward<U>(value);
	}
};

struct TestRange
{
	using reference = handle<int>;
	using index_type = ::std::size_t;
	using value_type = int;

	int * m_first;
	int * m_last;

	bool empty() const
	{
		return m_first == m_last;
	}

	void pop_front()
	{
		assert(!this->empty());

		++m_first;
	}

	reference front() const
	{
		return *m_first;
	}

	void pop_back()
	{
		assert(!this->empty());

		--m_last;
	}

	reference back() const
	{
		return *m_last;
	}

	reference operator[](index_type i) const
	{
		assert(i < this->size());

		return m_first[i];
	}

	TestRange slice(index_type i, index_type j) const
	{
		assert(i <= j && i <= size() && j <= size());

		return TestRange{m_first + i, m_first + j};
	}

	index_type size() const
	{
		return static_cast<index_type>(m_last - m_first);
	}
};

#define STR(tok) #tok

#define CONCEPT_ASSERT(concept, type)                                          \
	static_assert(models<concept, type>::value,                                  \
	              STR(type) " must be a " STR(concept))

CONCEPT_ASSERT(vlinde::range::Range, TestRange);
CONCEPT_ASSERT(vlinde::range::MultiPassRange, TestRange);
CONCEPT_ASSERT(vlinde::range::ForwardRange, TestRange);
CONCEPT_ASSERT(vlinde::range::BackwardRange, TestRange);
CONCEPT_ASSERT(vlinde::range::BidirectionalRange, TestRange);
CONCEPT_ASSERT(vlinde::range::RandomAccessRange, TestRange);
CONCEPT_ASSERT(vlinde::range::SlicableRange, TestRange);
CONCEPT_ASSERT(vlinde::range::SizedRange, TestRange);
CONCEPT_ASSERT(vlinde::range::ReadableRange, TestRange);

static_assert(models<vlinde::range::WritableRange, TestRange, int>::value,
              "TestRange must be a WritableRange for int");

// Use this constexpr stuff to define an infinite range.

struct Test
{
	static constexpr bool empty()
	{
		return true;
	}
};

struct Test2
{
	static bool empty()
	{
		return true;
	}
};

template <typename T>
using result = ::std::integral_constant<bool, T::empty()>;

template <int>
using sfinae_true = std::true_type;

template <typename T>
auto is_constant_test(int) -> decltype(result<T>{}, ::std::true_type{});

template <typename>
auto is_constant_test(...) -> ::std::false_type;

template <typename T>
using is_constexpr_empty = decltype(is_constant_test<T>(0));

int main()
{
	using ::vlinde::algorithm::for_each;

	{
		Test t;
		static_assert(is_constexpr_empty<Test>::value, "No!");

		Test2 t2;
		static_assert(!is_constexpr_empty<Test2>::value, "No!");
	}

	{
		// Apparently lambdas aren't copyable!
		struct is_even
		{
			bool operator()(int i) const noexcept
			{
				return i % 2 == 0;
			}
		};

		auto filter_test = filter(iota(10), is_even{});
		using filter_range = decltype(filter_test);

		CONCEPT_ASSERT(Range, filter_range);
		CONCEPT_ASSERT(MultiPassRange, filter_range);
		CONCEPT_ASSERT(ForwardRange, filter_range);
		CONCEPT_ASSERT(BackwardRange, filter_range);
		CONCEPT_ASSERT(BidirectionalRange, filter_range);
		CONCEPT_ASSERT(ReadableRange, filter_range);

		::std::cout << "filter test" << ::std::endl;

		for_each(filter_test,
		         [](filter_range::value_type i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;

		// And backward!
		for_each(retro(filter_test),
		         [](filter_range::value_type i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;

		::std::cout << ::std::endl;
	}

	{
		auto iota_test = iota(30, 10, -1);
		using iota_range = decltype(iota_test);

		CONCEPT_ASSERT(Range, iota_range);
		CONCEPT_ASSERT(MultiPassRange, iota_range);
		CONCEPT_ASSERT(ForwardRange, iota_range);
		CONCEPT_ASSERT(BackwardRange, iota_range);
		CONCEPT_ASSERT(BidirectionalRange, iota_range);
		CONCEPT_ASSERT(RandomAccessRange, iota_range);
		CONCEPT_ASSERT(SlicableRange, iota_range);
		CONCEPT_ASSERT(SizedRange, iota_range);
		CONCEPT_ASSERT(ReadableRange, iota_range);

		::std::cout << "iota test" << ::std::endl;

		for_each(iota_test,
		         [](iota_range::value_type i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;

		// And backward!
		for_each(retro(iota_test),
		         [](iota_range::value_type i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;

		::std::cout << ::std::endl;
	}

	{
		::std::cout << "iota unsigned test" << ::std::endl;

		auto range = iota(::std::size_t{3}, ::std::size_t{0});
		for_each(range, [](::std::size_t i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		::std::cout << "TestRange test" << ::std::endl;

		int array[6] = {0, 1, 2, 3, 4, 5};
		TestRange test{array, array + 6};
		for_each(test, [](int i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		::std::vector<int> ints{0, 1, 2, 3, 4, 5};
		auto it = ints.begin();
		my_advance(it, 3);
		assert(*it == 3);

		std::forward_list<int> more_ints{0, 1, 2, 3, 4, 5};
		auto it2 = more_ints.begin();
		my_advance(it2, 3);
		assert(*it == 3);
	}

	{
		// using iterator = ::std::vector<int>::iterator;
		using iterator = int *;
		static_assert(models<RandomAccessIterator, iterator>::value, "Surprise!");

		using best = most_refined<iterator, RandomAccessIterator>;
		static_assert(::std::is_same<best, RandomAccessIterator>::value,
		              "Surprise!");

		using forward = ::std::forward_list<int>::iterator;
		using forward_best = most_refined<forward, RandomAccessIterator>;
		::std::cout << typeid(forward_best).name();
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		::std::cout << "repeat slice test" << ::std::endl;

		auto repeat_test = repeat('H').slice(0, 20);
		//::std::cout << repeat_test.size() << ::std::endl;
		for_each(::std::move(repeat_test), [](char i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;

		// And backward!
		for_each(retro(repeat_test), [](char i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;
		::std::cout << ::std::endl;
	}

	{
		::std::cout << "repeat bound test" << ::std::endl;

		auto repeat_test = repeat('H', 20);
		using repeat_range = decltype(repeat_test);

		CONCEPT_ASSERT(Range, repeat_range);
		CONCEPT_ASSERT(MultiPassRange, repeat_range);
		CONCEPT_ASSERT(ForwardRange, repeat_range);
		CONCEPT_ASSERT(BidirectionalRange, repeat_range);
		CONCEPT_ASSERT(RandomAccessRange, repeat_range);
		CONCEPT_ASSERT(SlicableRange, repeat_range);
		CONCEPT_ASSERT(SizedRange, repeat_range);
		CONCEPT_ASSERT(ReadableRange, repeat_range);

		::std::cout << repeat_test.size() << ::std::endl;
		for_each(repeat_test, [](char c) { ::std::cout << c << " "; });
		::std::cout << ::std::endl;
		for (repeat_range::index_type i = 0; i < repeat_test.size(); ++i) {
			::std::cout << repeat_test[i] << " ";
		}
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		auto repeat_test = repeat('H');
		using repeat_range = decltype(repeat_test);

		CONCEPT_ASSERT(Range, repeat_range);
		CONCEPT_ASSERT(MultiPassRange, repeat_range);
		CONCEPT_ASSERT(InfiniteRange, repeat_range);
		CONCEPT_ASSERT(ForwardRange, repeat_range);
		CONCEPT_ASSERT(RandomAccessRange, repeat_range);
		CONCEPT_ASSERT(SlicableRange, repeat_range);
		CONCEPT_ASSERT(ReadableRange, repeat_range);
	}

	{
		::std::cout << "take test" << ::std::endl;

		auto take_test = take(iota(10), 5);
		using take_range = decltype(take_test);

		::std::cout << typeid(take_range).name() << ::std::endl;
		auto take_test2 = take(take_test, 3);
		::std::cout << typeid(take_test2).name() << ::std::endl;
		{
			using ::vlinde::range::detail::iota_range;
			using ::vlinde::range::detail::take_range;
			using test_type = take_range<take_range<iota_range<int, int>, int>, int>;
			::std::cout << typeid(test_type).name() << ::std::endl;
		}

		CONCEPT_ASSERT(Range, take_range);
		CONCEPT_ASSERT(MultiPassRange, take_range);
		CONCEPT_ASSERT(ForwardRange, take_range);
		CONCEPT_ASSERT(RandomAccessRange, take_range);
		CONCEPT_ASSERT(SlicableRange, take_range);
		CONCEPT_ASSERT(SizedRange, take_range);
		CONCEPT_ASSERT(ReadableRange, take_range);

		::std::cout << take_test.size() << ::std::endl;
		for_each(take_test, [](int i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;
		for (take_range::index_type i = 0; i < take_test.size(); ++i) {
			::std::cout << take_test[i] << " ";
		}
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		using ::vlinde::algorithm::copy;
		using ::vlinde::algorithm::shuffle;

		::std::cout << "shuffle test" << ::std::endl;

		::std::mt19937 generator{::std::random_device{}()};

		::std::vector<int> ints(20);
		copy(iota(ints.size()), range(ints));

		for_each(range(ints), [](int i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl;

		shuffle(range(ints), generator);

		for_each(range(ints), [](int i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		::std::cout << "generator test" << ::std::endl;

		::std::mt19937 twister{::std::random_device{}()};

		auto range = ::vlinde::range::generator(twister);
		for_each(take(::std::move(range), 50),
		         [](::std::size_t i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl << ::std::endl;
	}

	{
		::std::cout << "Random Range test" << ::std::endl;

		::std::mt19937 twister{::std::random_device{}()};

		auto range = ::vlinde::range::random_range(twister);
		for_each(take(::std::move(range), 50),
		         [](::std::size_t i) { ::std::cout << i << " "; });
		::std::cout << ::std::endl << ::std::endl;
	}

	::std::mt19937 generator{::std::random_device{}()};

	using range_type = decltype(iota(50));

	::std::vector<int> ints(20);
	::std::vector<int> const const_ints = ints;

	using ints_range_type = decltype(range(const_ints));
	static_assert(::std::is_same<ints_range_type::reference, int const &>::value,
	              "Missed it!");

	return 0;
}
