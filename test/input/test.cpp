#include "vlinde/input/abstraction.hpp"

#include <linux/input.h>

#include <fstream>
#include <iostream>

int main()
{
	using namespace vlinde::input::abstraction;

	// input_reader ir;
	bool running = true;

	while (running) {
		// auto events = ir.tick();
		::std::initializer_list<event> events = {};

		for (auto event : events) {
			std::cout << "Device: " << event.source.device << std::endl
			          << "\tElement: " << event.source.number
			          << std::endl;
			switch (event.source.type) {
			case event_type::button:
				std::cout << "\tType: button" << std::endl
				          << "\tPressed: "
				          << (event.button_state ==
				                      button_state::pressed
				                  ? "Yes"
				                  : "No") << std::endl;
				if (event.source.number == 316u &&
				    event.button_state == button_state::pressed)
					running = false;
				break;
			case event_type::absolute_axis:
				std::cout << "\tType: absolute" << std::endl
				          << "\tValue: " << event.axis_position
				          << std::endl;
				break;
			case event_type::relative_axis:
				std::cout << "\tType: absolute" << std::endl
				          << "\tValue: " << event.axis_position
				          << std::endl;
				break;
			case event_type::device:
				std::cout << "\tType: device" << std::endl
				          << "\tRemoved: "
				          << (event.device_state ==
				                      device_state::disconnected
				                  ? "Yes"
				                  : "No") << std::endl;
				break;
			}
		}
	}

	return 0;
}
