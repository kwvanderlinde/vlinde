#include <vlinde/thread.hpp>

#include <chrono>
#include <future>
#include <iostream>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

int main()
{
	using namespace ::vlinde::thread;

#if 0
	{
		thread_local_storage<::std::string> storage;
		::std::vector<::std::future<::std::string>> tasks;
		for (::std::size_t i = 0; i < 100; ++i) {
			tasks.push_back(::std::async(::std::launch::async, [&, i] {
				storage.get() =
				  ::std::to_string(i) + " " + ::std::to_string(i) + "\n";
				::std::this_thread::sleep_for(::std::chrono::milliseconds{50});
				return storage.get();
			}));
		}
		for (auto& task : tasks)
			std::cout << task.get() << std::endl;
		for (auto& value : storage) {
			value += "bye";
		}
		for (auto const& value : storage) {
			std::cout << value << std::endl;
		}
		return 0;
	}
#endif

	synchronized<::std::string, recursive<ticket_mutex>> my_string_{
	  "Hello\n"};
	decltype(my_string_) my_string{my_string_};
	::std::vector<::std::future<void>> tasks;

	for (::std::size_t i = 0; i < 5; ++i) {
		tasks.push_back(::std::async(::std::launch::async, [&, i] {
			my_string([i](::std::string& s) {
				::std::ostringstream oss;
				oss << ::std::this_thread::get_id();
				s += oss.str();
				s += "\n";
			});
			my_string([](::std::string& s) {
				::std::cout << s << ::std::endl;
			});
		}));
	}
	for (auto& task : tasks)
		task.wait();

	::std::cout << "Done\n";

	return 0;
}
