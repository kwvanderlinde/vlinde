
#include <vlinde/concepts.hpp>
#include <vlinde/memory/aligned_allocator.hpp>
#include <vlinde/memory/block.hpp>
#include <vlinde/memory/default_allocator.hpp>
#include <vlinde/memory/typed_memory.hpp>
#include <vlinde/algorithm.hpp>

#include <iostream>

#define STR(tok) #tok

#define CONCEPT_ASSERT(concept, type)                                          \
	static_assert(::vlinde::concepts::models<concept, type>::value,              \
	              STR(type) " must be a " STR(concept))

using namespace ::vlinde::memory;

struct allocator_test
{
	block<void> alloc(size_t) noexcept;

	block<void> aligned_alloc(size_t, size_t, size_t) noexcept;

	void free(block<void>) noexcept;

	bool operator==(allocator_test const &) const;

	bool operator!=(allocator_test const &) const;

	::std::size_t get_size(void *) const noexcept;

	block<void> resolve_internal(void *) const noexcept;

	block<void> resize(block<void>, ::std::size_t) noexcept;

	friend void swap(allocator_test &, allocator_test &) noexcept
	{
	}
};

int main()
{
	using namespace ::vlinde::memory;

	{
		default_allocator alloc;
		auto block = alloc.alloc(20 * sizeof(int));
		if (!block.get_pointer()) {
			::std::cerr << "Could not allocate memory with which to test."
			            << ::std::endl;
			return 1;
		}

		typed_memory<int> typed{block};
	}

	{
		using ::vlinde::algorithm::for_each;

		int array[100] = {};
		block<int> mb{array, 100};
		for_each(mb.range(), [](int i) { ::std::cout << i << ::std::endl; });
	}

	{
		static_assert(
		    ::std::is_same<
		        aligned_allocator<default_allocator>,
		        aligned_allocator<aligned_allocator<default_allocator>>>::value,
		    "Improper aliasing");

		CONCEPT_ASSERT(Allocator, default_allocator);

		using aligned_default_allocator = aligned_allocator<default_allocator>;
		CONCEPT_ASSERT(Allocator, aligned_default_allocator);
		static_assert(
		    allocator_traits<aligned_default_allocator>::has_aligned_alloc::value,
		    "aligned_allocator must provide aligned_alloc");
		static_assert(
		    allocator_traits<aligned_default_allocator>::has_resize::value,
		    "aligned_allocator must provide resize");

		aligned_allocator<default_allocator> allocator;

		char const * const str = "This is a longer string";
		::std::size_t const length = ::std::strlen(str) + 1;

		// Allocate without alignment requirements.

		try {
			auto buffer = allocator.alloc(length);

			// Check the size.
			assert(buffer.get_length() == length);

			// Zero fill the buffer.
			::std::memset(buffer.get_pointer(), 0, buffer.get_length());

			::std::memcpy(buffer.get_pointer(), str, length);

			allocator.free(buffer);
		} catch (::std::bad_alloc const &) {
		}

		// Barrel through a whole lot of alignments and offsets.
		::std::vector<block<void>> allocations;
		decltype(allocations)::size_type max_allocations = 3000;
		for (::std::size_t alignment = 1; alignment <= 0x8000; alignment <<= 1) {
			for (::std::size_t count = 0; count < alignment; ++count) {
				// Periodically release memory.
				if (allocations.size() > max_allocations) {
					for (block<void> block : allocations) {
						allocator.free(block);
					}
					allocations.clear();
				}

				::std::size_t const offset = count % length;
				try {
					auto buffer = allocator.aligned_alloc(length, alignment, offset);

					// Check the size and alignment requirements.
					assert(buffer.get_length() >= length);
					auto const address = reinterpret_cast<uintptr_t>(
					    reinterpret_cast<char *>(buffer.get_pointer()) + offset);
					assert(address % alignment == 0);

					// Do some writes for Valgrind to verify.

					// First, zero fill the buffer.
					::std::memset(buffer.get_pointer(), 0, buffer.get_length());

					::std::memcpy(buffer.get_pointer(), str, length);
					allocations.push_back(buffer);
				} catch (::std::bad_alloc const &) {
				}
			}
		}

		for (block<void> block : allocations) {
			allocator.free(block);
		}
	}

	{
		CONCEPT_ASSERT(Allocator, allocator_test);
		static_assert(allocator_traits<allocator_test>::has_aligned_alloc::value,
		              "allocator_test provides aligned_alloc, but it's not "
		              "recognized!");
		static_assert(allocator_traits<allocator_test>::has_resize::value,
		              "allocator_test provides resize, but it's not recognized!");
		static_assert(allocator_traits<allocator_test>::has_resolve_internal::value,
		              "allocator_test provides resolve_internal, but "
		              "it's not recognized!");
	}

	return 0;
}
