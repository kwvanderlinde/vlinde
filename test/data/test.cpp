#include <vlinde/data/maybe.hpp>
#include <vlinde/data/vector.hpp>

#include <iostream>

int main()
{
    using namespace ::vlinde::data;

    maybe<int> might_be_an_int{100};

    for (auto i : might_be_an_int) {
        std::cout << i << std::endl;
    }

    return 0;
}
