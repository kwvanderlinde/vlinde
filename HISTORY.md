Version 1.0.0
=============

Initial release. Includes the following:

- `vlinde::data::maybe`
- `vlinde::memory::make_unique`
- `vlinde::memory::scope_guard` and `SCOPE_EXIT`

Version 1.1.0
=============

Added multithreading facilities in the `vlinde::thread` namespace. This
includes the following mutex types:

- `null_mutex`: fully non-exclusive mutex.
- `spin_mutex`: a spin lock based mutex.
- `ticket_mutex`: a fair spin lock based mutex.
- `recursive`: adapts mutexes to make them recursive.

Also included are:

- `synchronized`: synchronizes access to an object.
- `thread_local_storage` - TLS as an object, not just a storage class.

Beyond the threading library, a coding style was set in place (see
`STYLE_GUIDE.md`). All existing code was updated to recognize the new
style.
