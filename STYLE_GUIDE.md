Code style guidelines
=====================

Organization
------------

Components should be separated as much as possible. Prefer generality if
it does not comprise much complexity. Prefer loosely coupled components
over tightly coupled components. Prefer placing implementation details
into a `detail` namespace rather than in the private section of a class.

The organization of files should reflect the hierarchical arrangement of
namespaces. For instance, the namespace `A::B::C` should correspond to
a directory structure with `C` as a subdirectory of `B` which is a
subdirectory of `A`. Header files should be located under the `include`
directory. As a corollary, a header file should only contain one
namespace, depending in which directory it is located.

A global components shall be a type declarations or definitions, a
function declaration or definition, or a template thereof. Variables
shall not be global, unless they are also constant.

Column limit
------------

Code shall be formatted to a 72 character column limit. For the purposes
of formatting, tabs shall be considered to be four characters in width.

Indentation
-----------

Tabs shall be used for indentation, while spaces are reserved for
other formatting. This will allow anyone to read the code with their
preferred indentation size, without ruining the layout of the code.

Access modifiers, as well `case` and `goto` labels, shall be aligned to
their parent construct rather than indented.

Namespaces should not induce indentation. This is unnecessary since each
header only defines one namespace. The nexted namespace syntax
`namespace A { namespace B { } }` should be seen as a longhand for the
non-existant, but preferred, syntax `namespace A::B { }`. Thus,
indenting each level of namespace does not make sense.

Braces
------

Brace initialization lists are formatted no differently than a
parenthesized expression with the same contents.

Braces which delimit a function block, namespace scope, or class body
shall be placed on their own line, indented the same as the enclosing
context. Code within such braces shall be indented one tab from the
column in which the braces appear.

Braces delimiting a block scope which is not associated with control
flow shall be on their own lines. If the block is associated with a
control flow construct, the opening brace shall not be placed on its own
line, but rather separated one space from the preceding token. In a
`do`/`while` loop, the `while` shall be separated one space from the
closing brace.

Initializer lists
-----------------

Constructor initializer lists should have a single newline between the
constructor prototype and the initial colon. The colon shall not be
indented. If several initializers exist, they shall each be on their own
line. The comma which separates initializers should be on the same line
as the subsequent initializer, aligned with the colon. A space shall
separate each initializer from the preceding colon or comma.

Naming conventions
------------------

Identifiers shall be written in `PascalCase` if they are template
parameters. Otherwise, identifiers shall be `snake_case`. Private static
class members shall be prefixed with `s_`. Private instance members
shall be prefixed with `m_`. Privated thread local static member shall
be prefixed with `t_`.

Documentation comments
----------------------

Documentation comments shall be delimited by an opening `/**` and a
closing `*/`. Each new line in the comment shall be prefixed with a `*`
which is aligned with the second `*` in the opening `/**`. Documentation
commands shall be predefined Doxygen commands, or those aliases which
have been added to the Doxyfile. There should be no reason to use any
other markup.

Documentation should be written as a sequence of paragraphs in Markdown
format. The first paragraph should be a short, one sentence description
of the component. The subsequent paragraphs provide a detailed
explanation.

Special paragraphs should be prefixed with a command which is on its own
line, but with no interveaning empty line. For instance, to stylize a
paragraph as a "note", the `\note` command should be written, then a
newline added on which the paragraph begin.

The namespaces with the name `detail` are documented slightly
differently. They do not need to be fully documented, though they are
allowed to be. The first paragraph of a documentation comment for a
`detail` namespace shall consists solely of the `\internal`
command. This command shall not be terminated. Since the brief paragraph
(if it exists) will therefore not be the first paragraph, it must be
prefixed with the `\brief` command. This allows Doxygen to recognize the
brief paragraph despite not being the first paragraph.
