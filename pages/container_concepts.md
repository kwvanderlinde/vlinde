Container concepts {#container_concepts_page}
==================

[TOC]

Container concepts {#container_concepts}
==================

In the following sections, let `T` denote a Destructible type, let `X`
denote a container of `T`, let `a` and `b` denote values of type `X`,
let `r` and `s` denote non-const values of type `X`, let `rv` denote a
non-const rvalue of type `X`, and let `u` be an identifier.

Note: Containers are modeled as though they are designed using the
handle-body pattern. As such, certain simple data structures such as
stack allocated arrays do not satisfy the Container concept. This
should not be a problem in practice, since the container concepts
are rarely used in generic code. Instead, ranges are used most often
since they are an abstraction above containers and data structures.

Auxiliary concepts {#auxiliary_container_concepts}
------------------

Some container operations place extra constraints on the container's
value type, or on the relationship between the container and its value
type.

Let `p` be a value of type `void*`, let `v` be an expression of type
`T`, and let `rv` be an rvalue of type `T`. Then the following
auxiliary concepts are defined:

- `T` is *Copy Insertable* into `X` if `new (p) T(v)` is well formed.
- `T` is *Move Insertable* into `X` if `new (p) T(rv)` is well formed.
- `T` is *Emplace Constructible* into `X` from `args`, for zero or more
  arguments `args`, if `new (p) T(args)` is well formed.

Container {#container}
---------

A type `X` models the *Container* concepts if `X` is destructible, and
the following types and expressions are well formed with the given
constraints.

`X` must be Copyable if `T` is copy insertable into `X`.

<table>
<tr>
    <th>
        Expression
    </th>
    <th>
        Return type
    </th>
    <th>
        Notes
    </th>
    <th>
        Complexity
    </th>
</tr>

<tr>
    <td>
        `X::value_type`
    </td>
    <td>
        `T`
    </td>
    <td></td>
    <td></td>
</tr>

<tr>
    <td>
        `X::size_type`
    </td>
    <td>
        Unsigned integral type.
    </td>
    <td>
    </td>
    <td>
        `X::size_type` can represent the number of elements in any instance of `X`.
    </td>
</tr>

<tr>
    <td>
        `X::range_type`
    </td>
    <td>
        Range type whose `value_type` is `T`. 
    </td>
    <td>
        `X::range_type` must be a Forward Readable Writable MultiPass
        Range.<br/>
        Convertible to `X::const_range_type`.
    </td>
    <td>
    </td>
</tr>

<tr>
    <td>
        `X::const_range_type`
    </td>
    <td>
        Range type whose `value_type` is (possibly `const`) `T`. 
    </td>
    <td>
        `X::const_range_type` must be a Forward Readable MultiPass
        Range.
    </td>
    <td>
    </td>
</tr>

<tr>
    <td>
        `X u;`
    </td>
    <td></td>
    <td>
        No throw.<br/>
        Post: `u.empty()`
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `X()`
    </td>
    <td></td>
    <td>
        No throw.<br/>
        Post: `X().empty()`
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `X(a)`
    </td>
    <td></td>
    <td>
        Requires: `T` is Copy Insertable into `X`<br/>
        Post: `a == X(a)`
    </td>
    <td>
        Linear
    </td>
</tr>

<tr>
    <td>
        `X u(a)`<br/>
        `X u = a`
    </td>
    <td></td>
    <td>
        Requires: `T` is Copy Insertable into `X`<br/>
        Post: `u == a`
    </td>
    <td>
        Linear
    </td>
</tr>

<tr>
    <td>
        `X u(rv)`<br/>
        `X u = rv`
    </td>
    <td></td>
    <td>
        No throw<br/>
        Post: `u` shall be equal to the value `rv` had before this construction.
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `r = a`
    </td>
    <td>
        `X&`
    </td>
    <td>
        Post: `r == a`.
    </td>
    <td>
        Linear
    </td>
</tr>

<tr>
    <td>
        `r = rv`
    </td>
    <td>
        `X&`
    </td>
    <td>
        No throw<br/>
        Post: `a` shall be equal to the value `rv` had before this assignment.
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `swap(r, s)`
    </td>
    <td>
        `X&`
    </td>
    <td>
        No throw<br/>
        Exchanges the contents of `r` and `s`.
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `::%std::addressof(a)->~X()`
    </td>
    <td>
        `void`
    </td>
    <td>
        No throw<br/>
        The destructor is applied to every element of `a`.<br/>
        All memory is freed.
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `a.empty()`
    </td>
    <td>
        `bool`
    </td>
    <td>
        No throw<br/>
        Returns: `a.range().empty()`
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `a.size()`
    </td>
    <td>
        `X::size_type`
    </td>
    <td>
        No throw<br/>
        Returns: `count(a.range())`
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `a.range()`
    </td>
    <td>
        `X::range_type`; `X::const_range_type` for constant `a`.
    </td>
    <td>
        No throw
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `a.crange()`
    </td>
    <td>
        `X::const_range_type`
    </td>
    <td>
        No throw
    </td>
    <td>
        Constant
    </td>
</tr>

<tr>
    <td>
        `a == b`
    </td>
    <td>
        `bool`
    </td>
    <td>
        No throw<br/>
        Returns: `equal(a.range(), b.range())`<br/>
        Requires: `T` is Equality Comparable
    </td>
    <td>
        Linear
    </td>
</tr>

<tr>
    <td>
        `a != b`
    </td>
    <td>
        `bool`
    </td>
    <td>
        Equivalent to `!(a == b)`
    </td>
    <td>
        Linear
    </td>
</tr>

</table>
