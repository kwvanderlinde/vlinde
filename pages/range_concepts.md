Range Concepts {#range_concepts_page}
==============

[TOC]

Range Concepts {#range_concepts}
==============

The VLinde Library Range concepts are based Alexandrescu's informIT
paper entitled "On Iteration". The paper defines ranges in a way that
decouples range traversal from element access, as in Boost's new
iterator concepts. It is also the basis for the range interfaces in D's
standard library Phobos.

The terminology used here is not equivalent to the "standard"
terminology of iterators and ranges. In particular, a "Forward Range"
does not require the multi-pass guarantee, unlike Forward Iterators in
C++, and Forward Ranges in D. Instead, we prefer a more fine-grained
approach to concepts. The end result is that we have completely
separated traversal direction, access, and multi-pass guarantees.

In the following sections, let `X` denote a range type, `a` a value of
type `X`, and `r` a non-const value of type `X`.

Some ranges support indices. These ranges define the type or alias
`X::index_type`. For these ranges, let `i` and `j` denote values of type
`I`.

Basic Range Concepts {#basic_range_concepts}
--------------------

### Range {#range}

A type `X` models the *Range* concept if `X` is Move Constructible, Move
Assignable, and the following expressions are valid with the given constraints.

| Expression     | Return type           |
|----------------|-----------------------|
| `X::reference` |                       |
| `a.empty()`    | convertible to `bool` |

Note: Adhering to the Range concept is not very useful in itself.
Rather, the Range concept provides a useful base for the other
concepts.

### Multi-Pass Range {#multi_pass_range}

A type `X` models the *Mutli-Pass Range* concept if `X` is a [Range] and
is also Copy Constructible and Copy Assignable. Additionally, `X` must
offer multi-pass semantics. That is, if `s` is a copy of `a`, then `s`
and `a` are equivalent but independent ranges.

### Sized Range {#sized_range}

A type `X` models the *Sized Range* concept if `X` is a [Range] and the
following expressions are valid with the given semantics.

<table>
<tr>
  <th>Expression</th>
  <th>Return type</th>
  <th>Conditions</th>
</tr>
<tr>
  <td>
    `X::index_type`
  </td>
  <td>
    Unsigned integral type.
  </td>
  <td>
    Capable of representing `a.size()` for any `a`.
  </td>
</tr>
<tr>
  <td>
    `a.size()`
  </td>
  <td>
    `X::index_type`
  </td>
  <td>
    `a.size() == 0` if and only if `e.empty()`
  </td>
</tr>
</table>

### Infinite Range {#infinite_range}

A type `X` models the *Infinite Range* concept if `X` is a [Range] and
the assertion `!a.empty()` is verifiable at compile time.

Note: this effectively requires the following declaration for `X::empty`:

    static constexpr bool empty()
    {
      return false;
    }

### Random Range {#random_range}

Not to be confused with [Random Access Range]

A type `X` model the *Random Range* concept if `X` is a [Readable Range]
and provides the following member functions:

| Expression | Return type     | Requirements                                                                     |
|------------|-----------------|----------------------------------------------------------------------------------|
| `a.min()`  | `X::value_type` | Returns the smallest value the `a()` may return. Strictly less than `a.max()`.   |
| `a.max()`  | `X::value_type` | Returns the largest value the `a()` may return. Strictly greater than `a.min()`. |
| `a()`      | `X::value_type` | Returns a random value in the range `[a.min(), a.max()]`.                        |

Additionally, if the following member function is provided, then the values
returned by `a()` must be uniformly distributed:

    static constexpr void is_uniform_random_range() { }

In this case, `X` is a *Uniform Random Range*.

Range Traversal Concepts {#range_traversal_concepts}
------------------------

### Forward range {#forward_range}

A type `X` models the *Forward Range* concept if `X` is a [Range] and the
following expressions are valid with the given constraints.

| Expression      | Return type    | Preconditions |
|-----------------|----------------|---------------|
| `a.front()`     | `X::reference` | `!a.empty()`  |
| `r.pop_front()` |                | `!r.empty()`  |

Additionally, if `X` models [Sized Range], then the requirement in the
following table must hold.

| Expression                          | Precondition | Postconditions       |
|-------------------------------------|--------------|----------------------|
| `auto n = r.size(); r.pop_front();` | `!r.empty()`  | `r.size() == --n`   |

### Backward Range {#backward_range}

A type `X` models the *Backward Range* concept if `X` is a [Range] and the
following expressions are valid with the given constraints.

| Expression     | Return type    | Preconditions |
| ---------------|----------------|---------------|
| `a.back()`     | `X::reference` | `!r.empty()`  |
| `r.pop_back()` |                | `!r.empty()`  |

Additionally, if `X` models [Sized Range], then the requirements in the
following table must hold.

| Expression                         | Precondition  | Postconditions      |
|------------------------------------|---------------|---------------------|
| `auto n = r.size(); r.pop_back();` | `!r.empty()`  | `r.size() == --n`   |

### Bidirectional Range {#bidirectional_range}

A type `X` models the *Bidirectional Range* concept if `X` is a
[Forward Range] and a [Backward Range].

### Random Access Range {#random_access_range}

A type `X` models the *Random Access Range* concept if `X` is a
[Forward Range] and the following expressions are valid with the given
constraints.

| Expression      | Return type            | Preconditions |
|-----------------|------------------------|---------------|
| `X::index_type` | Unsigned integral type |               |
| `a[i]`          | `X::reference`         | `!a.empty()`  |

### Slicable Range {#slicable_range}

A type `X` models the *Slicable Range* concept if `X` is a
[Forward Range] and the following expressions are valid with the given
semantics.

<table>
<tr>
  <th>Expression</th>
  <th>Return type</th>
  <th>Preconditions</th>
  <th>Semantics</th>
</tr>
<tr>
  <td>
    `X::index_type`
  </td>
  <td>
    Unsigned integral type
  </td>
  <td>
  </td>
  <td>
  </td>
</tr>
<tr>
  <td>
    `a.slice(i, j)`
  </td>
  <td>
    A Range modeling a traversal concept no weaker than that of `X`.
  </td>
  <td>
    `i <= j`
  </td>
  <td>
    Returns range with the same elements as `take(drop(x, i), j - i)`
  </td>
</tr>
</table>

If `X` additionally models [Sized Range], then the following condition
holds:

| Expression      | Pre-condition             | Semantics                       |
|-----------------|---------------------------|---------------------------------|
| `a.slice(i, j)` | `i <= j && j <= a.size()` | `a.slice(i, j).size() == j - i` |

Range Access Concepts {#range_access_concepts}
---------------------

### Readable Range {#readable_range}

A type `X` models the *Readable Range* concept if `X` is a [Range], the
type `X::value_type` exists, and `X::value_type` is Constructible from
`X::reference`, and `X::value_type` is Assignable from `X::reference`.

### Writable Range {#writable_range}

A type `X` models the *Writable Range* concept for a type `O` if `X` is a
[Range] and `O` is Assignable to `X::reference`.

[Range]: #range ""
[Multi-Pass Range]: #multi_pass_range ""
[Sized Range]: #sized_range ""

[Forward Range]: #forward_range ""
[Backward Range]: #backward_range ""
[Bidirectional Range]: #bidirectional_range ""
[Random Access Range]: #random_access_range ""
[Slicable Range]: #slicable_access_range ""

[Readable Range]: #readable_range ""
[Writable Range]: #writable_range ""