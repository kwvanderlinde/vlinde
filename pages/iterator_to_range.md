Comparison of Iterator to Range Algorithms {#iterator_to_range}
==========================================

This is a map from iterator based algorithms to the analagous range
based algorithms. Note that it is not a strict equivalence. In part,
this is because of the difference nature of iterators and ranges.
Additionally, some iterator based algorithms, e.g., `swap_ranges`,
assume the second range will not be exhausted, and so return an
iterator into the second range. The range equivalents, e.g., the
identically named `swap_ranges`, account for either range being
exhausted, and so return a pair of ranges.

Non-modifying algorithms
------------------------

<table>
  <tr>
    <th>
      Iterator code
    </th>
    <th>
      Range-based equivalent
    </th>
  </tr>
  <tr>
    <td>

        all_of(first, last, pred)
        any_of(first, last, pred)
        none_of(first, last, pred)

    </td>
    <td>

        all_of(range, pred)
        any_of(range, pred)
        none_of(range, pred)

    </td>
  </tr>
  <tr>
    <td>

        for_each(first, last, func)

    </td>
    <td>

        for_each(range, func)

    </td>
  </tr>
  <tr>
    <td>

        count(first, last, value)
        count_if(first, last, pred)

    </td>
    <td>

        count(range, value)
        count(filter(range, pred))

    </td>
  </tr>
  <tr>
    <td>

        mismatch(first, last, first2)
        mismatch(first, last, first2, pred)
        mismatch(first, last, first2, last2)
        mismatch(first, last, first2, last2, pred)

    </td>
    <td>

        mismatch(range1, range2)
        mismatch(range1, range2, pred)

    </td>
  </tr>
  <tr>
    <td>

        equal(first, last, first2)
        equal(first, last, first2, pred)
        equal(first, last, first2, last2)
        equal(first, last, first2, last2, pred)

    </td>
    <td>

        equal(range1, range2)
        equal(range1, range2, pred)

    </td>
  </tr>
  <tr>
    <td>

        find(first, last, value)
        find_if(first, last, pred)
        find_if_not(first, last, pred)

    </td>
    <td>

        find_if(range, equals(value))
        find_if(range, pred)
        find_if(range, not1(pred))

    </td>
  </tr>
  <tr>
    <td>

        find_end(first, last, first2, last2)
        find_end(first, last, first2, last2, pred)

    </td>
    <td>

        search_last(range1, range2)
        search_last(range1, range2, pred)

    </td>
  </tr>
  <tr>
    <td>

        find_first_of(first, last, first2, last2)
        find_first_of(first, last, first2, last2, pred)

    </td>
    <td>

        find_first_of(range1, range2)
        find_first_of(range1, range2, pred)

    </td>
  </tr>
  <tr>
    <td>

        adjacent_find(first, last)
        adjacent_find(first, last, pred)

    </td>
    <td>

        adjacent_find(range)
        adjacent_find(range, pred)

    </td>
  </tr>
  <tr>
    <td>

        search(first, last, first2, last2)
        search(first, last, first2, last2, pred)

    </td>
    <td>

        search(range1, range2)
        search(range1, range2, pred)

    </td>
  </tr>
  <tr>
    <td>

        search_n(first, last, count, value)
        search_n(first, last, count, value, pred)

    </td>
    <td>

        search(range, repeat(value, count))
        search(range, repeat(value, count), pred)

    </td>
  </tr>
  <tr>
    <td>

        is_permutation(first1, last1, first2)
        is_permutation(first1, last1, first2, pred)
        is_permutation(first1, last1, first2, last2)
        is_permutation(first1, last1, first2, last2, pred)

    </td>
    <td>
      Currently no alternative.
    </td>
  </tr>
</table>


Modifying algorithms
--------------------

<table>
  <tr>
    <td>

        copy(first, last, out_first)
        copy_if(first, last, out_first, pred)
        copy_n(first, count, out_first)
        copy_backward(first, last, last_out)

    </td>
    <td>

        copy(in, out)
        copy(filter(in, pred), out)
        copy(take(in, count), out)
        copy(retro(in), retro(out))

    </td>
  </tr>
  <tr>
    <td>

        move(first, last, first_out)
        move_backward(first, last, last_out)

    </td>
    <td>

        move(in, out)
        move(retro(in), retro(out))

    </td>
  </tr>
  <tr>
    <td>

        fill(first, last, value)
        fill_n(first, count, value);

    </td>
    <td>

        copy(repeat(value), range)
        copy(repeat(value, count), range)

    </td>
  </tr>
  <tr>
    <td>

        transform(first, last, first_out, unary_op)
        transform(first1, last1, first2, last2, first_out, binary_op)

    </td>
    <td>

        copy(map(range, unary_op), out)
        copy(map(zip(range1, range2), binary_op), out)

    </td>
  </tr>
  <tr>
    <td>

        generate(first, last, generator)
        generate_n(first, count, generator)

    </td>
    <td>

        copy(generate(generator), out)
        copy(generate(generator), take(out, count))

    </td>
  </tr>
  <tr>
    <td>

        remove(first, last, value)
        remove_if(first, count, pred)
        remove_copy(first, last, first_out, value)
        remove_copy_if(first, count, first_out, pred)

    </td>
    <td>

        remove_if(range, equals(value))
        remove_if(first, count, pred)
        copy(filter(range, equals(value), out)
        copy(filter(range, not2(pred)), out)

    </td>
  </tr>
  <tr>
    <td>

        replace_if(first, last, equals(old_value), new_value)
        replace_if(first, count, pred, new_value)
        replace_copy(first, last, first_out, old_value, new_value)
        replace_copy_if(first, count, first_out, pred, new_value)

    </td>
    <td>

        replace_if(range, equals(old_value), new_value)
        replace_if(range, pred, new_value)
        copy(map(range, [&](A a){ return a == old_value ? new_value : a }), out)
        copy(map(range, [&](A a){ return pred(a) ? new_value : a }), out)

    </td>
  </tr>
  <tr>
    <td>

        iter_swap(iter1, iter2)
        swap_ranges(first1, last1, first2)

    </td>
    <td>

        swap_front(range1, range2)
        swap_range(range1, range2)

    </td>
  </tr>
  <tr>
    <td>

        reverse(first, last)
        reverse_copy(first, last, first_out)

    </td>
    <td>

        reverse(range)
        copy(retro(range), out)

    </td>
  </tr>
  <tr>
    <td>

        rotate(first, middle, last)
        rotate_copy(first, middle, last, first_out)

      Note: [first, middle) union [middle, last) == [first, last).

    </td>
    <td>

        rotate(range1, range2)
        copy(range1, copy(range2, out))

      Note: range1 and range2 need not be subranges of a common range.

    </td>
  </tr>
  <tr>
    <td>

        random_shuffle(first, last)        // deprecated
        random_shuffle(first, last, func)  // deprecated
        shuffle(first, last, urng)

    </td>
    <td>

        // Implementation defined. No particular equivalent.
        shuffle(range, map(iota(range.size(), 0), func))
        shuffle(range, urng)

    </td>
  </tr>
  <tr>
    <td>

        unique(first, last)
        unique(first, last, pred)
        unique_copy(first, last, first_out)
        unique_copy(first, last, first_out, pred)

    </td>
    <td>

        unique(range)
        unique(range, pred)
        copy(group(range), out)
        copy(group(range, pred), out)

    </td>
  </tr>

  <tr>
    <td>
    </td>
    <td>
    </td>
  </tr>
</table>