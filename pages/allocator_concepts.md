Allocator concepts {#allocator_concepts_page}
==================

[TOC]

Allocator concepts {#allocator_concepts}
==================

The status quo of C++ allocators is far from ideal. The design of
allocators causes both the definition and use of allocators to be a
tedious process. This includes the particular use which is defining
allocator aware containers, or other types which must correctly wrap an
allocator. The WG21 paper describing the [EASTL][] outlines several
issues with C++'s allocator model, and exhibits potential solutions to
many of these shortcomings. These issues include:

1. Allocators are difficult to use.
2. Allocators are essentially required to be templated types.
3. Allocators defy any sensible object model (they are "class-based",
   not "instance-based").
4. `rebind` effectively makes an allocator's value type meaningless.
5. Allocators are both memory allocators *and* object factories, which
   is a poor separation of responsibility.

Several more issues are listed involving the interaction of allocators
with containers. These are described more fully at the
[container concepts page][containers].

Some of these issues have attempted rectifications in C++11.
Specifically, C++11 allows for "stateful" allocators, which are
allocators which actually support instance semantics. However, the use
of these allocators is needlessly complex, since even the most basic
operations cannot be performed directly, but must instead be queried
using the various `propagate_on_container_*` types in
`std::allocator_traits`. Even copy construction must be executed using
`allocator_traits<Allocator>::%select_on_container_copy_construction`.

In order to bring sanity to the world of C++ allocators, we here define
a simplified allocator model, expressed as a few concepts, which are
intended to greatly simplify both the use and creation of allocators.
These concepts were influenced by the EASTL design, but we don't try to
encompass all allocators within a particular interface. Rather, we have
factored the allocator concepts to support different kinds of
allocators. In particular, the Allocator concept encompasses all
allocators. The Allocator Value concept extends Allocator with value
semantics. The latter is appropriate for general use, for instance, in
containers. The former has more specialized use, e.g., an Allocator
which reserves a fixed portion of the call stack cannot support copy
or swap operations, so it would not be a Allocator Value.

The particular interface used here is also influenced by Howard
Hinnant's WG21 paper [N1953][] which proposes extensions to C++'s
allocator model which allow more cooperation between an allocator and
the user by communicating size information. It is based off Hinnant's
previous WG14 paper [N1085][] which proposes analogous extensions to
C's `malloc` system. The reader is encouraged to read these documents
as they may provide a rationale for the design of out allocator model.

In the following sections, `memory_block` always refers to
`::vlinde::memory::memory_block` and `size_t` always refers to
`::%std::size_t`.

[EASTL]: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2007/n2271.html ""
[Containers]: \ref container_concepts_page ""
[N1953]: http://www.open-std.org/jtc1/sc22/wg21/docs/papers/2006/n1953.html ""
[N1085]: http://www.open-std.org/jtc1/sc22/wg14/www/docs/n1085.htm ""

Valid Memory Blocks {#valid_memory_blocks}
-------------------

In order to intelligibly disallow various memory violations, the
following concepts use the notion of a valid `memory_block`.

Let `mb` be a value of type `memory_block`, and let `a` be an
allocator. We say that `mb` is valid with respect to `a` if `a` may
free `mb`. If a function returns a `memory_block` which is valid with
respect to `a`, then the `memory_block` must remain valid with respect
to `a` until the referenced memory is freed, the size of the referenced
memory is changed, or `a` is assigned to. Freeing or resizing of memory
may only occur at the explicit request of the user.

A `memory_block` with a null `pointer` field is not consider valid with
respect to any allocator.

Allocator {#allocator}
---------

A type `X` models the *Allocator* concept if `X` is Copy Constructible,
Copy Assignable, Destructible, Equality Comparable, and if l-values of
type `X` are Swappable. Also, `X` must have member functions covariant
with the following:

    memory_block alloc(size_t size) noexcept;
    void free(memory_block block) noexcept;

None of these operations shall exit via an exception. Additionally,
equality implies equivalence of allocators, as per the usual value
semantics:

- If `b` is a copy of `a`, then `a == b`.
- If `a == b`, and the same operations are then applied to both `a` and
  `b`, then `a == b`.
- If `mb` is valid with respect to `a`, and `a == b`, then `mb` is also
  valid with respect to `b`.

The call `a.alloc(size)` attempts to allocate a memory block of size at
least `size` from the allocator `a`. If successful, a block is returned
which is valid with respect to `a`, and with its `size` field set to
the same value as the `size` parameter. Otherwise, on failure, a block
is returned with a null `pointer` field, and with the `size` field set
to a hint which indicates a size of allocation that may succeed in the
future. Note: the size returned upon failure is not required to be
meaningful.

The call `a.free(block)` frees the memory referenced by
`block.pointer` from the allocator `a`. `block` is required to be valid
with respect to `a`.

Optional Allocator Operations {#optional_allocator_operations}
-----------------------------

The following operations are not required to be implemented by an
Allocator. Some of these operations permit a reasonable default
implementation. Such implementations are provided through
`::vlinde::memory::allocator_traits`.

### `aligned_alloc`

    memory_block alloc(size_t size, size_t alignment, size_t offset) noexcept;

<dl class="section pre">
<dt>
  Pre
</dt>
<dd>
  `alignment` must be a power of two. Also, `offset < size` unless
  `size == 0`.
</dd>
</dl>
<dl class="section post">
<dt>
  Post
</dt>
<dd>
  `result.pointer == null || result.size == size`. Also, if
  `result.pointer != null`, then the returned memory is aligned as
  requested.
</dd>
</dl>

The call `a.alloc(size, alignment, offset)` has the same behaviour as
`a.alloc(size)`, except that, upon success, the returned `memory_block`
has the request alignment. Specifically, the memory at
`advance(mb.pointer, offset)` is aligned to `alignment`.

It is strongly encouraged that extended alignments be supported.
However, as of C++14, the standard manner of aligning memory (i.e.,
`::std::align`) is not required to support extended alignments. Thus,
this condition is not currently a strict requirement, but will likely
be strengthened for future revisions of the standard.

### `resize`

    memory_block resize(memory_block mb, size_t size) noexcept;

<dl class="section pre">
<dt>
  Pre
</dt>
<dd>
  `mb` must be valid with respect to `*this`.
</dd>
</dl>
<dl class="section post">
<dt>
  Post
</dt>
<dd>
  `result.pointer == null || result.size == size`.
</dd>
</dl>

The call `a.resize(mb, size)` attempts to resize the memory referenced
by `mb` to `size`. If such a resize is not possible, then the call
fails. Otherwise, if the call is successful, the result will have the
same pointer as `mb`, but the size will be at least `size`.

If `size == mb.size`, then this call does nothing, and simply returns
`mb`. If `size < mb.size`, then the call may only succeed if the
resulting size is actually less than `mb.size`. That is, success
implies that the memory was actually reduced in size. Additionally, the
post-condition enforces that the memory must be expanded in the case
that `size > mb.size`.

Upon failure, the result's `pointer` field is set to `nullptr`.
Additionally, the `size` field should is set such that using this value
for the `size` parameter in a future call may succeed. Note that the
returned `size` is not required to be meaningful. For instance, a
conforming implementation may always set this field to `mb.size` upon
failure.

### `resolve_internal`

    memory_block resolve_internal(void * pointer) const noexcept;

If `pointer` points within a block of memory owned by `*this`, then the
result is a `memory_block` which references that block of memory.
