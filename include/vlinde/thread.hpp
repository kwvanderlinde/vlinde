#ifndef VLINDE_THREAD_HPP
#define VLINDE_THREAD_HPP

namespace vlinde
{
/** Primitives for multithreading.
 */
namespace thread
{
}
}

#include "thread/null_mutex.hpp"
#include "thread/recursive.hpp"
#include "thread/spin_mutex.hpp"
#include "thread/synchronized.hpp"
#include "thread/thread_local_storage.hpp"
#include "thread/ticket_mutex.hpp"

#endif // VLINDE_THREAD_HPP
