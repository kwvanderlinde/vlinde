#ifndef VLINDE_TMP_DETAIL_TYPELIST_OP_FWD_HPP
#define VLINDE_TMP_DETAIL_TYPELIST_OP_FWD_HPP

#include <cstddef>

namespace vlinde
{
namespace tmp
{
namespace detail
{
template <typename Typelist>
struct head_impl;

template <typename Typelist>
struct tail_impl;

template <typename T, typename Typelist>
struct cons_impl;

template <template <typename> class Func, typename Typelist>
struct map_impl;

template <template <typename, typename> class Func, typename Init,
          typename Typelist>
struct fold_impl;

template <typename Typelist, ::std::size_t Index>
struct at_impl;

template <typename Typelist1, typename Typelist2>
struct concat_impl;

template <template <typename> class Predicate, typename Typelist>
struct find_first_impl;
}
}
}

#endif // VLINDE_TMP_DETAIL_TYPELIST_OP_FWD_HPP
