#ifndef VLINDE_TMP_TYPELIST_OP_HPP
#define VLINDE_TMP_TYPELIST_OP_HPP

namespace vlinde
{
namespace tmp
{
namespace detail
{

template <typename T, typename... Ts>
struct head_impl<typelist<T, Ts...>>
{
	using type = T;
};

template <typename T, typename... Ts>
struct tail_impl<typelist<T, Ts...>>
{
	using type = typelist<Ts...>;
};

template <typename T, typename... Ts>
struct cons_impl<T, typelist<Ts...>>
{
	using type = typelist<T, Ts...>;
};

template <template <typename> class Func>
struct map_impl<Func, typelist<>>
{
	using type = typelist<>;
};

template <template <typename> class Func, typename Typelist>
struct map_impl
{
	using type = cons<Func<head<Typelist>>, map<Func, tail<Typelist>>>;
};

template <template <typename, typename> class Func, typename Init>
struct fold_impl<Func, Init, typelist<>>
{
	using type = Init;
};

template <template <typename, typename> class Func, typename Init,
          typename Typelist>
struct fold_impl
{
	using type = fold<Func, Func<Init, head<Typelist>>, tail<Typelist>>;
};

template <typename Typelist, ::std::size_t Index>
struct at_impl
{
	using type = at<Typelist, Index - 1>;
};

template <typename Typelist>
struct at_impl<Typelist, 0>
{
	using type = head<Typelist>;
};

template <typename... Ts, typename... Us>
struct concat_impl<typelist<Ts...>, typelist<Us...>>
{
	using type = typelist<Ts..., Us...>;
};

// We must handle this case specially to avoid the next instantiation,
// which is undefined.

template <template <typename> class Predicate, typename T>
struct find_first_impl<Predicate, typelist<T>>
{
	using type =
	    typename ::std::enable_if<Predicate<T>::value, T>::type;
};

template <template <typename> class Predicate, typename T,
          typename... Ts>
struct find_first_impl<Predicate, typelist<T, Ts...>>
{
	using type = typename ::std::conditional<
	    Predicate<T>::value, T,
	    find_first<Predicate, typelist<Ts...>>>::type;
};
}
}
}

#endif // VLINDE_TMP_TYPELIST_OP_HPP
