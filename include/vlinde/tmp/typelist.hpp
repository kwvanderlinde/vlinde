#ifndef VLINDE_TMP_TYPELIST_HPP
#define VLINDE_TMP_TYPELIST_HPP

#include "detail/typelist_op_fwd.hpp"

#include <vlinde/tmp/logic.hpp>

#include <cstddef>
#include <type_traits>

namespace vlinde
{
namespace tmp
{
template <typename... Types>
struct typelist
{
};

// The following are operations on type lists.

template <typename Typelist>
using head = typename detail::head_impl<Typelist>::type;

template <typename Typelist>
using tail = typename detail::tail_impl<Typelist>::type;

template <typename T, typename Typelist>
using cons = typename detail::cons_impl<T, Typelist>::type;

template <template <typename> class Func, typename Typelist>
using map = typename detail::map_impl<Func, Typelist>::type;

template <template <typename, typename> class Func, typename Init,
          typename Typelist>
using fold = typename detail::fold_impl<Func, Init, Typelist>::type;

template <typename Typelist>
using any = fold<or_, ::std::false_type, Typelist>;

template <typename Typelist>
using all = fold<and_, ::std::true_type, Typelist>;

template <typename Typelist>
using none = ::std::integral_constant<bool, !any<Typelist>::value>;

template <typename Typelist, ::std::size_t Index>
using at = typename detail::at_impl<Typelist, Index>::type;

template <typename Typelist1, typename Typelist2>
using concat = typename detail::concat_impl<Typelist1, Typelist2>::type;

template <typename Typelists>
using flatten = fold<concat, typelist<>, Typelists>;

template <template <typename> class Predicate, typename Typelist>
using find_first =
    typename detail::find_first_impl<Predicate, Typelist>::type;
}
}

#include "detail/typelist_op.hpp"

#endif // VLINDE_TMP_TYPELIST_HPP
