#ifndef VLINDE_TMP_UTILITY_HPP
#define VLINDE_TMP_UTILITY_HPP

#include <vlinde/tmp/logic.hpp>

#include <type_traits>

namespace vlinde
{
namespace tmp
{
namespace detail
{
template <typename Class, typename... Args>
struct enable_as_copy_constructor_impl;

// No parameters cannot act as a copy constructor.
template <typename Class>
struct enable_as_copy_constructor_impl<Class> : ::std::true_type
{
};

// Two or more parameters cannot act as a copy constructor.
template <typename Class, typename Arg1, typename Arg2, typename... Args>
struct enable_as_copy_constructor_impl<Class, Arg1, Arg2, Args...>
    : ::std::true_type
{
};

// One parameter might act as a copy constructor.
template <typename Class, typename Arg>
struct enable_as_copy_constructor_impl<Class, Arg>
    : not_<
          ::std::is_base_of<Class, typename ::std::remove_reference<Arg>::type>>
{
};
}

/** This is an extension `disable_if_same_or_derived` as done by Eric Niebler at
 * http://ericniebler.com/2013/08/07/universal-references-and-the-copy-constructo/
 * It is specifically intended to disable "forward constructors" such as
 *
 *    struct A
 *    {
 *    // ...
 *
 *    template<typename... Args>
 *    A(Args &&... args);
 *
 *    // ...
 *    };
 *
 * Such constructors are often better matches the the copy constructor, so they
 * should almost never match a single type parameter of the class type.
 */
template <typename Class, typename... Args>
using disable_as_copy_constructor = typename ::std::enable_if<
    detail::enable_as_copy_constructor_impl<Class, Args...>::value>::type;
}
}

#endif // VLINDE_TMP_UTILITY_HPP
