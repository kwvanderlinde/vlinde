#ifndef VLINDE_TMP_LOGIC_HPP
#define VLINDE_TMP_LOGIC_HPP

#include <type_traits>

namespace vlinde
{
namespace tmp
{
template <typename Lhs, typename Rhs>
using or_ = ::std::integral_constant<bool, Lhs::value || Rhs::value>;

template <typename Lhs, typename Rhs>
using and_ = ::std::integral_constant<bool, Lhs::value&& Rhs::value>;

template <typename Bool>
using not_ = ::std::integral_constant<bool, !Bool::value>;
}
}

#endif // VLINDE_TMP_LOGIC_HPP
