#ifndef VLINDE_RANGE_REPEAT_HPP
#define VLINDE_RANGE_REPEAT_HPP

#include "detail/repeat_range.hpp"

#include "take.hpp"

namespace vlinde
{
namespace range
{
/** Constructs an infinite range where each element is `value`.
  *
  * \tparam T The value type of the range.
  *
  * \param value The value to be repeated throughout the range.
  *
  * \returns A [Range] which is [Multi Pass], [Infinite], [Forward],
  * [Random Access], [Slicable], and [Readable].
  *
  * [Range]: \ref range
  * [Multi Pass]: \ref multi_pass_range
  * [Infinite]: \ref infinite_range
  * [Forward]: \ref forward_range
  * [Random Access]: \ref random_access_range
  * [Slicable]: \ref slicable_range
  * [Readable]: \ref readable_range
  */
template <typename T>
auto repeat(T const& value) -> detail::repeat_range<T>
{
	return detail::repeat_range<T>{value};
}

/** Constructs an finite range of `count` elements, where each element
  * is `value`.
  *
  * \tparam T The value type of the range.
  *
  * \tparam Integral An Integral type.
  *
  * \param value The value to be repeated throughout the range.
  *
  * \param count The number of elements in the range.
  *
  * \returns A [Range] with `count` elements which is [Multi Pass],
  * [Bidirectional], [Random Access], [Slicable], [Sized], and
  * [Readable].
  *
  * [Range]: \ref range
  * [Multi Pass]: \ref multi_pass_range
  * [Bidirectional]: \ref bidirectional_range
  * [Random Access]: \ref random_access_range
  * [Slicable]: \ref slicable_range
  * [Sized]: \ref sized_range
  * [Readable]: \ref readable_range
  */
template <typename T, typename Integral>
auto repeat(T const& value, Integral count)
    -> decltype(take(repeat(value), count))
{
	return take(repeat(value), count);
}
}
}

#endif // VLINDE_RANGE_REPEAT_HPP
