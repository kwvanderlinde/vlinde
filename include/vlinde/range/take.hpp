#ifndef VLINDE_RANGE_TAKE_HPP
#define VLINDE_RANGE_TAKE_HPP

#include "detail/take_range.hpp"

#include <utility>

namespace vlinde
{
namespace range
{
/** Creates a new range representing the first `count` elements of
  * `range`.
  *
  * If `range` has no more than `count` elements, then the returned
  * range represents all of `range`.
  *
  * \tparam Range A [Range](\ref range).
  *
  * \tparam Integral An Integral type.
  *
  * \param range The range to take elements from.
  *
  * \param count The number of elements to represent.
  *
  * \returns A [Range]. If `Range` models [Multi Pass Range], then so
  * does the result. If `Range` models [Forward Range], then so does
  * the result. If `Range` models [Random Access Range], so does the
  * result. If `Range` models [Sized Range], then so does the result. If
  * `Range` models both [Random Access Range] and [Sized Range], or if
  * `Range` models both [Range Access Range] and [Infinite Range], then
  * the result additionally models [Backward Range]. If `Range` models
  * [Readable Range], the so does the result. If `Range` models
  * [Writable Range], then so does the result.
  *
  * [Range]: \ref range
  * [Multi Pass Range]: \ref multi_pass_range
  * [Forward Range]: \ref forward_range
  * [Bidirectional Range]: \ref bidirectional_range
  * [Random Access Range]: \ref random_access_range
  * [Slicable Range]: \ref slicable_range
  * [Readable Range]: \ref readable_range
  * [Writable Range]: \ref writable_range
  *
  * \requires `count >= 0`;
  */
template <typename Range, typename Integral>
auto take(Range range, Integral count)
    -> detail::take_range<Range, Integral>
{
	assert(count >= 0);

	return { ::std::move(range), count};
}

template <typename Range, typename I, typename Integral>
auto take(detail::take_range<Range, I> range, Integral count)
    -> detail::take_range<Range, Integral>
{
	assert(count >= 0);

	return { ::std::move(range), count};
}
}
}

#endif // VLINDE_RANGE_TAKE_HPP
