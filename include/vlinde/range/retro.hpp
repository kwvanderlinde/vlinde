#ifndef VLINDE_RANGE_RETRO_HPP
#define VLINDE_RANGE_RETRO_HPP

#include "detail/retro_range.hpp"

namespace vlinde
{
namespace range
{

/** Creates a range which represents a reversed version of `range`.
  *
  * \tparam Range A [Range][].
  *
  * \param range The range to reverse.
  *
  * \returns A [Range]. If `Range` models [Multi Pass Range][], so does
  * the result. If `Range` models [Forward Range][], the result models
  * [Backward Range][]. If `Range` models [Backward Range][], the result
  * models [Forward Range][]. If `Range` models [Sized Range][], so does
  * the result. If `Range` models both [Sized Range][] and
  * [Random Access Range][], the result models [Random Access Range][].
  * If `Range` models both [Sized Range][] and [Slicable Range][], the
  * result models [Slicable Range][]. If `Range` models
  * [Readable Range][], so does the result. If `Range` models
  * [Writable Range][], so does the result.
  *
  * [Range]: \ref range
  * [Multi Pass Range]: \ref multi_pass_range
  * [Forward Range]: \ref forward_range
  * [Backward Range]: \ref backward_range
  * [Bidirectional Range]: \ref bidirectional_range
  * [Random Access Range]: \ref random_access_range
  * [Sized Range]: \ref sized_range
  * [Slicable Range]: \ref slicable_range
  * [Readable Range]: \ref readable_range
  * [Writable Range]: \ref writable_range
  */
template <typename Range>
auto retro(Range range) -> detail::retro_range<Range>
{
	return detail::retro_range<Range>{::std::move(range)};
}
}
}

#endif
