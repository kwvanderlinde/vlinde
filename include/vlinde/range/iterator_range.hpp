#ifndef VLINDE_RANGE_ITERATOR_RANGE_HPP
#define VLINDE_RANGE_ITERATOR_RANGE_HPP

#include <cassert>
#include <iterator>

#include <vlinde/concepts.hpp>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename Iterator, typename = concepts::Void>
struct iterator_range_index_type_mixin
{
};

template <typename Iterator>
struct iterator_range_index_type_mixin<
    Iterator, concepts::requires<
                  concepts::models<concepts::RandomAccessIterator, Iterator>>>
{
	using index_type = typename ::std::make_unsigned<
	    typename ::std::iterator_traits<Iterator>::difference_type>::type;
};
}

template <typename Iterator>
class iterator_range : public detail::iterator_range_index_type_mixin<Iterator>
{
private:
	Iterator m_first;
	Iterator m_last;

	using traits = ::std::iterator_traits<Iterator>;

public:
	using reference = typename ::std::iterator_traits<Iterator>::reference;

	iterator_range(Iterator first, Iterator last)
	: m_first(::std::move(first))
	, m_last(::std::move(last))
	{
	}

	/** \requires `Iterator` is a Forward Iterator.
	  */
	/*template <typename..., typename X = Iterator,
	          typename = concepts::requires<
            concepts::models<concepts::ForwardIterator, X>>>*/
	iterator_range(iterator_range const & other)
	: m_first(other.m_first)
	, m_last(other.m_last)
	{
	}

	iterator_range(iterator_range && other) = default;

	/** \requires `Iterator` is a Forward Iterator.
	  */
	/*template <typename..., typename X = Iterator,
	          typename = concepts::requires<
            concepts::models<concepts::ForwardIterator, X>>>*/
	iterator_range & operator=(iterator_range const & other)
	{
		m_first = other.m_first;
		m_last = other.m_last;
	}

	iterator_range & operator=(iterator_range && other) = default;

	~iterator_range() = default;

	bool empty() const
	{
		return m_first == m_last;
	}

	/** \requires `Iterator` is an Input Iterator.
	  */
	template <typename..., typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::InputIterator, X>>>
	auto front() const -> reference
	{
		assert(!this->empty());

		return *m_first;
	}

	/** \requires `Iterator` is an Input Iterator.
	  */
	template <typename..., typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::InputIterator, X>>>
	auto pop_front() -> void
	{
		assert(!empty());

		++m_first;
	}

	/** \requires `Iterator` is a Bidirectional Iterator.
	  */
	template <typename..., typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::BidirectionalIterator, X>>>
	auto back() const -> reference
	{
		assert(!this->empty());

		return *m_last;
	}

	/** \requires `Iterator` is a Bidirectional Iterator.
	  */
	template <typename..., typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::BidirectionalIterator, X>>>
	auto pop_back() -> void
	{
		assert(!empty());

		--m_last;
	}

	/** \requires `Iterator` is a Random Access Iterator.
	 */
	template <typename..., typename U = iterator_range, typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::RandomAccessIterator, X>>>
	auto size() const noexcept -> typename U::index_type
	{
		auto const result = m_last - m_first;
		assert(result > 0);
		return static_cast<typename U::index_type>(result);
	}

	/** \requires `Iterator` is a Random Access Iterator.
	  */
	template <typename..., typename U = iterator_range, typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::RandomAccessIterator, X>>>
	auto operator[](typename U::index_type index) -> reference
	{
		assert(index < this->size());

		return m_first[index];
	}

	/** \requires `Iterator` is a Random Access Iterator.
	  */
	template <typename..., typename U = iterator_range, typename X = Iterator,
	          typename = concepts::requires<
	              concepts::models<concepts::RandomAccessIterator, X>>>
	auto slice(typename U::index_type i, typename U::index_type j)
	    -> iterator_range
	{
		assert(i <= j);
		assert(j <= this->size());

		return iterator_range(m_first + i, m_first + j);
	}
};

namespace detail
{
using ::std::begin;

template <typename StdRange>
using range_iterator_type = decltype(begin(::std::declval<StdRange>()));
}

template <typename Iterator>
iterator_range<Iterator> make_iterator_range(Iterator first,
                                             Iterator last) noexcept
{
	return iterator_range<Iterator>(::std::move(first), ::std::move(last));
}

template <typename StdRange>
auto make_iterator_range(StdRange const & range) noexcept
    -> iterator_range<detail::range_iterator_type<StdRange>>
{
	using ::std::begin;
	using ::std::end;
	return make_iterator_range(begin(range), end(range));
}
}
}

#endif // VLINDE_RANGE_ITERATOR_RANGE_HPP
