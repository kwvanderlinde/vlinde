#ifndef VLINDE_RANGE_FILTER_HPP
#define VLINDE_RANGE_FILTER_HPP

#include "detail/filter_range.hpp"

namespace vlinde
{
namespace range
{

/** Creates a range of all elements `e` in `range` such that
  * `predicate(e)`.
  *
  * \tparam Range A [Readable][] [Range][].
  *
  * \tparam Predicate A unary predicate on `Range::value_type`.
  *
  * \param range The range to take elements from.
  *
  * \param predicate The predicate to apply to `range`.
  *
  * \returns A [Readable][] [Range][]. If `Range` is additionally a
  * [Forward Range][Forward] or [Backward Range][Backward], then so is
  * the result. If `Range` is [Writable][], then the result is
  * [Writable][] with the same set of types.
  *
  * [range]: \ref range
  * [forward]: \ref forward_range
  * [backward]: \ref backward_range
  * [readable]: \ref readable_range
  * [writable]: \ref writable_range
  */
template <typename Range, typename Predicate>
auto filter(Range range, Predicate predicate)
    -> detail::filter_range<Range, Predicate>
{
	return { ::std::move(range), ::std::move(predicate)};
}
}
}

#endif // VLINDE_RANGE_FILTER_HPP
