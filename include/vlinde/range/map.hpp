#ifndef VLINDE_RANGE_MAP_HPP
#define VLINDE_RANGE_MAP_HPP

#include "detail/map_range.hpp"

namespace vlinde
{
namespace range
{

/** Creates a range which automatically applies `functor` to each
  * element of `range`.
  *
  * The application of `functor` to `range` is done lazily.
  *
  * \tparam Range A [Readable][readable range] [Range][].
  *
  * \param range The range to reverse.
  *
  * \param functor The function object to apply to each element.
  *
  * \returns A [Readable Range][]. If `Range` models
  * [Multi Pass Range][], so does the result. If `Range` models
  * [Forward Range][], so does the result. If `Range` models
  * [Backward Range][], so does the result. If `Range` models
  * [Sized Range][], so does the result. If `Range` models
  * [Random Access Range][], so does the result. If `Range` models
  * [Slicable Range][], so does the result. If the return type of
  * `Functor` is suitably defined, the result models [Writable Range][].
  *
  * [Range]: \ref range
  * [Multi Pass Range]: \ref multi_pass_range
  * [Forward Range]: \ref forward_range
  * [Backward Range]: \ref backward_range
  * [Bidirectional Range]: \ref bidirectional_range
  * [Random Access Range]: \ref random_access_range
  * [Sized Range]: \ref sized_range
  * [Slicable Range]: \ref slicable_range
  * [Readable Range]: \ref readable_range
  * [Writable Range]: \ref writable_range
  */
template <typename Range, typename Functor>
auto map(Range range, Functor functor)
    -> detail::map_range<Range, Functor>
{
	return detail::map_range<Range, Functor>{ ::std::move(range),
	                                          ::std::move(functor)};
}
}
}

#endif // VLINDE_RANGE_MAP_HPP
