#ifndef VLINDE_RANGE_GENERATOR_HPP
#define VLINDE_RANGE_GENERATOR_HPP

#include "detail/generator_range.hpp"

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace range
{
template <typename Func>
auto generator(Func && func)
    -> detail::generator_range<typename ::std::result_of<Func()>::type>
{
	using value_type = typename ::std::result_of<Func()>::type;
	return detail::generator_range<value_type>(::std::forward<Func>(func));
}
}
}

#endif // VLINDE_RANGE_GENERATOR_HPP
