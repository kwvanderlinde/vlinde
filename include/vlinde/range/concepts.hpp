#ifndef VLINDE_RANGE_CONCEPTS_HPP
#define VLINDE_RANGE_CONCEPTS_HPP

#include <vlinde/concepts.hpp>

namespace vlinde
{
namespace range
{
// Basic Range Concepts

struct Range
    : concepts::refines<concepts::MoveConstructible, concepts::MoveAssignable>
{
	template <typename X, typename U = typename X::reference>
	static auto requires_impl(X const & a)
	    -> decltype(concepts::convertible_to<bool>(a.empty()));

	template <typename X>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>()));
};

struct MultiPassRange : concepts::refines<Range, concepts::CopyConstructible,
                                          concepts::CopyAssignable>
{
};

struct SizedRange : concepts::refines<Range>
{
	template <typename X, typename I = typename X::index_type>
	static auto requires_impl(X const & a)
	    -> decltype(concepts::returns<I>(a.size()));

	template <typename X>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>()));
};

namespace detail
{
template <int>
struct sfinae
{
};
}

struct InfiniteRange : concepts::refines<Range>
{
	template <typename T>
	static auto requires() -> decltype(
	    detail::sfinae<(T::empty(), 0)>{},
	    concepts::is_true(::std::integral_constant<bool, !T::empty()>{}));
};

struct RandomRange : concepts::refines<Range>
{
	template <typename X, typename U = typename X::reference,
	          typename V = typename X::value_type>
	static auto requires_impl(X const & a)
	    -> decltype(concepts::returns<V>(a.min()), concepts::returns<V>(a.max()),
	                concepts::returns<V>(a()));

	template <typename X>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>()));
};

struct UniformRandomRange : concepts::refines<RandomRange>
{
	template <typename X>
	static auto requires() -> decltype(X::is_uniform_random_range());

	template <typename X>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>()));
};

// Range Traversal Concepts

struct ForwardRange : concepts::refines<Range>
{
	template <typename X, typename U = typename X::reference>
	static auto requires_impl(X const & a, X & r)
	    -> decltype(concepts::returns<U>(a.front()), r.pop_front());

	template <typename X>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>(),
	                                                 concepts::val<X &>()));
};

struct BackwardRange : concepts::refines<Range>
{
	template <typename X, typename U = typename X::reference>
	static auto requires_impl(X const & a, X & r)
	    -> decltype(concepts::returns<U>(a.back()), r.pop_back());

	template <typename X>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>(),
	                                                 concepts::val<X &>()));
};

struct BidirectionalRange : concepts::refines<ForwardRange, BackwardRange>
{
};

struct RandomAccessRange : concepts::refines<ForwardRange>
{
	template <typename X, typename U = typename X::reference,
	          typename I = typename X::index_type>
	static auto requires_impl(X const & a, I i)
	    -> decltype(concepts::returns<U>(a[i]));

	template <typename X, typename I = typename X::index_type>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>(),
	                                                 concepts::val<I>()));
};

struct SlicableRange : concepts::refines<ForwardRange>
{
	// TODO Check that a.slice(i, j) models all range traversal concepts
	// that `X` does. For now, we just settle for ForwardRange.
	template <typename X, typename I = typename X::index_type>
	static auto requires_impl(X const & a, I i, I j)
	    -> decltype(concepts::models<ForwardRange, decltype(a.slice(i, j))>{});

	template <typename X, typename I = typename X::index_type>
	static auto requires() -> decltype(requires_impl(concepts::val<X const &>(),
	                                                 concepts::val<I>(),
	                                                 concepts::val<I>()));
};

// Range Access Concepts

struct ReadableRange : concepts::refines<Range>
{
	template <typename X, typename T = typename X::value_type,
	          typename U = typename X::reference>
	static auto requires() -> decltype(
	    concepts::models<concepts::Constructible, T, U>{},
	    concepts::models<concepts::Assignable, T &, U>{});
};

struct WritableRange : concepts::refines<Range(concepts::arg<0>)>
{
	template <typename X, typename O, typename U = typename X::reference>
	static auto requires() -> decltype(
	    concepts::is_true(concepts::models<concepts::Assignable, U, O>{}));
};
}
}

#endif // VLINDE_RANGE_CONCEPTS_HPP
