#ifndef VLINDE_RANGE_DETAIL_GENERATOR_RANGE_HPP
#define VLINDE_RANGE_DETAIL_GENERATOR_RANGE_HPP

#include <vlinde/tmp/utility.hpp>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename T>
class generator_range
{
	// I'd rather store an arbitrary `Func`, but I need to guarantee a move
	// assignment operator.
	::std::function<T()> m_func;
	T m_value;

public:
	// Readable Range.
	using value_type = T;

	using reference = value_type const &;

	template <typename Func,
	          typename = tmp::disable_as_copy_constructor<generator_range, Func>>
	generator_range(Func && func)
	: m_func(::std::forward<Func>(func))
	, m_value(m_func())
	{
	}

	// Not a Multi-Pass Range
	generator_range(generator_range const &) = delete;

	generator_range(generator_range &&) = default;

	~generator_range() = default;

	// Not a Multi-Pass Range
	generator_range & operator=(generator_range const &) = delete;

	generator_range & operator=(generator_range &&) = default;

	// Infinite Range
	static constexpr bool empty() noexcept
	{
		return false;
	}

	// Forward Range

	reference front() const
	{
		return m_value;
	}

	void pop_front()
	{
		m_value = m_func();
	}
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_GENERATOR_RANGE_HPP
