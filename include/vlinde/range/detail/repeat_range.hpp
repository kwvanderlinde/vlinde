#ifndef VLINDE_RANGE_DETAIL_REPEAT_RANGE_HPP
#define VLINDE_RANGE_DETAIL_REPEAT_RANGE_HPP

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename T>
class repeat_range
{
	T m_value;

public:
	explicit repeat_range(T const& value) : m_value{value}
	{
	}

	// Range

	repeat_range(repeat_range&&) = default;

	repeat_range& operator=(repeat_range&&) = default;

	using reference = T;

	// Infinite Range
	static constexpr bool empty()
	{
		return false;
	}

	// Multi-Pass Range

	repeat_range(repeat_range const&) = default;

	repeat_range& operator=(repeat_range const&) = default;

	// Forward Range

	reference front() const
	{
		return m_value;
	}

	void pop_front()
	{
	}

	// Backward Range

	reference back() const
	{
		return m_value;
	}

	void pop_back()
	{
	}

	// All the indexing concepts

	using index_type = ::std::size_t;

	// Random Access Range

	reference operator[](index_type) const
	{
		return m_value;
	}

	// Slicable Range

	auto slice(index_type i, index_type j)
	    const -> take_range<repeat_range, index_type>
	{
		return take(*this, j - i);
	}

	// Readable Range

	using value_type = T;
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_REPEAT_RANGE_HPP
