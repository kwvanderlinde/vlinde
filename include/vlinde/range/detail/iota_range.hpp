#ifndef VLINDE_RANGE_DETAIL_IOTA_RANGE_HPP
#define VLINDE_RANGE_DETAIL_IOTA_RANGE_HPP

#include <vlinde/concepts/numeric.hpp>
#include <vlinde/concepts/utility.hpp>

#include <type_traits>

#include <iostream>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename T, typename Step,
          typename = concepts::requires<
              concepts::models<concepts::Integral, T>,
              concepts::models<concepts::Integral, Step>>>
class iota_range
{
private:
	T m_current;
	T m_end;
	Step m_step;

public:
	using value_type = T;

	using reference = value_type;

	using index_type = typename ::std::make_unsigned<
	    decltype((m_end - m_current) / m_step)>::type;

public:
	iota_range(T const start, T const end)
	: iota_range{start, end, start <= end ? 1 : -1}
	{
	}

	iota_range(T const start, T const end, Step const step)
	: m_current{start}, m_end{end}, m_step{step}
	{
		assert(step != 0);
		if (step > 0)
			assert(start <= end);
		else
			assert(end <= start);

		// Calculate the true end value.
		if (m_step > 0)
			m_end = m_current + m_step * this->size();
		else
			m_end = m_current - (-m_step) * this->size();
	}

	bool empty() const
	{
		return m_current == m_end;
	}

	reference front() const
	{
		assert(!empty());

		return m_current;
	}

	void pop_front()
	{
		assert(!empty());

		m_current += m_step;
	}

	reference back() const
	{
		assert(!empty());

		return static_cast<reference>(m_end - m_step);
	}

	void pop_back()
	{
		assert(!empty());

		m_end -= m_step;
	}

	reference operator[](index_type const index) const
	{
		assert(!empty());
		assert(index <= size());

		if (m_step > 0)
			return m_current + index * m_step;
		else
			return m_current - index * (-m_step);
	}

	iota_range<T, Step> slice(index_type const first,
	                          index_type const last) const
	{
		assert(first <= last);
		assert(last <= size());

		auto const start =
		    static_cast<T>(m_step > 0 ? m_current + first * m_step
		                              : m_current - first * (-m_step));
		auto const end =
		    static_cast<T>(m_step > 0 ? m_current + last * m_step
		                              : m_current - last * (-m_step));

		return {start, end, m_step};
	}

	index_type size() const
	{
		if (m_current == m_end)
			return 0;

		if (m_step > 0)
			return static_cast<index_type>((m_end - m_current) /
			                               m_step);

		return static_cast<index_type>((m_current - m_end) / (-m_step));
	}
};

template <typename Start, typename End, typename Step>
struct iota_info
{
	using value_type = typename ::std::common_type<Start, End>::type;

	using step_type = typename ::std::conditional<
	    ::std::is_void<Step>::value,
	    typename ::std::make_signed<value_type>::type, Step>::type;

	using type = iota_range<value_type, step_type>;
};

template <typename Start, typename End, typename Step = void>
using iota_type = typename iota_info<Start, End, Step>::type;
}
}
}

#endif // VLINDE_RANGE_DETAIL_IOTA_RANGE_HPP
