#ifndef VLINDE_RANGE_DETAIL_RANDOM_RANGE_HPP
#define VLINDE_RANGE_DETAIL_RANDOM_RANGE_HPP

#include "generator_range.hpp"

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename URNG>
class random_range
{
	using result_type = typename URNG::result_type;

	generator_range<result_type> m_generator;

public:
	using value_type = typename decltype(m_generator)::value_type;

	using reference = typename decltype(m_generator)::reference;

	explicit random_range(URNG const & urng)
	: m_generator(urng)
	{
	}

	explicit random_range(URNG && urng)
	: m_generator(::std::move(urng))
	{
	}

	random_range(random_range &&) = default;

	random_range & operator=(random_range &&) = default;

	static result_type min()
	{
		return URNG::min();
	}

	static result_type max()
	{
		return URNG::max();
	}

	bool empty() const
	{
		return m_generator.empty();
	}

	reference front() const
	{
		return m_generator.front();
	}

	void pop_front()
	{
		m_generator.pop_front();
	}
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_RANDOM_RANGE_HPP
