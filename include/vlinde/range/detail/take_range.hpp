#ifndef VLINDE_RANGE_DETAIL_TAKE_RANGE_HPP
#define VLINDE_RANGE_DETAIL_TAKE_RANGE_HPP

#include "voider.hpp"

#include <vlinde/concepts/utility.hpp>
#include <vlinde/range/concepts.hpp>

#include <algorithm>
#include <cassert>
#include <utility>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename Integral>
using index = typename ::std::make_unsigned<Integral>::type;

template <typename Range, typename Index, typename = concepts::Void>
struct index_type_mixin
{
};

template <typename Range, typename Integral>
struct index_type_mixin<Range, Integral,
                        voider<typename Range::index_type>>
{
	// Interestingly, we don't use Range::index_type!
	using index_type = index<Integral>;
};

template <typename Range, typename Integral, typename = concepts::Void>
struct sized_range_mixin
{
	static index<Integral> get_size_else_max(Range const&)
	{
		return ::std::numeric_limits<index<Integral>>::max();
	}
};

template <typename Range, typename Integral>
struct sized_range_mixin<
    Range, Integral,
    concepts::requires<concepts::models<SizedRange, Range>>>
{
	static index<Integral> get_size_else_max(Range const& range)
	{
		return range.size();
	}
};

template <typename Range, typename Integral, typename = concepts::Void>
struct readable_range_mixin
{
};

template <typename Range, typename Integral>
struct readable_range_mixin<
    Range, Integral,
    concepts::requires<concepts::models<ReadableRange, Range>>>
{
	using value_type = typename Range::value_type;
};

template <typename Range, typename Integral>
struct take_range : sized_range_mixin<Range, Integral>,
                    readable_range_mixin<Range, Integral>,
                    index_type_mixin<Range, Integral>
{
private:
    template<typename T>
	static index<T> unsign(T t)
	{
		return t;
	}

	using count_type = index<Integral>;

	Range m_range;
	count_type m_count;

public:
	take_range(Range range, Integral count)
	: m_range{ ::std::move(range)}, m_count{size_impl(range, count)}
	{
		assert(count >= 0);
	}

	template <typename Integral2>
	take_range(take_range<Range, Integral2> range, Integral count)
	: m_range{ ::std::move(range.m_range)},
	  m_count{unsign(count) <= range.m_count ? unsign(count)
	                                         : range.m_count}
	{
		assert(count >= 0);
	}

	// Range

	using reference = typename Range::reference;

	take_range(take_range&&) = default;
	take_range& operator=(take_range&&) = default;

	bool empty() const
	{
		return m_count == 0 || m_range.empty();
	}

	// Multi-Pass Range

	// These are deleted if `Range` does not satisfy them.

	take_range(take_range const&) = default;

	take_range& operator=(take_range const&) = default;

	// Sized Range.

	// index_type is inherited from index_type_mixin.

private:
	// Note that the minimum between a larger and a smaller integral
	// type be represented by either type. So the caller can specify
	// `Result` to indicate the most convenient return type.
	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	static index<Integral> size_impl(U const& range, count_type count)
	{
		auto size_or_max =
		    sized_range_mixin<Range, Integral>::get_size_else_max(
		        range);

		if (count <= size_or_max) {
			return count;
		}

		return size_or_max;
	}

public:
	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto size() const -> count_type
	{
		return this->size_impl(m_range, m_count);
	}

	// Forward Range.

	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto front() const -> typename U::reference
	{
		return m_range.front();
	}

	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto pop_front() -> void
	{
		assert(!this->empty());

		m_range.pop_front();
		--m_count;
	}

	// Backward Range.

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<RandomAccessRange, U>,
	              tmp::or_<concepts::models<SizedRange, U>,
	                       concepts::models<InfiniteRange, U>>>>
	auto back() const -> typename U::reference
	{
		assert(!this->empty());
		return m_range[m_count - 1];
	}

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<RandomAccessRange, U>,
	              tmp::or_<concepts::models<SizedRange, U>,
	                       concepts::models<InfiniteRange, U>>>>
	auto pop_back() -> void
	{
		assert(!this->empty());

		--m_count;
	}

	// Random Access Range.

	// index_type is inherited from random_access_range_mixin.

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<RandomAccessRange, U>>>
	auto operator[](count_type const i) const -> typename U::reference
	{
		assert(i <= m_count);

		return m_range[i];
	}

	// Slicable Range.

	// index_type is inherited from random_access_range_mixin.

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<SlicableRange, U>>>
	auto slice(count_type const i, count_type const j)
	    const -> decltype(::std::declval<U const&>().slice(i, j))
	{
		assert(i <= j);
		assert(j <= m_count);

		// It's safe to just fall back to the underlying range.
		return m_range.slice(i, j);
	}

	// Readable Range

	// value_type is inherited from readable_range_mixin.

	// Writable Range

	// Handled automatically
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_TAKE_RANGE_HPP
