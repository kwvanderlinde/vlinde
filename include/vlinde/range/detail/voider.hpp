#ifndef VLINDE_RANGE_DETAIL_VOIDER_HPP
#define VLINDE_RANGE_DETAIL_VOIDER_HPP

#include <vlinde/concepts/utility.hpp>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename... Ts>
using voider = concepts::Void;
}
}
}

#endif // VLINDE_RANGE_DETAIL_VOIDER_HPP
