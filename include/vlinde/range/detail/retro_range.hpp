#ifndef VLINDE_RANGE_DETAIL_RETRO_RANGE_HPP
#define VLINDE_RANGE_DETAIL_RETRO_RANGE_HPP

#include "voider.hpp"

#include <vlinde/concepts/utility.hpp>
#include <vlinde/range/concepts.hpp>

#include <utility>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename Range, typename = concepts::Void>
struct retro_range_index_type_mixin
{
};

template <typename Range>
struct retro_range_index_type_mixin<Range,
                                    voider<typename Range::index_type>>
{
	using index_type = typename Range::index_type;
};

template <typename Range, typename = concepts::Void>
struct retro_range_value_type_mixin
{
};

template <typename Range>
struct retro_range_value_type_mixin<Range,
                                    voider<typename Range::value_type>>
{
	using value_type = typename Range::value_type;
};

template <typename Range, typename = concepts::requires<
                              concepts::models<BackwardRange, Range>>>
struct retro_range : retro_range_index_type_mixin<Range>,
                     retro_range_value_type_mixin<Range>
{
private:
	Range m_range;

public:
	explicit retro_range(Range range) : m_range(::std::move(range))
	{
	}

	// Range

	using reference = typename Range::reference;

	retro_range(retro_range&&) = default;

	retro_range& operator=(retro_range&&) = default;

	bool empty() const
	{
		return m_range.empty();
	}

	// Multi-Pass Range

	retro_range(retro_range const&) = default;

	retro_range& operator=(retro_range const&) = default;

	// Sized Range

	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<SizedRange, U>>>
	auto size() const -> typename U::index_type
	{
		return m_range.size();
	}

	// Forward Range.

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<BackwardRange, U>>>
	auto front() const -> typename U::reference
	{
		return m_range.back();
	}

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<BackwardRange, U>>>
	auto pop_front() -> void
	{
		assert(!this->empty());

		m_range.pop_back();
	}

	// Backward Range.

	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto back() const -> typename U::reference
	{
		assert(!this->empty());

		return m_range.front();
	}

	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto pop_back() -> void
	{
		assert(!this->empty());

		m_range.pop_front();
	}

	// Random Access Range.

	template <typename..., typename U = Range,
	          typename = concepts::requires<
	              concepts::models<RandomAccessRange, U>,
	              concepts::models<SizedRange, U>>>
	auto operator[](typename U::index_type const i)
	    const -> typename U::reference
	{
		assert(i <= m_range.size());

		return m_range[m_range.size() - i - 1];
	}

	// Slicable Range.

	template <typename..., typename U = Range,
	          typename =
	              concepts::requires<concepts::models<SlicableRange, U>,
	                                 concepts::models<SizedRange, U>>>
	auto slice(typename U::index_type const i,
	           typename U::index_type const j) const
	    -> retro_range<decltype(::std::declval<U const&>().slice(i, j))>
	{
		assert(i <= j);
		assert(j <= m_range.size());

		using slice_type =
		    decltype(::std::declval<U const&>().slice(i, j));
		auto const size = m_range.size();
		return retro_range<slice_type>{
		    m_range.slice(size - i - 1, size - j - 1)};
	}

	// Readable Range

	// value_type is inherited.

	// Writable Range

	// Handled automatically in `reference`.
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_RETRO_RANGE_HPP
