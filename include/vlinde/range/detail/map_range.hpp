#ifndef VLINDE_RANGE_DETAIL_MAP_RANGE_HPP
#define VLINDE_RANGE_DETAIL_MAP_RANGE_HPP

#include "voider.hpp"

#include <vlinde/concepts/utility.hpp>
#include <vlinde/range/concepts.hpp>

#include <utility>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename Range, typename = concepts::Void>
struct map_range_index_type_mixin
{
};

template <typename Range>
struct map_range_index_type_mixin<Range,
                                  voider<typename Range::index_type>>
{
	using index_type = typename Range::index_type;
};

template <typename R, typename F,
          typename = concepts::requires<concepts::models<ReadableRange, R>>>
struct map_range : map_range_index_type_mixin<R>
{
private:
	R m_range;
	F m_functor;

public:
	explicit map_range(R range, F functor)
	: m_range(::std::move(range)), m_functor(::std::move(functor))
	{
	}

	// Range

	using reference =
	    decltype(m_functor(::std::declval<typename R::reference>()));

	map_range(map_range&&) = default;

	map_range& operator=(map_range&&) = default;

	bool empty() const
	{
		return m_range.empty();
	}

	// Multi-Pass Range

	map_range(map_range const&) = default;

	map_range& operator=(map_range const&) = default;

	// Sized Range

	template <typename..., typename U = R,
	          typename =
	              concepts::requires<concepts::models<SizedRange, U>>>
	auto size() const -> typename U::index_type
	{
		return m_range.size();
	}

	// Forward Range.

	template <typename..., typename U = R,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto front() const -> reference
	{
		return m_functor(m_range.front());
	}

	template <typename..., typename U = R,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto pop_front() -> void
	{
		assert(!this->empty());

		m_range.pop_front();
	}

	// Backward Range.

	template <typename..., typename U = R,
	          typename = concepts::requires<
	              concepts::models<BackwardRange, U>>>
	auto back() const -> reference
	{
		assert(!this->empty());

		return m_functor(m_range.back());
	}

	template <typename..., typename U = R,
	          typename = concepts::requires<
	              concepts::models<BackwardRange, U>>>
	auto pop_back() -> void
	{
		assert(!this->empty());

		m_range.pop_back();
	}

	// Random Access Range.

	template <typename..., typename U = R,
	          typename = concepts::requires<
	              concepts::models<RandomAccessRange, U>>>
	auto operator[](typename U::index_type const i)
	    const -> typename U::reference
	{
		return m_functor(m_range[i]);
	}

	// Slicable Range.

	template <typename..., typename U = R,
	          typename =
	              concepts::requires<concepts::models<SlicableRange, U>,
	                                 concepts::models<SizedRange, U>>>
	auto slice(typename U::index_type const i,
	           typename U::index_type const j)
	    const -> map_range<
	                decltype(::std::declval<U const&>().slice(i, j)), F>
	{
		assert(i <= j);

		using slice_type =
		    decltype(::std::declval<U const&>().slice(i, j));
		return map_range<slice_type, F>{m_range.slice(i, j), m_functor};
	}

	// Readable Range

	using value_type = reference;

	// Writable Range

	// Handled automatically in `reference`.
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_MAP_RANGE_HPP
