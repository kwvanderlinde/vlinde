#ifndef VLINDE_RANGE_DETAIL_FILTER_RANGE_HPP
#define VLINDE_RANGE_DETAIL_FILTER_RANGE_HPP

#include <vlinde/concepts/utility.hpp>
#include <vlinde/range/concepts.hpp>

namespace vlinde
{
namespace range
{
namespace detail
{
template <typename R, typename P,
          typename = concepts::requires<
              concepts::models<ReadableRange, R>,
              tmp::or_<concepts::models<ForwardRange, R>,
                       concepts::models<BackwardRange, R>>>>
class filter_range
{
	R m_range;
	P m_predicate;

	void search_for_next_forward(Range)
	{
	}

	void search_for_next_forward(ForwardRange)
	{
		for (; !m_range.empty() && !m_predicate(m_range.front());
		     m_range.pop_front()) {
		}
	}

	void search_for_next_forward()
	{
		search_for_next_forward(
		    concepts::choose_from<R, ForwardRange, Range>{});
	}

	void search_for_next_backward(Range)
	{
	}

	void search_for_next_backward(BackwardRange)
	{
		for (; !m_range.empty() && !m_predicate(m_range.back());
		     m_range.pop_back()) {
		}
	}

	void search_for_next_backward()
	{
		search_for_next_backward(
		    concepts::choose_from<R, BackwardRange, Range>{});
	}

public:
	filter_range(R range, P predicate)
	: m_range{ ::std::move(range)}, m_predicate(::std::move(predicate))
	{
		this->search_for_next_forward();
		this->search_for_next_backward();
	}

	// Range

	using reference = typename R::reference;

	filter_range(filter_range&&) = default;

	filter_range& operator=(filter_range&&) = default;

	bool empty() const
	{
		return m_range.empty();
	}

	// Multi-Pass Range

	// These are implicitly deleted if `Range` does not have them.

	filter_range(filter_range const&) = default;

	filter_range& operator=(filter_range const&) = default;

	// Forward Range

	template <typename..., typename U = R,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto front() const -> typename U::reference
	{
		return m_range.front();
	}

	template <typename..., typename U = R,
	          typename =
	              concepts::requires<concepts::models<ForwardRange, U>>>
	auto pop_front() -> void
	{
		m_range.pop_front();
		this->search_for_next_forward();
	}

	// Backward Range

	template <typename..., typename U = R,
	          typename = concepts::requires<
	              concepts::models<BackwardRange, U>>>
	auto back() const -> typename U::reference
	{
		return m_range.back();
	}

	template <typename..., typename U = R,
	          typename = concepts::requires<
	              concepts::models<BackwardRange, U>>>
	auto pop_back() -> void
	{
		m_range.pop_back();
		this->search_for_next_backward();
	}

	// Readable Range

	using value_type = typename R::value_type;

	// Writeable Range

	// Handled automaticaly via `reference`.
};
}
}
}

#endif // VLINDE_RANGE_DETAIL_FILTER_RANGE_HPP
