#ifndef VLINDE_RANGE_DROP_HPP
#define VLINDE_RANGE_DROP_HPP

#include <vlinde/range/concepts.hpp>
#include <vlinde/concepts/utility.hpp>

namespace vlinde
{
namespace range
{
/** Skips at most `count` elements of `range` and returns the result.
  *
  * If `range` has no more than `count` elements, then the `range` is
  * simply exhausted before being returned.
  *
  * \tparam Range A [Forward Range](\ref forward_range).
  *
  * \tparam Integral An Integral type.
  *
  * \param range The range to drop elements from.
  *
  * \param count The number of elements to drop.
  *
  * \returns `range` advanced by `count` elements or until it is
  * exhausted.
  *
  * \requires `count >= 0`.
  *
  * \todo Specialize this for Slicable ranges with Dollar.
  */
template <typename Range, typename Integral,
          typename =
              concepts::requires<concepts::models<ForwardRange, Range>>>
Range drop(Range range, Integral count)
{
	for (; count != 0 && !range.empty(); --count) {
		range.pop_front();
	}
	return ::std::move(range);
}
}
}

#endif // VLINDE_RANGE_DROP_HPP
