#ifndef VLINDE_RANGE_RANDOM_RANGE_HPP
#define VLINDE_RANGE_RANDOM_RANGE_HPP

#include "detail/random_range.hpp"

#include <type_traits>

namespace vlinde
{
namespace range
{
template <typename URNG>
auto random_range(URNG && urng)
    -> detail::random_range<typename ::std::remove_cv<
        typename ::std::remove_reference<URNG>::type>::type>
{
	using type =
	    typename ::std::remove_cv<typename ::std::decay<URNG>::type>::type;

	return detail::random_range<type>(::std::forward<URNG>(urng));
}
}
}

#endif // VLINDE_RANGE_RANDOM_RANGE_HPP
