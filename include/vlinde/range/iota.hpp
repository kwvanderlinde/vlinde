#ifndef VLINDE_RANGE_IOTA_HPP
#define VLINDE_RANGE_IOTA_HPP

#include "detail/iota_range.hpp"

#include <vlinde/concepts/numeric.hpp>
#include <vlinde/concepts/utility.hpp>

namespace vlinde
{
namespace range
{
/** Constructs a range of elements from `start` to `end` with step size
  * `step`.
  *
  * \tparam Start An Integral type.
  *
  * \tparam End An Integral type.
  *
  * \tparam Step An Integral type.
  *
  * \param start The first value in the range.
  *
  * \param end The value to indicate the end of the range.
  *
  * \param step The amount to step by.
  *
  * \returns A [Range] which is [Multi Pass], [Bidirectional],
  * [Random Access], [Slicable], [Sized], and [Readable].
  *
  * [Range]: \ref range
  * [Multi Pass]: \ref multi_pass_range
  * [Bidirectional]: \ref bidirectional_range
  * [Random Access]: \ref random_access_range
  * [Slicable]: \ref slicable_range
  * [Sized]: \ref sized_range
  * [Readable]: \ref readable_range
  *
  * \requires `Start` and `End` have a common type, as determined by
  * `::%std::common_type`.
  *
  * \requires `step != 0`.
  *
  * \requires If `step > 0`, then `start >= end`. Else, `end <= start`.
  */
template <typename Start, typename End, typename Step,
          typename = concepts::requires<
              concepts::models<concepts::Integral, Start>,
              concepts::models<concepts::Integral, End>,
              concepts::models<concepts::Integral, Step>>>
auto iota(Start start, End end, Step step)
    -> detail::iota_type<Start, End, Step>
{
	return detail::iota_type<Start, End, Step>(start, end, step);
}

/** Constructs a range of elements from `start` to `end`.
  *
  * The step size is `1` if `start <= end`, or `-1` otherwise.
  *
  * \tparam Start An Integral type.
  *
  * \tparam End An Integral type.
  *
  * \param start The first value in the range.
  *
  * \param end The value to indicate the end of the range.
  *
  * \returns A [Range] which is [Multi Pass], [Bidirectional],
  * [Random Access], [Slicable], [Sized], and [Readable].
  *
  * [Range]: \ref range
  * [Multi Pass]: \ref multi_pass_range
  * [Bidirectional]: \ref bidirectional_range
  * [Random Access]: \ref random_access_range
  * [Slicable]: \ref slicable_range
  * [Sized]: \ref sized_range
  * [Readable]: \ref readable_range
  *
  * \requires `Start` and `End` have a common type, as determined by
  * `::%std::common_type`.
  */
template <typename Start, typename End,
          typename = concepts::requires<
              concepts::models<concepts::Integral, Start>,
              concepts::models<concepts::Integral, End>>>
auto iota(Start start, End end) -> detail::iota_type<Start, End>
{
	return detail::iota_type<Start, End>(start, end);
}

/** Equivalent to `iota<End, End>(0, end)`.
  */
template <typename End, typename = concepts::requires<
                            concepts::models<concepts::Integral, End>>>
auto iota(End end) -> decltype(iota<End, End>(0, end))
{
	return iota<End, End>(0, end);
}
}
}

#endif // VLINDE_RANGE_IOTA_HPP
