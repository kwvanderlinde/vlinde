#ifndef VLINDE_CONTAINERS_ARRAY_HPP
#define VLINDE_CONTAINERS_ARRAY_HPP

#include <vlinde/algorithm/min_max.hpp>
#include <vlinde/algorithm/non_modifying.hpp>
#include <vlinde/memory/default_allocator.hpp>
#include <vlinde/memory/typed_allocator.hpp>
#include <vlinde/range/iterator_range.hpp>

#include <memory>
#include <stdexcept>
#include <utility>

namespace vlinde
{
namespace containers
{
namespace detail
{
template <typename T, typename Allocator>
struct array_base
{
	using allocator_type = ::vlinde::memory::typed_allocator<T, Allocator>;

	allocator_type m_allocator;
	::vlinde::memory::block<T> m_memory;

	array_base(allocator_type allocator) noexcept
	    : m_allocator(::std::move(allocator)),
	      m_memory{nullptr, 0}
	{
	}

	array_base(Allocator allocator) noexcept
	    : m_allocator(::std::move(allocator)),
	      m_memory{nullptr, 0}
	{
	}

	array_base(::std::size_t size, Allocator allocator) noexcept
	    : m_allocator(::std::move(allocator)),
	      m_memory(m_allocator.alloc(size))
	{
	}

	~array_base()
	{
		if (m_memory)
			m_allocator.free(m_memory);
	}
};
}

template <typename T, typename Allocator = ::vlinde::memory::default_allocator>
class array : private detail::array_base<T, Allocator>
{
	using base_type = detail::array_base<T, Allocator>;

public:
	using value_type = T;
	using size_type = ::std::size_t;
	using range_type = ::vlinde::range::iterator_range<T *>;
	using const_range_type = ::vlinde::range::iterator_range<T const *>;

	array()
	: array(Allocator())
	{
	}

	array(Allocator allocator)
	: base_type(::std::move(allocator))
	{
	}

	explicit array(size_type count)
	: array(count, Allocator())
	{
	}

	array(size_type count, T const & value)
	: array(count, value, Allocator())
	{
	}

	array(size_type count, Allocator allocator)
	: array(count, T(), ::std::move(allocator))
	{
	}

	array(size_type count, T const & value, Allocator allocator)
	: base_type(count, ::std::move(allocator))
	{
		::vlinde::memory::uninitialized_fill(this->range(), value);
	}

	array(array const & other)
	: array(other.range())
	{
	}

	array(array && other) noexcept : base_type(::std::move(other.m_allocator))
	{
    using ::std::swap;
		swap(this->m_memory, other.m_memory);
	}

	explicit array(::std::initializer_list<T> init,
	               Allocator allocator = Allocator())
	: array(::vlinde::range::make_iterator_range(init), ::std::move(allocator))
	{
	}

	template <typename MultiPassForwardReadableRange>
	explicit array(MultiPassForwardReadableRange range,
	               Allocator allocator = Allocator())
	: base_type(::vlinde::algorithm::count(range), ::std::move(allocator))
	{
		::vlinde::memory::uninitialized_copy(range, this->range());
	}

	~array()
	{
		// Destroy all elements
		using ::vlinde::algorithm::for_each;

		for_each(this->crange(),
		         [](T const & obj) { ::std::addressof(obj)->~T(); });
	}

	array & operator=(array const & other)
	{
		array temp(other);
		swap(*this, temp);
		return *this;
	}

	array & operator=(array && other) noexcept
	{
		array temp(::std::move(other));
		swap(*this, temp);
		return *this;
	}

	friend void swap(array & lhs, array & rhs) noexcept
	{
		using ::std::swap;

		swap(lhs.m_allocator, rhs.m_allocator);
		swap(lhs.m_memory, rhs.m_memory);
	}

	//////////////////////////////////////
	// Capacity //////////////////////////
	//////////////////////////////////////

	bool empty() const noexcept
	{
		return this->size() == 0;
	}

	size_type size() const noexcept
	{
		if (this->m_memory)
			return this->m_memory.get_length();

		return 0;
	}

	//////////////////////////////////////
	// Ranges ////////////////////////////
	//////////////////////////////////////

	range_type range() noexcept
	{
		return this->m_memory.range();
	}

	const_range_type range() const noexcept
	{
		return this->m_memory.range();
	}

	const_range_type crange() const noexcept
	{
		return this->range();
	}

	//////////////////////////////////////
	// Element Access ////////////////////
	//////////////////////////////////////

	T & at(size_type pos)
	{
		if (!(pos < this->size()))
			throw ::std::out_of_range("::vlinde::containers::array");

		return (*this)[pos];
	}

	T const & at(size_type pos) const
	{
		if (!(pos < this->size()))
			throw ::std::out_of_range("::vlinde::containers::array");

		return (*this)[pos];
	}

	T & operator[](size_type pos) noexcept
	{
		assert(pos < this->size());

		return this->m_memory.get_pointer()[pos];
	}

	T const & operator[](size_type pos) const noexcept
	{
		assert(pos < this->size());

		return this->m_memory.get_pointer()[pos];
	}
};

namespace detail
{
template <typename T, typename Predicate>
bool equality(array<T> const & lhs, array<T> const & rhs,
              Predicate pred) noexcept
{
	return ::vlinde::algorithm::equal(lhs.range(), rhs.range(),
	                                  ::std::move(pred));
}

template <typename T, typename Compare>
bool compare(array<T> const & lhs, array<T> const & rhs,
             Compare compare) noexcept
{
	return ::vlinde::algorithm::lexicographical_compare(lhs.range(), rhs.range(),
	                                                    ::std::move(compare));
}
}

template <typename T>
bool operator==(array<T> const & lhs, array<T> const & rhs) noexcept
{
	return detail::equality(lhs, rhs, ::std::equal_to<T>{});
}

template <typename T>
bool operator!=(array<T> const & lhs, array<T> const & rhs) noexcept
{
	return detail::equality(lhs, rhs, ::std::not_equal_to<T>{});
}

template <typename T>
bool operator<(array<T> const & lhs, array<T> const & rhs) noexcept
{
	return compare(lhs, rhs, ::std::less<T>{});
}

template <typename T>
bool operator<=(array<T> const & lhs, array<T> const & rhs) noexcept
{
	return compare(lhs, rhs, ::std::less_equal<T>{});
}

template <typename T>
bool operator>(array<T> const & lhs, array<T> const & rhs) noexcept
{
	return compare(lhs, rhs, ::std::greater<T>{});
}

template <typename T>
bool operator>=(array<T> const & lhs, array<T> const & rhs) noexcept
{
	return compare(lhs, rhs, ::std::greater_equal<T>{});
}
}
}

#endif // VLINDE_CONTAINERS_ARRAY_HPP
