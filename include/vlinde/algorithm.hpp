#ifndef VLINDE_ALGORITHM_HPP
#define VLINDE_ALGORITHM_HPP

namespace vlinde
{
/** An complement to the C++ Standard Library algorithm, focusing on
 * range-based interfaces.
 */
namespace algorithm
{
}
}

#include "algorithm/min_max.hpp"
#include "algorithm/modifying.hpp"
#include "algorithm/non_modifying.hpp"
#include "algorithm/numeric.hpp"

#endif // VLINDE_ALGORITHM_HPP
