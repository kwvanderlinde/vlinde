#ifndef VLINDE_MEMORY_UTILITY_HPP
#define VLINDE_MEMORY_UTILITY_HPP

#include <vlinde/memory/block.hpp>

#include <cstddef>
#include <memory>
#include <type_traits>

namespace vlinde
{
namespace memory
{
inline bool is_power_of_two(::std::size_t x)
{
	return (x != 0) && !(x & (x - 1));
}

/** Calculates the amount of offest to apply to `buffer` to make it
  * aligned according to `alignment`.
  *
  * \returns The amount of offset to apply to `buffer`.
  */
inline ::std::size_t get_alignment_padding(block<void> buffer,
                                           ::std::size_t alignment)
{
	void * new_pointer = buffer.get_pointer();
	::std::size_t new_size = buffer.get_length();

	// Align a one element buffer.
	::std::align(alignment, 1, new_pointer, new_size);

	return buffer.get_length() - new_size;
}

/** An analogure of `::%std::align` for `block<void>`.
 *
 * In addition to the simple alignment provided by `::%std::align`, this version
 * accepts an `offset` parameter. If the alignment is successful, then
 * `advance(result, offset)` will be aligned with the requested alignment. This
 * may be useful if a certain alignment is needed within an object, say, for a
 * structure holding some metadata followed by a payload which requires special
 * alignment.
 *
 * \pre `offset < size`
 */
inline block<void> align(block<void> mb, ::std::size_t alignment,
                         ::std::size_t size, ::std::size_t offset = 0)
{
	assert(is_power_of_two(alignment));
	assert(size == 0 ? (offset == 0) : (offset < size));

	// We don't need to account for the first `offset` bytes when aligning within
	// the tail of `mb`.
	auto const aligned_size = size - offset;

	// Firstly, move over the offset so we don't try to align that.
	auto aligned_memory = advance(mb, offset);

	// Do a simple alignment of the remaining memory.
	void * new_pointer = aligned_memory.get_pointer();
	::std::size_t new_length = aligned_memory.get_length();
	auto result_ptr =
	    ::std::align(alignment, aligned_size, new_pointer, new_length);

	// Report any failure.
	if (result_ptr == nullptr)
		return block<void>{nullptr, 0};

	// Step back over the offset portion to yield to full and aligned memory.
	aligned_memory = {new_pointer, new_length};
	return regress(aligned_memory, offset);
}

template <typename MultiPassRange, typename T>
void uninitialized_fill(MultiPassRange range, T const & value)
{
	using value_type =
	    typename ::std::decay<typename MultiPassRange::reference>::type;
	auto range_copy = range;

	::std::size_t fill_count = 0;
	try {
		for (; !range.empty(); range.pop_front(), ++fill_count) {
			::new (static_cast<void *>(::std::addressof(range.front())))
			    value_type(value);
		}
		return range;
	} catch (...) {
		// Delete all elements we've constructed so far.
		range = range_copy;
		for (::std::size_t i = 0; i < fill_count; range.pop_front(), ++i) {
			::std::addressof(range.front())->~value_type();
		}

		throw;
	}
}

template <typename Range, typename MultiPassRange>
MultiPassRange uninitialized_copy(Range input, MultiPassRange range)
{
	using value_type =
	    typename ::std::decay<typename MultiPassRange::reference>::type;
	auto range_copy = range;

	::std::size_t fill_count = 0;
	try {
		for (; !(range.empty() || input.empty());
		     range.pop_front(), input.pop_front(), ++fill_count) {
			::new (static_cast<void *>(::std::addressof(range.front())))
			    value_type(input.front());
		}
	} catch (...) {
		// Delete all elements we've constructed so far.
		range = range_copy;
		for (::std::size_t i = 0; i < fill_count; range.pop_front(), ++i) {
			::std::addressof(range.front())->~value_type();
		}

		throw;
	}

	return range;
}
}
}

#endif // VLINDE_MEMORY_UTILITY_HPP
