#ifndef VLINDE_MEMORY_TYPED_ALLOCATOR_HPP
#define VLINDE_MEMORY_TYPED_ALLOCATOR_HPP

#include "detail/typed_allocator_impl.hpp"

#include <vlinde/memory/default_allocator.hpp>

namespace vlinde
{
namespace memory
{
template <typename T, typename Allocator = default_allocator>
using typed_allocator = detail::typed_allocator_impl<Allocator, T>;
}
}

#endif // VLINDE_MEMORY_TYPED_ALLOCATOR_HPP
