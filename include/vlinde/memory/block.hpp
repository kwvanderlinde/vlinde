#ifndef VLINDE_MEMORY_BLOCK_HPP
#define VLINDE_MEMORY_BLOCK_HPP

#include <vlinde/range/iterator_range.hpp>

#include <cassert>
#include <cstddef>

namespace vlinde
{
namespace memory
{
template <typename T>
class block;

namespace detail
{
template <typename T>
class block_range_mixin
{
	block<T> & as_child() noexcept
	{
		return *static_cast<block<T> *>(this);
	}

	block<T> const & as_child() const noexcept
	{
		return *static_cast<block<T> const *>(this);
	}

public:
	using range_type = ::vlinde::range::iterator_range<T *>;

	using const_range_type = ::vlinde::range::iterator_range<T const *>;

	/** Returns a range of successive slots in the array.
	  *
	  * The underlying memory is not guaranteed to be initialized. In
	  * fact, the caller may initialize or uninitialize any portion of
	  * the range at any time. It is the caller's responsibility to
	  * initialize the elements of the range before use, if necessary.
	  */
	range_type range() noexcept
	{
		block<T> & child = this->as_child();

		auto const first = child.get_pointer();
		auto const last = first + child.get_length();
		return range_type(first, last);
	}

	/** Returns a range of successive slots in the array.
	  *
	  * The underlying memory is not guaranteed to be initialized. In
	  * fact, the caller may initialize or uninitialize any portion of
	  * the range at any time. It is the caller's responsibility to
	  * initialize the elements of the range before use, if necessary.
	  */
	const_range_type range() const
	{
		return this->crange();
	}

	/** Returns a range of successive slots in the array.
	  *
	  * The underlying memory is not guaranteed to be initialized. In
	  * fact, the caller may initialize or uninitialize any portion of
	  * the range at any time. It is the caller's responsibility to
	  * initialize the elements of the range before use, if necessary.
	  */
	const_range_type crange() const
	{
		block<T> const & child = this->as_child();

		auto const first = child.get_pointer();
		auto const last = first + child.get_length();
		return const_range_type(first, last);
	}
};

// A range interface does not make sense for a void block.
template <>
class block_range_mixin<void>
{
};
}

/** A POD type representing a range of memory.
  *
  * \todo Consider writing a debug version with a shared implementation which
  * may be invalidated. This could be useful for testing the usage of
  * allocators.
  */
template <typename T>
class block : public detail::block_range_mixin<T>
{
	T * m_pointer;
	::std::size_t m_length;

public:
	block() noexcept = default;

	block(block const &) noexcept = default;

	block(block &&) noexcept = default;

	block(T * pointer, ::std::size_t length) noexcept : m_pointer(pointer),
	                                                    m_length(length)
	{
	}

	~block() = default;

	block & operator=(block const &) noexcept = default;

	block & operator=(block &&) noexcept = default;

	explicit operator bool() const noexcept
	{
		return m_pointer != nullptr;
	}

	T * get_pointer() const noexcept
	{
		return m_pointer;
	}

	::std::size_t get_length() const noexcept
	{
		return m_length;
	}
};

/** Reduces the block by `bytes` bytes by advancing the pointer.
  *
  * \pre `get_pointer() != nullptr`
  * \pre `bytes <= get_length()`
  */
inline block<void> advance(block<void> const & mb, ::std::size_t bytes) noexcept
{
	assert(mb);
	assert(bytes <= mb.get_length());

	void * new_pointer = reinterpret_cast<char *>(mb.get_pointer()) + bytes;
	::std::size_t new_length = mb.get_length() - bytes;

	return {new_pointer, new_length};
}

// Since a void block is the most raw access to storage, we allow the unsafe
// operation known as regress.

/** Expands the block by `bytes` bytes by regressing the pointer.
  *
  * \pre `get_pointer() != nullptr`
  * \warning Unsafe! No guarantee can be made that the new block lies within a
  * single allocation.
  */
inline block<void> regress(block<void> const & mb, ::std::size_t bytes) noexcept
{
	assert(mb);

	void * new_pointer = reinterpret_cast<char *>(mb.get_pointer()) - bytes;
	::std::size_t new_length = mb.get_length() + bytes;

	return {new_pointer, new_length};
}
}
}

#include <type_traits>

static_assert(::std::is_pod<::vlinde::memory::block<void>>::value,
              "block<void> is not a pod type");

#endif // VLINDE_MEMORY_BLOCK_HPP
