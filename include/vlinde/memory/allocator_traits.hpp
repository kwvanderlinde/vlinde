#ifndef VLINDE_MEMORY_ALLOCATOR_TRAITS_HPP
#define VLINDE_MEMORY_ALLOCATOR_TRAITS_HPP

#include "detail/allocator_traits_impl.hpp"

#include <vlinde/memory/block.hpp>

#include <cstddef>

namespace vlinde
{
namespace memory
{
/**
 * Defines the full allocator interface, with default implementations
 * for any missing methods.
 *
 * An `Allocator` must only provide the methods `alloc` and `free`, in
 * addition to being an equality comparable value type. However, on top
 * of these basic methods (most notably, `alloc`), it is possible to
 * implement an aligned version of `alloc`, as well as trivial (though
 * not very helpful) implementations of `resize` and other methods.
 *
 * For any method exposed in `allocator_traits<Allocator>`, the
 * implementation in `Allocator` is preferred, if it exists. Otherwise,
 * a default implementation is used.
 *
 * \todo require Allocator to be an Allocator.
 */
template <typename Allocator>
struct allocator_traits : detail::aligned_alloc::mixin<Allocator>,
                          detail::resize::mixin<Allocator>,
                          detail::resolve_internal::mixin<Allocator>
{
	using allocator_type = Allocator;

	static block<void> alloc(allocator_type & allocator,
	                         ::std::size_t size) noexcept
	{
		block<void> const result = allocator.alloc(size);

		// Post-condition
		assert(result.get_pointer() == nullptr || result.get_length() == size);

		return result;
	}

	static void free(allocator_type & allocator, block<void> block) noexcept
	{
		// Pre-condition
		assert(block.get_pointer() != nullptr);

		allocator.free(block);
	}
};
}
}

#endif // VLINDE_MEMORY_ALLOCATOR_TRAITS_HPP
