#ifndef VLINDE_MEMORY_ALGINED_ALLOCATOR_HPP
#define VLINDE_MEMORY_ALGINED_ALLOCATOR_HPP

#include "detail/aligned_allocator_impl.hpp"

namespace vlinde
{
namespace memory
{

/** Adapts an Allocator to become an Alignment Aware Allocator.
  *
  * `aligned_allocator<Allocator>` additionally models Allocator Value
  * if `Allocator` does.
  */
template <typename A>
using aligned_allocator =
    typename detail::aligned_allocator_impl<A>::type;
}
}

#endif // VLINDE_MEMORY_ALGINED_ALLOCATOR_HPP
