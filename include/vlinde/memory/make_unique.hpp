#ifndef VLINDE_MEMORY_MAKEUNIQUE
#define VLINDE_MEMORY_MAKEUNIQUE

// From
// http://stackoverflow.com/questions/10149840/c-arrays-and-make-unique?lq=1
#include <type_traits>
#include <utility>
#include <memory>

namespace vlinde
{
namespace memory
{
/** Memory-safe analogue of `::%std::make_shared` for
  * `::%std::unique_ptr`.
  *
  * `make_unique` is a simple wrapper around the constructor of
  * `::%std::unique_ptr`. However, using `make_unique` is always
  * exception and memory safe, while explicitly using `new` and passing
  * the result to `::%std::unique_ptr`'s constructor may not be.
  *
  * The `new` operator is called using brace initialization, so
  * aggregates without a constructor may be used with `make_unique`.
  *
  * \tparam T
  * Specifies that `make_unique` should create a
  *`::%std::unique_ptr<T>`.
  *
  * \tparam Args
  * The types of the arguments to be forwarded to `T`'s constructor.
  *
  * \param args
  * The arguments to be forwarded to `T`'s constructor. These are used
  * to create a `T*`, which is immediately handed to the constructor of
  * `::%std::unique_ptr<T>`.
  *
  * @return
  * A new `::%std::unique_ptr<T>`, where the encapsulated pointer is
  * constructed using `args...`.
  */
template <class T, class... Args>
typename ::std::enable_if<!::std::is_array<T>::value,
                          ::std::unique_ptr<T>>::type
make_unique(Args&&... args)
{
  return ::std::unique_ptr<T>(new T (::std::forward<Args>(args)...));
}

/** Memory-safe analogue of `::%std::make_shared` for
  *`::%std::unique_ptr`, specialized for arrays.
  *
  * `make_unique` is a simple wrapper around the constructor of
  * `::%std::unique_ptr`. However, using `make_unique` is always
  * exception and memory safe, while explicitly using `new` and passing
  * the result to `::%std::unique_ptr`'s constructor may not be.
  *
  * \tparam T
  * Specifies that `make_unique` should create a `::%std::unique<T>`.
  * `T` is an array type.
  *
  * \param n
  * The size of the array to create.
  *
  * \return
  * A new `::%std::unique_ptr<T>`, where the encapsulated array has
  * length `n`.
  */
template <class T>
typename ::std::enable_if<::std::is_array<T>::value,
                          ::std::unique_ptr<T>>::type
make_unique(::std::size_t n)
{
	// The underlying type of the array.
	using type = typename ::std::remove_extent<T>::type;
	return ::std::unique_ptr<T>(new type[n]);
}

/** Convenience function for copying a `::%std::unique_ptr` which
  * manages an array of data.
  *
  * This function is useful for reducing boilerplate code necessary to
  * copy the data managed by a `::%std::unique_ptr`.
  *
  * \tparam T
  * The array element type to copy.
  *
  * \param ptr
  * The `::%std::unique_ptr` to copy.
  *
  * \param length
  * The number of elements to copy. `length` must be no more than the
  * length of the array managed by `ptr`.
  *
  * \return
  * If `ptr == nullptr`, then `nullptr`. Otherwise, returns a
  * `::%std::unique_ptr` which manages a copy of the array managed by
  * `ptr`.
  */
template <typename T>
::std::unique_ptr<T[]> copy_unique(::std::unique_ptr<T[]> const& ptr,
                                   ::std::size_t length)
{
	if (!ptr)
		return nullptr;

	auto new_ptr = ::vlinde::memory::make_unique<T[]>(length);
	::std::copy(ptr.get(), ptr.get() + length, new_ptr.get());
	return new_ptr;
}

/** Convenience function for copying a `::%std::unique_ptr` which
  * manages a single value.
  *
  * This function is useful for reducing boilerplate code necessary to
  * copy the data managed a `::%std::unique_ptr`.
  *
  * \tparam T
  * The managed type to copy.
  *
  * \param ptr
  * The `::%std::unique_ptr` to copy.
  *
  * \return
  * If `ptr == nullptr`, then returns `nullptr`. Otherwise, returns a
  * `::%std::unique_ptr<T>` which manages a copy of the data managed by
  * `ptr`.
  */
template <typename T>
::std::unique_ptr<T> copy_unique(::std::unique_ptr<T> const& ptr)
{
	if (!ptr)
		return nullptr;

	return ::vlinde::memory::make_unique<T>(*ptr);
}
}
}
#endif // VLINDE_MEMORY_MAKEUNIQUE
