#ifndef VLINDE_MEMORY_ALLOCATOR_CONCEPTS_HPP
#define VLINDE_MEMORY_ALLOCATOR_CONCEPTS_HPP

#include <vlinde/concepts.hpp>
#include <vlinde/memory/block.hpp>

#include <cstddef>

namespace vlinde
{
namespace memory
{
struct Allocator
    : concepts::refines<concepts::CopyConstructible, concepts::CopyAssignable,
                        concepts::Destructible, concepts::EqualityComparable,
                        concepts::Swappable>
{
	template <typename X>
	static auto requires(X & r = ::std::declval<X &>(), ::std::size_t size = 0,
	                     block<void> mb = {})
	    -> decltype(concepts::returns<block<void>>(r.alloc(size)), r.free(mb));
};
}
}

#endif // VLINDE_MEMORY_ALLOCATOR_CONCEPTS_HPP
