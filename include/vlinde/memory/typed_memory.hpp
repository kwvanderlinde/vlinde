#ifndef VLINDE_MEMORY_TYPED_MEMORY
#define VLINDE_MEMORY_TYPED_MEMORY

#include <vlinde/range/iterator_range.hpp>

namespace vlinde
{
namespace memory
{
/** A typed block of memory.
  *
  * `typed_memory<T>` is isomorphic to `block<void>`, but is imbued
  * with type information and higher level operations. A
  *`typed_memory<T>` is valid precisely when the corresponding
  * `block<void>` is valid.
  *
  * Conceptually, `typed_memory<T>` is an array of `T`. However, not
  * all elements of the array are guaranteed to be constructed. The
  * intent of `typed_memory<T>` is to be low level storage for data
  * structures such as vectors.
  */
template <typename T>
class typed_memory
{
private:
	block<void> m_memory;

public:
	typed_memory(typed_memory const&) = default;
	typed_memory& operator=(typed_memory const&) = default;

	/** Converts an untyped block of memory into a typed array.
	  */
	typed_memory(block<void> const& untyped_block)
	: m_memory(untyped_block)
	{
	}

	/** Converts typed memory to untyped memory.
	  */
	operator block<void>() const
	{
		return m_memory;
	}

	/** Accesses the slot at index `index`.
	  *
	  * The returned reference is not guaranteed to refer to initialized
	  * memory. It is the caller's responsibility to ensure that the
	  * memory is initialized. Indeed, the caller may initialize and
	  * uninitialize memory at will.
	  */
	T& operator[](::std::size_t index)
	{
		return reinterpret_cast<T*>(m_memory.get_pointer())[index];
	}

	T const& operator[](::std::size_t index) const
	{
		return reinterpret_cast<T*>(m_memory.get_pointer())[index];
	}

	::std::size_t size() const
	{
		return m_memory.get_length() / sizeof(T);
	}

	using range_type = ::vlinde::range::iterator_range<T*>;

	using const_range_type = ::vlinde::range::iterator_range<T const*>;

	/** Returns a range of successive slots in the array.
	  *
	  * The underlying memory is not guaranteed to be initialized. In
	  * fact, the caller may initialize or uninitialize any portion of
	  * the range at any time. It is the caller's responsibility to
	  * initialize the elements of the range before use, if necessary.
	  */
	range_type range()
	{
		auto const first = reinterpret_cast<T*>(m_memory.get_pointer());
		auto const last = first + this->size();
		return range_type(first, last);
	}

	/** Returns a range of successive slots in the array.
	  *
	  * The underlying memory is not guaranteed to be initialized. In
	  * fact, the caller may initialize or uninitialize any portion of
	  * the range at any time. It is the caller's responsibility to
	  * initialize the elements of the range before use, if necessary.
	  */
	const_range_type range() const
	{
		return this->crange();
	}

	/** Returns a range of successive slots in the array.
	  *
	  * The underlying memory is not guaranteed to be initialized. In
	  * fact, the caller may initialize or uninitialize any portion of
	  * the range at any time. It is the caller's responsibility to
	  * initialize the elements of the range before use, if necessary.
	  */
	const_range_type crange() const
	{
		auto const first = reinterpret_cast<T const*>(m_memory.get_pointer());
		auto const last = first + this->size();
		return const_range_type(first, last);
	}
};
}
}

#endif // VLINDE_MEMORY_TYPED_MEMORY
