#ifndef VLINDE_MEMORY_DETAIL_ALLOCATOR_TRAITS_COMMON_HPP
#define VLINDE_MEMORY_DETAIL_ALLOCATOR_TRAITS_COMMON_HPP

#include <type_traits>

namespace vlinde
{
namespace memory
{
namespace detail
{
// This could potentially be expanded to have non_member as well.
enum class impl_kind { member, none };

using member_impl =
    ::std::integral_constant<impl_kind, impl_kind::member>;

using no_impl = ::std::integral_constant<impl_kind, impl_kind::none>;
}
}
}

#endif // VLINDE_MEMORY_DETAIL_ALLOCATOR_TRAITS_COMMON_HPP
