#ifndef VLINDE_MEMORY_DETAIL_ALIGNED_ALLOCATOR_IMPL_HPP
#define VLINDE_MEMORY_DETAIL_ALIGNED_ALLOCATOR_IMPL_HPP

#include <vlinde/concepts/utility.hpp>
#include <vlinde/memory/allocator_concepts.hpp>
#include <vlinde/memory/allocator_traits.hpp>
#include <vlinde/memory/block.hpp>
#include <vlinde/memory/utility.hpp>
#include <vlinde/tmp/utility.hpp>

#include <functional>

namespace vlinde
{
namespace memory
{
namespace detail
{
template <typename A, typename = concepts::Void>
struct aligned_allocator_impl;

// Just alias the underlying allocator if it's already Alignment Aware.
template <typename A>
struct aligned_allocator_impl<
    A, concepts::requires<typename allocator_traits<A>::has_aligned_alloc>>
{
	using type = A;
};

// An implementation of aligned allocation on top of any old Allocator.
template <typename A, typename>
struct aligned_allocator_impl
{
	class type
	{
	private:
		A m_allocator;

	public:
		template <typename... Args,
		          typename = tmp::disable_as_copy_constructor<type, Args...>>
		type(Args &&... args)
		: m_allocator(::std::forward<Args>(args)...)
		{
		}

		type(type const &) noexcept = default;

		type(type &&) noexcept = default;

		~type() = default;

		type & operator=(type const &) noexcept = default;

		type & operator=(type &&) noexcept = default;

		bool operator==(type const & other) const noexcept
		{
			return m_allocator == other.m_allocator;
		}

		bool operator!=(type const & other) const noexcept
		{
			return m_allocator != other.m_allocator;
		}

		block<void> alloc(::std::size_t const size) noexcept
		{
			// Perform the allocation, using all of the returned memory.
			return alloc_impl(size, [size](block<void> mb) {
				// Restrict the size of the memory to the requested amount.
				return block<void>{mb.get_pointer(), size};
			});
		}

		void free(block<void> mb) noexcept
		{
			block<void> const header = get_header(mb);

			m_allocator.free(header);
		}

		block<void> aligned_alloc(::std::size_t size, ::std::size_t alignment,
		                          ::std::size_t offset) noexcept
		{
			assert(is_power_of_two(alignment));
			assert(size == 0 ? (offset == 0) : (offset < size));

			// The total size required to guarantee that alignment is possible.
			::std::size_t const allocation_size = size + (alignment - 1);

			// Perform the allocation, and select an aligned portion of the memory.
			using ::std::placeholders::_1;
			return alloc_impl(allocation_size, [&](block<void> mb) {
				mb = align(mb, alignment, size, offset);
				// Restrict the size of the memory to the requested amount.
				return block<void>{mb.get_pointer(), size};
			});
		}

		block<void> resize(block<void> user_allocation, ::std::size_t size) noexcept
		{
			// Shrinking is an easy case.
			if (size <= user_allocation.get_length())
				return {user_allocation.get_pointer(), size};

			// Expansion requires grabbing the header to see if we can resize.
			auto const header = get_header(user_allocation);

			char * const allocation_end =
			    reinterpret_cast<char *>(header.get_pointer()) + header.get_length();
			char * const user_start =
			    reinterpret_cast<char *>(user_allocation.get_pointer());

			// This is the most the user allocation can be expanded to.
			::std::ptrdiff_t const signed_max_length = allocation_end - user_start;
			assert(signed_max_length >= 0);
			::std::size_t const max_length =
			    static_cast<::std::size_t>(signed_max_length);

			if (max_length <= size)
				// Can't expand the memory to `size`. Return `max_length` as a hint.
				return {nullptr, max_length};

			// We are able expand the memory, so return this to the user.
			return {user_allocation.get_pointer(), max_length};
		}

	private:
		/* An allocation is laid out as:
		 *
		 * 1. possible padding.
		 * 2. A `block<void>` header (possibly unaligned).
		 * 3. The returned result of an allocation.
		 * 4. possible padding.
		 *
		 * The header describes the underlying allocation, *not* the allocation
		 * returned to the user.
		 *
		 * \param required_size The size needed to fit the data and any alignment
		 * requirements.
		 *
		 * \param strategy A function which select a portion of the allocated
		 * memory. The result is the user's memory.
		 *
		 * \return The result of applying `strategy` to the allocated memory.
		 */
		template <typename Strategy>
		block<void> alloc_impl(::std::size_t allocation_size,
		                       Strategy strategy) noexcept
		{
			// Add room for the header.
			allocation_size += sizeof(block<void>);

			// Allocate the requested memory.
			block<void> const header = m_allocator.alloc(allocation_size);

			// Report any failure.
			if (!header)
				return header;

			// Select a portion of the allocated memory. Be sure to leave space in
			// front for the header.
			auto const user_memory = strategy(advance(header, sizeof(header)));

			// Report any failure.
			if (!user_memory)
				return user_memory;

			assert(::std::less<void *>{}(header.get_pointer(),
			                             user_memory.get_pointer()));
			assert(user_memory.get_length() <= allocation_size);

			// Write the header just before the user's memory.
			::std::memcpy(regress(user_memory, sizeof(header)).get_pointer(), &header,
			              sizeof(header));

			// Yield the user's memory.
			return user_memory;
		}

		block<void> get_header(block<void> mb) noexcept
		{
			block<void> header;

			// Retrieve the header. We need to memcpy into a local in order to ensure
			// alignment of the `block<void>`.
			::std::memcpy(&header, regress(mb, sizeof(header)).get_pointer(),
			              sizeof(header));

			{
#ifndef NDEBUG
				// It must be the case that the user allocation lives entirely within
				// the base allocation.

				void * const base_start = header.get_pointer();
				void * const user_start = mb.get_pointer();
				// The allocations cannot share a start, since the header comes before
				// the user allocation.
				assert(::std::less<void *>{}(base_start, user_start));

				char * const base_end =
				    reinterpret_cast<char *>(base_start) + header.get_length();
				char * const user_end =
				    reinterpret_cast<char *>(user_start) + mb.get_length();
				// The allocations can share an end, since no metadata exists after the
				// user allocation.
				assert(::std::greater_equal<char *>{}(base_end, user_end));
#endif // NDEBUG
			}

			return header;
		}
	};
};
}
}
}

#endif // VLINDE_MEMORY_DETAIL_ALIGNED_ALLOCATOR_IMPL_HPP
