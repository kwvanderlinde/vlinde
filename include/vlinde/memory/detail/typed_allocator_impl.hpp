#ifndef VLINDE_MEMORY_DETAIL_TYPED_ALLOCATOR_IMPL_HPP
#define VLINDE_MEMORY_DETAIL_TYPED_ALLOCATOR_IMPL_HPP

#include <vlinde/memory/aligned_allocator.hpp>
#include <vlinde/memory/block.hpp>

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace memory
{
namespace detail
{
template <typename Allocator, typename T>
class typed_allocator_base
{
	using traits = allocator_traits<Allocator>;

protected:
	Allocator m_allocator;

	typed_allocator_base(Allocator allocator)
	: m_allocator(::std::move(allocator))
	{
	}

	typed_allocator_base(typed_allocator_base const &) = default;

	typed_allocator_base(typed_allocator_base &&) = default;

	~typed_allocator_base() = default;

	typed_allocator_base & operator=(typed_allocator_base const &) = default;

	typed_allocator_base & operator=(typed_allocator_base &&) = default;

	bool operator==(typed_allocator_base const & rhs) const noexcept
	{
		return m_allocator == rhs.m_allocator;
	}

	friend void swap(typed_allocator_base const & lhs,
	                 typed_allocator_base const & rhs) noexcept
	{
		swap(lhs.m_allocator, rhs.m_allocator);
	}

	void free(block<T> typed_block) noexcept
	{
		::std::size_t size = sizeof(T) * typed_block.get_length();
		auto untyped_pointer = reinterpret_cast<void *>(typed_block.get_pointer());
		block<void> untyped_block(untyped_pointer, size);
		traits::free(this->m_allocator, untyped_block);
	}
};

template <typename Allocator, typename T>
bool operator!=(typed_allocator_base<Allocator, T> const & lhs,
                typed_allocator_base<Allocator, T> const & rhs) noexcept
{
	return !(lhs == rhs);
}

/** \internal
  *
  * \brief Implemetation of a typed allocator for types with alignment
  * restrictions.
  *
  * \todo Implement alignment.
  */
template <typename Allocator, typename T, typename = void>
class typed_allocator_impl
    : private typed_allocator_base<aligned_allocator<Allocator>, T>
{
  using base_type = typed_allocator_base<aligned_allocator<Allocator>, T>;
	using allocator_type = aligned_allocator<Allocator>;
	using traits = allocator_traits<allocator_type>;

public:
	typed_allocator_impl(Allocator allocator)
	: typed_allocator_base<allocator_type, T>(::std::move(allocator))
	{
	}

	typed_allocator_impl(typed_allocator_impl const &) = default;

	typed_allocator_impl(typed_allocator_impl &&) = default;

	~typed_allocator_impl() = default;

	typed_allocator_impl & operator=(typed_allocator_impl const &) = default;

	typed_allocator_impl & operator=(typed_allocator_impl &&) = default;

	block<T> alloc(::std::size_t count) noexcept
	{
		::std::size_t size = sizeof(T) * count;
		auto untyped_block =
		    traits::aligned_alloc(this->m_allocator, size, alignof(T));
		auto typed_pointer = reinterpret_cast<T *>(untyped_block.get_pointer());

		return {typed_pointer, count};
	}

  using base_type::free;
};

template <typename Allocator, typename T>
class typed_allocator_impl<Allocator, T,
                           typename ::std::enable_if<alignof(T) == 1u>::type>
    : private typed_allocator_base<Allocator, T>
{
  using base_type = typed_allocator_base<Allocator, T>;
	using allocator_type = Allocator;
	using traits = allocator_traits<allocator_type>;

public:
	typed_allocator_impl(Allocator allocator)
	: typed_allocator_base<allocator_type, T>(::std::move(allocator))
	{
	}

	typed_allocator_impl(typed_allocator_impl const &) = default;

	typed_allocator_impl(typed_allocator_impl &&) = default;

	~typed_allocator_impl() = default;

	typed_allocator_impl & operator=(typed_allocator_impl const &) = default;

	typed_allocator_impl & operator=(typed_allocator_impl &&) = default;

	block<T> alloc(::std::size_t count) noexcept
	{
		::std::size_t size = sizeof(T) * count;
		auto untyped_block = traits::alloc(this->m_allocator, size);
		auto typed_pointer = reinterpret_cast<T *>(untyped_block.get_pointer());

		return {typed_pointer, count};
	}

  using base_type::free;
};

template <typename T, typename Allocator>
bool operator!=(typed_allocator_impl<Allocator, T> const & lhs,
                typed_allocator_impl<Allocator, T> const & rhs) noexcept
{
	return !(lhs == rhs);
}
}
}
}

#endif // VLINDE_MEMORY_DETAIL_TYPED_ALLOCATOR_IMPL_HPP
