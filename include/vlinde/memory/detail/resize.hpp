#ifndef VLINDE_MEMORY_DETAIL_RESIZE_HPP
#define VLINDE_MEMORY_DETAIL_RESIZE_HPP

#include "allocator_traits_common.hpp"

#include <vlinde/memory/block.hpp>
#include <vlinde/utility/compare.hpp>

#include <cstddef>
#include <utility>

namespace vlinde
{
namespace memory
{
namespace detail
{
namespace resize
{
template <typename Allocator>
auto member_test(int) -> decltype(
    ::std::declval<Allocator &>().resize(::std::declval<block<void>>(),
                                         ::std::declval<::std::size_t>()),
    member_impl{});

template <typename Allocator>
auto member_test(...) -> no_impl;

template <typename Allocator>
using impl_type = decltype(member_test<Allocator>(0));

template <typename Allocator, typename ImplType = impl_type<Allocator>>
struct mixin;

template <typename Allocator>
struct mixin<Allocator, member_impl>
{
	using has_resize = ::std::true_type;

	static auto resize(Allocator & allocator, block<void> mb,
	                   ::std::size_t size) noexcept -> block<void>
	{
		// Pre-conditions.
		assert(mb.get_pointer() != nullptr);

		block<void> const result = allocator.resize(mb, size);

		// Post-conditions.
		if (result.get_pointer() != nullptr) {
			using vlinde::utility::compare;

			assert(result.get_length() == size);
			assert(compare(size, mb.get_length()) ==
			       compare(result.get_length(), mb.get_length()));
		}

		return result;
	}
};

template <typename Allocator>
struct mixin<Allocator, no_impl>
{
	using has_resize = ::std::false_type;

	static auto resize(Allocator &, block<void> mb,
	                   ::std::size_t) noexcept -> block<void>
	{
		// Always fails.
		return block<void>{nullptr, mb.get_length()};
	}
};
}
}
}
}

#endif // VLINDE_MEMORY_DETAIL_RESIZE_HPP
