#ifndef VLINDE_MEMORY_DETAIL_ALIGNED_ALLOC_HPP
#define VLINDE_MEMORY_DETAIL_ALIGNED_ALLOC_HPP

#include "allocator_traits_common.hpp"

#include <vlinde/memory/block.hpp>
#include <vlinde/memory/utility.hpp>

#include <cstddef>
#include <utility>

namespace vlinde
{
namespace memory
{
namespace detail
{
namespace aligned_alloc
{
template <typename Allocator>
auto member_test(int) -> decltype(::std::declval<Allocator &>().aligned_alloc(
                                      ::std::declval<::std::size_t>(),
                                      ::std::declval<::std::size_t>(),
                                      ::std::declval<::std::size_t>()),
                                  member_impl{});

template <typename Allocator>
auto member_test(...) -> no_impl;

template <typename Allocator>
using impl_type = decltype(member_test<Allocator>(0));

template <typename Allocator, typename ImplType = impl_type<Allocator>>
struct mixin;

template <typename Allocator>
struct mixin<Allocator, member_impl>
{
	using has_aligned_alloc = ::std::true_type;

	static auto aligned_alloc(Allocator & allocator, ::std::size_t size,
	                   ::std::size_t alignment,
	                   ::std::size_t offset = 0) noexcept -> block<void>
	{
		// Pre-conditions
		assert(is_power_of_two(alignment));
		assert((size == 0) || (offset < size));

		block<void> const result = allocator.aligned_alloc(size, alignment, offset);

		// Post-conditions
		assert(result.get_length() == size);
    // No portable way to check the alignment yet.

		return result;
	}
};

template <typename Allocator>
struct mixin<Allocator, no_impl>
{
	using has_aligned_alloc = ::std::false_type;
};
}
}
}
}

#endif // VLINDE_MEMORY_DETAIL_ALIGNED_ALLOC_HPP
