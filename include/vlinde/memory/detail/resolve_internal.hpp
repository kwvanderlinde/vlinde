#ifndef VLINDE_MEMORY_DETAIL_RESOLVE_INTERNAL_HPP
#define VLINDE_MEMORY_DETAIL_RESOLVE_INTERNAL_HPP

#include "allocator_traits_common.hpp"

#include <vlinde/memory/block.hpp>

#include <utility>

namespace vlinde
{
namespace memory
{
namespace detail
{
namespace resolve_internal
{
template <typename Allocator>
auto member_test(int) -> decltype(
    ::std::declval<Allocator &>().resolve_internal(::std::declval<void *>()),
    member_impl{});

template <typename Allocator>
auto member_test(...) -> no_impl;

template <typename Allocator>
using impl_type = decltype(member_test<Allocator>(0));

template <typename Allocator, typename ImplType = impl_type<Allocator>>
struct mixin;

template <typename Allocator>
struct mixin<Allocator, member_impl>
{
	using has_resolve_internal = ::std::true_type;

	static auto resolve_internal(Allocator & allocator,
	                             void * pointer) noexcept -> block<void>
	{
		return allocator.resolve_internal(pointer);
	}
};

template <typename Allocator>
struct mixin<Allocator, no_impl>
{
	using has_resolve_internal = ::std::false_type;
};
}
}
}
}

#endif // VLINDE_MEMORY_DETAIL_RESOLVE_INTERNAL_HPP
