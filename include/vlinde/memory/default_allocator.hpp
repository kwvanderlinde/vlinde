#ifndef VLINDE_MEMORY_DEFAULT_ALLOCATOR_HPP
#define VLINDE_MEMORY_DEFAULT_ALLOCATOR_HPP

#include <vlinde/memory/block.hpp>

#include <cstddef>
#include <new>

namespace vlinde
{
namespace memory
{
/** An analogue of `::%std::allocator` which implements the VLinde
  * Library's Allocator Value concept.
  */
class default_allocator
{
public:
	block<void> alloc(::std::size_t size) noexcept
	{
		return {::operator new (size, ::std::nothrow_t{}), size};
	}

	void free(block<void> mb) noexcept
	{
		::operator delete(mb.get_pointer());
	}
};

inline void swap(default_allocator &, default_allocator &) noexcept
{
}

/** Equality operator.
  *
  * \returns `true`.
  */
inline bool operator==(default_allocator const &,
                       default_allocator const &) noexcept
{
	return true;
}

/** Inequality operator.
  *
  * \returns `false`.
  */
inline bool operator!=(default_allocator const &,
                       default_allocator const &) noexcept
{
	return false;
}
}
}

#endif // VLINDE_MEMORY_DEFAULT_ALLOCATOR_HPP
