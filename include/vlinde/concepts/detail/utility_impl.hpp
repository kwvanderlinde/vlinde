#ifndef VLINDE_CONCEPTS_DETAIL_IMPL_HPP
#define VLINDE_CONCEPTS_DETAIL_IMPL_HPP

#include <vlinde/tmp/typelist.hpp>

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace concepts
{
namespace detail
{
// Utility to check whether the concept is satisfied.
template <typename Concept, typename... Ts>
auto models_check(int) -> decltype(Concept::template requires<Ts...>(),
                                   ::std::true_type{});

template <typename Concept, typename... Ts>
auto models_check(...) -> std::false_type;

template <typename Concept, typename... Ts>
struct models_impl<Concept, ::vlinde::tmp::typelist<Ts...>>
{
	// First, check that each base concept is satisfied.
	template <typename BaseConcept>
	using mapper = models<BaseConcept, Ts...>;
	using all_base_concepts_satisfied =
	    tmp::all<tmp::map<mapper, base_concepts<Concept>>>;

	// Then check `Concept` itself.
	using concept_satisfied = decltype(models_check<Concept, Ts...>(0));

	using type =
	    tmp::and_<all_base_concepts_satisfied, concept_satisfied>;
};

// This neat trick from Eric Niebler allows use to use placeholders in
// the requires list with a functional syntax.
// The placeholders must be ::std::integral_constant<::std::size_t, n>.
template <typename Concept, typename... Placeholders, typename... Ts>
struct models_impl<Concept(Placeholders...),
                   ::vlinde::tmp::typelist<Ts...>>
{
	using type = models<
	    Concept, tmp::at<tmp::typelist<Ts...>, Placeholders::value>...>;
};

template <typename T>
auto has_base_concepts_test(typename T::base_concepts*)
    -> ::std::true_type;

template <typename T>
auto has_base_concepts_test(...) -> ::std::false_type;

template <typename T>
using has_base_concepts = decltype(has_base_concepts_test<T>(nullptr));

// We need `base_concepts` to work even for concepts which don't use
// `refines`.
template <typename Concept, bool>
struct base_concepts_impl_base
{
	using type = tmp::typelist<>;
};

template <typename Concept>
struct base_concepts_impl_base<Concept, true>
{
	using type = typename Concept::base_concepts;
};

template <typename Concept>
struct base_concepts_impl
    : base_concepts_impl_base<Concept,
                              has_base_concepts<Concept>::value>
{
};

// First, we must flatten the refinement hierarchy in a breadth first
// fashion. Then a linear scan of `models` will find us the right
// concept!

template <typename ResultList, typename ConceptList>
struct find_ancestors_impl;

template <typename Concept>
using find_ancestors = typename find_ancestors_impl<
    tmp::typelist<>, tmp::typelist<Concept>>::type;

template <typename ResultList>
struct find_ancestors_impl<ResultList, tmp::typelist<>>
{
	using type = ResultList;
};

template <typename ResultList, typename ConceptList>
struct find_ancestors_impl
{
	// Order preserving breadth-first search.

	using bases = tmp::flatten<tmp::map<base_concepts, ConceptList>>;

	using new_result = tmp::concat<ResultList, bases>;

	using type = typename find_ancestors_impl<new_result, bases>::type;
};

template <typename T, typename Concept>
struct most_refined_impl
{
	using hierarchy = tmp::cons<Concept, find_ancestors<Concept>>;

	using type = typename choose_from_impl<T, hierarchy>::type;
};

template <typename T, typename Typelist>
struct choose_from_impl
{
	template <typename C>
	using models_pred = models<C, T>;

	using type = tmp::find_first<models_pred, Typelist>;
};
}
}
}

#endif // VLINDE_CONCEPTS_DETAIL_IMPL_HPP
