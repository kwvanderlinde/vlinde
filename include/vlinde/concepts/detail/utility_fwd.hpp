namespace vlinde
{
namespace concepts
{
namespace detail
{
    template<typename Concept, typename Typelist>
    struct models_impl;

    template<typename Concept>
    struct base_concepts_impl;

    template<typename T, typename Concept>
    struct most_refined_impl;

    template<typename T, typename Typelist>
    struct choose_from_impl;
}
}
}
