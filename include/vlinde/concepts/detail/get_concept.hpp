#ifndef VLINDE_CONCEPTS_DETAIL_GET_CONCEPT_HPP
#define VLINDE_CONCEPTS_DETAIL_GET_CONCEPT_HPP

namespace vlinde
{
namespace concepts
{
namespace detail
{
template <typename T>
struct get_concept
{
	using type = T;
};

template <typename T, typename... Args>
struct get_concept<T(Args...)>
{
	using type = T;
};
}
}
}

#endif // VLINDE_CONCEPTS_DETAIL_GET_CONCEPT_HPP
