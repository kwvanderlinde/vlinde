#ifndef VLINDE_CONCEPTS_UTILITY_HPP
#define VLINDE_CONCEPTS_UTILITY_HPP

#include "detail/utility_fwd.hpp"

#include "detail/get_concept.hpp"

#include <vlinde/tmp/typelist.hpp>

#include <type_traits>

namespace vlinde
{
namespace concepts
{
struct Void
{
};

template <typename... Conditions>
using requires = typename ::std::enable_if<
    tmp::all<tmp::typelist<Conditions...>>{}, Void>::type;

template <typename ReturnType, typename T>
auto returns(T&& t) -> requires<::std::is_same<ReturnType, T>>;

template <typename ReturnType, typename T>
auto convertible_to(T&& t)
    -> requires<::std::is_convertible<T, ReturnType>>;

template <typename T>
auto is_true(T&& t) -> requires<T>;

template <typename T>
auto is_false(T&& t) -> requires<tmp::not_<T>>;

template <typename T>
auto val() -> T;

template <typename Concept, typename... Ts>
using models =
    typename detail::models_impl<Concept, tmp::typelist<Ts...>>::type;

/** Convenience accessor for a concept's base concepts.
 */
template <typename Concept>
using base_concepts =
    typename detail::base_concepts_impl<Concept>::type;

template <::std::size_t Index>
using arg = ::std::integral_constant<::std::size_t, Index>;

/** Performs a left-to-right breadth first search along the principal
 * upper set ^{Concept} for the first concept modeled by `T`.
 *
 * Note that it does not matter if a concept appears several times
 * through the search. Firstly, termination is guaranteed, since the
 * refinement hiearachy is bounded (as any class hierarchy is).
 * Secondly, if a concept is found twice, then it was rejected the first
 * time, and it will be rejected again the second time.
 */
template <typename T, typename Concept>
using most_refined =
    typename detail::most_refined_impl<T, Concept>::type;

/** Performs a left-to-right scan `Concepts`, picking the first concept
 * modeled by `T`.
 *
 * This is an alternative to `most_refined` when we want to restrict
 * the choices of results to a particular set, say, only those concepts
 * for which we've specialized an algorithm.
 */
template <typename T, typename... Concepts>
using choose_from = typename detail::choose_from_impl<
    T, tmp::typelist<Concepts...>>::type;

/** Defines the concept inheritance graph.
 *
 * \tparam BaseConcepts The list of concepts which are being refined.
 */
template <typename... BaseConcepts>
struct refines : detail::get_concept<BaseConcepts>::type...
{
	using base_concepts = tmp::typelist<BaseConcepts...>;

	/** Default prototype.
	 *
	 * This is required as a fallback in case the child class does not
	 * define one. By not defining it, we are force to call it only in
	 * unevaluated contexts, like within `decltype`.
	 */
	template <typename>
	static void requires();
};
}
}

#include "detail/utility_impl.hpp"

#endif // VLINDE_CONCEPTS_UTILITY_HPP
