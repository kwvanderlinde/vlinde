#ifndef VLINDE_CONCEPTS_NUMERIC_HPP
#define VLINDE_CONCEPTS_NUMERIC_HPP

#include <vlinde/concepts/utility.hpp>

#include <type_traits>

namespace vlinde
{
namespace concepts
{
struct Integral
{
	template <typename X>
	static auto requires() -> decltype(
	    is_true(::std::is_integral<X>{}));
};

struct FloatingPoint
{
	template <typename X>
	static auto requires() -> decltype(
	    is_true(::std::is_floating_point<X>{}));
};
}
}

#endif // VLINDE_CONCEPTS_NUMERIC_HPP
