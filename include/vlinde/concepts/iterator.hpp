#ifndef VLINDE_CONCEPTS_ITERATOR_HPP
#define VLINDE_CONCEPTS_ITERATOR_HPP

#include "basic.hpp"

#include <iterator>
#include <utility>

namespace vlinde
{
namespace concepts
{
struct Iterator
    : refines<CopyConstructible, CopyAssignable, Destructible>
{
	template <typename X>
	using reference_type =
	    typename ::std::iterator_traits<X>::reference;

	template <typename X, typename reference = reference_type<X>>
	static auto requires_impl(X& r)
	    -> decltype(models<Swappable, X&>{}, returns<reference>(*r),
	                returns<X&>(++r));

	template <typename X>
	static auto requires() -> decltype(
	    requires_impl(::std::declval<X&>()));
};

struct InputIterator : refines<Iterator, EqualityComparable>
{
	template <typename X>
	using value_type = typename ::std::iterator_traits<X>::value_type;

	template <typename X, typename T = value_type<X>>
	static auto requires_impl(X const& a, X const& b, X& r)
	    -> decltype(convertible_to<bool>(a != b), convertible_to<T>(*a),
	                (void)r++, convertible_to<T>(*r++));

	template <typename X>
	static auto requires() -> decltype(requires_impl(
	    ::std::declval<X const&>(), ::std::declval<X const&>(),
	    ::std::declval<X&>()));
};

struct OutputIterator : refines<Iterator(arg<0>)>
{
	template <typename X, typename O>
	static auto requires_impl(X& r, O&& o)
	    -> decltype(*r = ::std::forward<O>(o),
	                convertible_to<X const&>(r++),
	                *r++ = ::std::forward<O>(o));

	template <typename X, typename O>
	static auto requires() -> decltype(::std::declval<X>(),
	                                   ::std::declval<O>());
};

struct ForwardIterator : refines<InputIterator, DefaultConstructible>
{
	template <typename X>
	using reference_type =
	    typename ::std::iterator_traits<X>::reference;

	template <typename X, typename reference = reference_type<X>>
	static auto requires_impl(X& r)
	    -> decltype(convertible_to<X const&>(r++),
	                returns<reference>(*r++));

	template <typename X>
	static auto requires() -> decltype(
	    requires_impl(::std::declval<X&>()));
};

struct BidirectionalIterator : refines<ForwardIterator>
{
	template <typename X>
	using reference_type =
	    typename ::std::iterator_traits<X>::reference;

	template <typename X, typename reference = reference_type<X>>
	static auto requires_impl(X& r)
	    -> decltype(returns<X&>(--r), convertible_to<X const&>(r--),
	                returns<reference>(*r--));

	template <typename X>
	static auto requires() -> decltype(
	    requires_impl(::std::declval<X&>()));
};

struct RandomAccessIterator : refines<BidirectionalIterator>
{
	template <typename X>
	using difference_type =
	    typename ::std::iterator_traits<X>::difference_type;
	template <typename X>
	using reference_type =
	    typename ::std::iterator_traits<X>::reference;

	template <typename X, typename reference = reference_type<X>,
	          typename difference_type = difference_type<X>>
	static auto requires_impl(X const& a, X const& b, X& r,
	                          difference_type n)
	    -> decltype(returns<X&>(r += n), returns<X>(a + n),
	                returns<X>(n + a), returns<X&>(r -= n),
	                returns<X>(a - n), returns<difference_type>(b - a),
	                convertible_to<reference>(a[n]),
	                convertible_to<bool>(a < b),
	                convertible_to<bool>(a > b),
	                convertible_to<bool>(a >= b),
	                convertible_to<bool>(a <= b));

	template <typename X>
	static auto requires() -> decltype(requires_impl(
	    ::std::declval<X const&>(), ::std::declval<X const&>(),
	    ::std::declval<X&>(), ::std::declval<difference_type<X>>()));
};

struct ValueSwappable : refines<Iterator>
{
	template <typename X>
	static auto requires_impl(X&& x)
	    -> decltype(models<Swappable, decltype(*x)>{});

	template <typename T>
	static auto requires() -> decltype(requires_impl(val<T&&>()));
};
}
}

#endif // VLINDE_CONCEPTS_ITERATOR_HPP
