#ifndef VLINDE_CONCEPTS_BASIC_HPP
#define VLINDE_CONCEPTS_BASIC_HPP

#include <vlinde/concepts/utility.hpp>

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace concepts
{
// Define a bunch of fundamental concepts from §17.6.3

struct EqualityComparable
{
	template <typename T>
	static auto requires_impl(T const & a, T const & b)
	    -> decltype(convertible_to<bool>(a == b));

	template <typename T>
	static auto requires() -> decltype(requires_impl(val<T &&>(),
	                                                 val<T &&>()));
};

struct LessThanComparable
{
	template <typename T>
	static auto requires_impl(T const & a, T const & b)
	    -> decltype(convertible_to<bool>(a < b));

	template <typename T>
	static auto requires() -> decltype(requires_impl(val<T &&>,
	                                                 val<T &&>()));
};

struct Constructible
{
	template <typename T, typename... Us>
	static auto requires() -> decltype(
	    is_true(::std::is_constructible<T, Us...>{}));
};

struct DefaultConstructible
{
	template <typename T>
	static auto requires() -> decltype(
	    is_true(::std::is_default_constructible<T>{}));
};

struct MoveConstructible
{
	template <typename T>
	static auto requires() -> decltype(
	    is_true(::std::is_move_constructible<T>{}));
};

struct CopyConstructible : refines<MoveConstructible>
{
	template <typename T>
	static auto requires() -> decltype(
	    is_true(::std::is_copy_constructible<T>{}));
};

struct Assignable
{
	template <typename T, typename U>
	static auto requires() -> decltype(
	    is_true(::std::is_assignable<T, U>{}));
};

struct MoveAssignable
{
	template <typename T>
	static auto requires() -> decltype(
	    is_true(::std::is_move_assignable<T>{}));
};

struct CopyAssignable : refines<MoveAssignable>
{
	template <typename T>
	static auto requires() -> decltype(
	    is_true(::std::is_copy_assignable<T>{}));
};

struct Destructible
{
	template <typename T>
	static auto requires() -> decltype(
	    is_true(::std::is_destructible<T>{}));
};

namespace detail
{
// We need ADL with fallback to std::swap for the Swappable trait.
using std::swap;

template <typename T, typename U>
auto swappable_requires(T && t, U && u) -> decltype(swap(t, u));
}

struct Swappable
{
	template <typename T, typename U>
	static auto requires() -> decltype(detail::swappable_requires(
	    ::std::declval<T>(), ::std::declval<U>()));

	template <typename T>
	static auto requires() -> decltype(requires<T, T>());
};

struct NullablePointer
    : refines<EqualityComparable, DefaultConstructible,
              CopyConstructible, CopyAssignable, Destructible>
{
	template <typename T>
	static auto requires_impl(T & t, T const & a, T const & b,
	                          ::std::nullptr_t const & np)
	    -> decltype(models<Swappable, T &>{},
	                models<Constructible, T, std::nullptr_t>{},
	                returns<T &>(t = np), convertible_to<bool>(a != b),
	                convertible_to<bool>(a == nullptr),
	                convertible_to<bool>(nullptr == a),
	                convertible_to<bool>(nullptr != a),
	                convertible_to<bool>(a));

	template <typename T>
	static auto requires() -> decltype(requires_impl(val<T &>(),
	                                                 val<T const &>(),
	                                                 val<T const &>()));
};

struct Hash : refines<CopyConstructible(arg<0>), Destructible(arg<0>)>
{
	template <typename H, typename K>
	static auto requires_impl(H const & h, K const & k)
	    -> decltype(returns<::std::size_t>(h(k)));

	template <typename H, typename Key>
	static auto requires() -> decltype(
	    requires_impl(val<H const &>(), val<Key const &>()));
};
}
}

#endif // VLINDE_CONCEPTS_BASIC_HPP
