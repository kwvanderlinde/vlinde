#ifndef VLINDE_DATA_MAYBE
#define VLINDE_DATA_MAYBE

#include "detail/maybe_iterator.hpp"

#include <boost/iterator/iterator_facade.hpp>

#include <algorithm>
#include <cassert>
#include <cstddef>
#include <iterator>
#include <stdexcept>
#include <type_traits>

namespace vlinde
{
namespace data
{
/* §23 describes container requirements
 * §24 describes iterator requirements
 */
/** Represents the possibility of a value.
  *
  * A `maybe<T>` either represents no value, in which case it is called
  * *empty*. Otherwise, the `maybe<T>` is called *non-empty*.
  *
  * Note that the complexity guarantees stated here are those stated
  * by the C++11 standard for containers. However, since the number
  * of elements in a `maybe<T>` is bounded above by 1, all operations
  * are really constant time.
  *
  * \tparam T
  * The type of value which may be stored in a `maybe<T>`.
  *
  * \requires
  * `T` is not a `const` type.
  */
template <typename T>
class maybe
{
public:
	/** Alias for `T`.
	  *
	  * \requires
	  * `T` is `Destructible`
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	using value_type = T;

	/** Lvalue reference to `T`.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	using reference = T&;

	/** Lvalue reference to `const T`.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	using const_reference = value_type const&;

	/** Signed integer type.
	  *
	  * \requires
	  * Is identical to the difference type of `iterator` and
	  * `const_iterator`.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	using difference_type = ::std::ptrdiff_t;

	/** Unsigned integer type.
	  *
	  * \requires
	  * `size_type` can represent any non-negative value of
	  * `difference_type`
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	using size_type = ::std::size_t;

	/** Iterator type whose value type is `T`.
	  *
	  * \requires
	  * `iterator` is a random access iterator.
	  *
	  * \requires
	  * Convertible to `const_iterator`.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements), §23.2.1/9.
	  */
	using iterator = detail::maybe_iterator<T>;

	/** Constant iterator type whose value type is `T`.
	  *
	  * \requires
	  * `const_iterator` is a random access iterator.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements), §23.2.1/9.
	  */
	using const_iterator = detail::maybe_iterator<T const>;

private:
	union
	{
		/** The value stored in `*this`.
		  *
		  * \note `m_value` is not initialized by the default
		  * constructor.
		  */
		T m_value;
	};

	/** Whether `m_value` is an object.
	  *
	  * If `m_has_value` is `true`, then `m_value` has been initialized
	  * to an object, and has not been destroyed. If `false`, then
	  * either `m_value` was never initialized, or it has been destroyed
	  * without being reinitialized.
	  */
	bool m_has_value;

public:
	/** Constructs an empty `maybe<T>`.
	  *
	  * \post
	  * `this->empty()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	 */
	maybe() : m_has_value(false)
	{
	}

	/** Copy constructor.
	  *
	  * \requires
	  * `T` is `CopyInsertable` into `maybe<T>`.
	  *
	  * \post
	  * `*this == other`.
	  *
	  * \complexity
	  * Linear.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	maybe(maybe const& other) : m_has_value(other.m_has_value)
	{
		if (m_has_value)
			new (::std::addressof(m_value)) T(other.m_value);
	}

	/** Move constructor.
	  *
	  * \post
	  * `*this` shall be equal to the value `other` had before this
	  * constructor was called.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	maybe(maybe&& other) : maybe()
	{
		this->swap(other);
	}

	/** Destructor.
	  *
	  * The destructor is applied to every element of `*this`. All the
	  * memory is deallocated.
	  *
	  * \complexity
	  * Linear.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	~maybe()
	{
		if (m_has_value)
			m_value.~T();
	}

	/** Copy assignment.
	  *
	  * \post
	  * `*this == other`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	 */
	maybe& operator=(maybe const& other)
	{
		maybe tmp(other);
		this->swap(tmp);
		return *this;
	}

	/** Move assignment.
	  *
	  * All existing elements of `*this` are either move assigned to or
	  * destroyed.
	  *
	  * \post
	  * `*this` shall be equal to the value of `other` from before the
	  * assignment.
	  *
	  * \complexity
	  * Linear.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	maybe& operator=(maybe&& other)
	{
		maybe tmp(::std::move(other));
		this->swap(tmp);
		return *this;
	}

	/** Pseudo-copy constructor.
	  *
	  * Copy constructs the value of `*this` from `value`.
	  *
	  * \param value The `T` value from which to copy construct
	  * `*this`'s value
	  *
	  * \complexity
	  * Constant.
	  */
	maybe(T const& value) : m_value(value), m_has_value(true)
	{
	}

	/** Pseudo-move constructor.
	  *
	  * Move constructs the value of `*this` from `value`.
	  *
	  * \param value The `T` value from which to move construct
	  * `*this`'s value
	  *
	  * \complexity
	  * Constant.
	  */
	maybe(T&& value) : m_value(::std::move(value)), m_has_value(true)
	{
	}

	/** Pseudo-copy assignment operator.
	  *
	  * Destroys the value of `*this` (if it exists) and copy constructs
	  * the value of `*this` from `value`.
	  *
	  * \param value The `T` value from which to copy construct
	  * `*this`'s value
	  *
	  * \return `*this`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \exsafety
	  * If `T`'s copy constructor is no throw, then strong exception
	  * safety. Otherwise, basic exception safety.
	  */
	maybe& operator=(T const& value)
	{
		// protect against "self assignment".
		if (!m_has_value || ::std::addressof(this->m_value) !=
		                        ::std::addressof(value)) {
			this->clear();                                  // noexcept
			new (::std::addressof(this->m_value)) T(value); // except
			this->m_has_value = true;                       // noexcept
		}
		return *this;
	}

	/** Pseudo-move assignment operator.
	  *
	  * Destroys the value of `*this` (if it exists) and move constructs
	  * the value of `*this` from `value`.
	  *
	  * \param value The `T` value from which to move construct
	  * `*this`'s value
	  *
	  * \return `*this`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \exsafety
	  * If `T`'s move constructor is no throw, then strong exception
	  * safety. Otherwise, basic exception safety.
	  */
	maybe& operator=(T&& value)
	{
		// protect against "self assignment".
		if (!m_has_value || ::std::addressof(this->m_value) !=
		                        ::std::addressof(value)) {
			this->clear(); // noexcept
			new (::std::addressof(this->m_value))
			    T(::std::move(value)); // except
			this->m_has_value = true;  // noexcept
		}
		return *this;
	}

	/** Returns an iterator to the element stored in the `maybe<T>` or,
	  * if `this->empty()`, returns `end()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	iterator begin()
	{
		if (m_has_value)
			return { ::std::addressof(m_value),
			         detail::is_dereferencable::yes};
		else
			return end();
	}

	/** Returns a constant iterator to the element stored in the
	  * `maybe<T>` or, if `this->empty()`, returns `end()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	const_iterator begin() const
	{
		if (m_has_value)
			return { ::std::addressof(m_value),
			         detail::is_dereferencable::yes};
		else
			return cend();
	}

	/** Returns an iterator past-the-end of the `maybe<T>`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	iterator end()
	{
		return { ::std::addressof(m_value),
		         detail::is_dereferencable::no};
	}

	/** Returns a constant iterator past-the-end of the `maybe<T>`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	const_iterator end() const
	{
		return { ::std::addressof(m_value),
		         detail::is_dereferencable::no};
	}

	/** Returns a constant iterator to the element stored in the
	  * `maybe<T>` or, if `this->empty()`, returns `end()`.
	  *
	  * `a.cbegin()` is equivalent to
	  * `const_cast<maybe<T> const&>(a).begin()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	const_iterator cbegin() const
	{
		return begin();
	}

	/** Returns a constant iterator past-the-end of the `maybe<T>`.
	  *
	  * `a.cend()` is equivalent to
	  * `const_cast<maybe<T> const&>(a).end()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	const_iterator cend() const
	{
		return end();
	}

	/** Exchanges the contents of `*this` and `other`.
	  *
	  * \complexity
	  * Constant
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	void swap(maybe& other)
	{
		if (this->m_has_value) {
			if (other.m_has_value) {
				using ::std::swap;
				swap(this->m_value, other.m_value);
			} else {
				using ::std::swap;

				// We need move constructor to be
				// nothrow
				new (::std::addressof(other.m_value))
				    T(::std::move(this->m_value));
				this->m_value.~T();
				swap(this->m_has_value, other.m_has_value);
			}
		} else {
			if (other.m_has_value) {
				other.swap(*this);
			} else {
				// nothing to swap
			}
		}
	}

	/** Returns the number of values in the `maybe<T>`.
	  *
	  * `a.size()` is equivalent to `distance(a.begin(), a.end())`.
	  *
	  * \complexity
	  * constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	size_type size() const
	{
		using ::std::distance;

		distance(begin(), end());
	}

	/** Returns the theoretical maximum on the number of values stored
	  * in the `maybe<T>`.
	  *
	  * `a.max_size()` is equivalent to
	  * `distance(b.begin(), b.end())` where `b` is a `maybe<T>` of the
	  * largest possible size. That is, `a.max_size() == 1`.
	  *
	  * \complexity
	  * constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	size_type max_size() const
	{
		return 1u;
	}

	/** Checks if the `maybe<T>` is empty.
	  *
	  * `a.empty()` is equivalent to `a.begin() == a.end()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 96 (Container requirements).
	  */
	bool empty() const
	{
		return !m_has_value;
	}

	/** Iterator type whose value type is `T`.
	  *
	  * This type is `reverse_iterator<iterator>`.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	using reverse_iterator = ::std::reverse_iterator<iterator>;

	/** Iterator type whose value type is `const T`.
	  *
	  * This type is `reverse_iterator<const_iterator>`.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	using const_reverse_iterator =
	    ::std::reverse_iterator<const_iterator>;

	/** Returns a reverse iterator to the element stored in the
	  * `maybe<T>` or, if `this->empty()`, returns `rend()`.
	  *
	  * `a.rbegin()` is equivalent to `reverse_iterator(a.end())`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	reverse_iterator rbegin()
	{
		if (m_has_value)
			return iterator{this, ::std::addressof(m_value)};
		else
			return rend();
	}

	/** Returns a constant reverse iterator to the element stored in the
	  * `maybe<T>` or, if `this->empty()`, returns `rend()`.
	  *
	  * `a.rbegin()` is equivalent to `reverse_iterator(a.end())`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	const_reverse_iterator rbegin() const
	{
		if (m_has_value)
			return const_iterator{this, ::std::addressof(m_value)};
		else
			return rend();
	}

	/** Returns a reverse iterator past-the-end of the `maybe<T>`.
	  *
	  * `a.rend()` is equivalent to `reverse_iterator(a.begin())`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	reverse_iterator rend()
	{
		return const_iterator{nullptr, nullptr};
	}

	/** Returns a constant reverse iterator past-the-end of the
	 * `maybe<T>`.
	  *
	  * `a.rend()` is equivalent to `reverse_iterator(a.begin())`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	const_reverse_iterator rend() const
	{
		return const_iterator{nullptr, nullptr};
	}

	/** Returns a constant reverse iterator to the element stored in the
	  * `maybe<T>` or, if `this->empty()`, returns `rend()`.
	  *
	  * `a.crbegin()` is equivalent to
	  * `const_cast<maybe<T> const&>(a).rbegin()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	const_reverse_iterator crbegin() const
	{
		return rbegin();
	}

	/** Returns a constant reverse iterator past-the-end of the
	  * `maybe<T>`.
	  *
	  * `a.crend()` is equivalent to
	  * `const_cast<maybe<T> const&>(a).rend()`.
	  *
	  * \complexity
	  * Constant.
	  *
	  * \cpp11ref
	  * Table 97 (Reversible container requirements).
	  */
	const_reverse_iterator crend() const
	{
		return rend();
	}

	/** Tests if the `maybe<T>` is non-empty.
	  *
	  * `a` converted to `bool` is equivalent to `!a.empty()`.
	  *
	  * \boostref
	  * OptionalPointee concept.
	  */
	explicit operator bool() const
	{
		return m_has_value;
	}

	/** Returns a reference to the contained value.
	  *
	  * If `*this` is empty, then results are undefined.
	  *
	  * \boostref
	  * OptionalPointee concept.
	  */
	T& operator*()
	{
		assert(m_has_value);

		return m_value;
	}

	/** Returns a constant reference to the contained value.
	  *
	  * If `*this` is empty, then results are undefined.
	  *
	  * \boostref
	  * OptionalPointee concept.
	  */
	T const& operator*() const
	{
		assert(m_has_value);

		return m_value;
	}

	/** Returns a pointer to the contained value, or `nullptr` if
	  * `*this` is empty.
	  *
	  * \boostref
	  * OptionalPointee concept.
	  */
	T* operator->()
	{
		if (!m_has_value)
			return nullptr;

		return ::std::addressof(m_value);
	}

	/** Returns a constant pointer to the contained value, or `nullptr`
	  * if `*this` is empty.
	  *
	  * \boostref
	  * OptionalPointee concept.
	  */
	T const* operator->() const
	{
		if (!m_has_value)
			return nullptr;

		return ::std::addressof(m_value);
	}

	/** Replaces `*this` with an empty `maybe<T>`.
	  *
	  * If `!this->empty()`, then the value of `*this` is destroyed.
	  *
	  * \post `this->empty()`.
	  *
	  * \exsafety No throw.
	  */
	void clear() noexcept
	{
		if (this->m_has_value) {
			this->m_has_value = false;
			this->m_value.~T();
		}
	}

	/** Replaces the value of `*this` by perfect forwarding.
	  *
	  * If `!this->empty()`, then the value of `*this` is destroyed.
	  * The value of `*this` is then constructed by perfectly forwarding
	  * `args` to the constructor of `T`.
	  *
	  * \param args The arguments to `T`'s constructor.
	  *
	  * \exsafety If `T`'s constructor is no throw, then strong
	  * exception safety. Otherwise, basic exception safety.
	  */
	template <typename... Args>
	void emplace(Args&&... args)
	{
		// noexcept
		this->clear();

		// except
		new (::std::addressof(this->m_value))
		    T(::std::forward<Args>(args)...);

		// noexcept
		m_has_value = true;
	}
};

/** Equality operator.
  *
  * `==` is an equivalence relation. `a == b` is equivalent to
  * `a.size() == b.size() && equal(a.begin(), a.end(), b.begin())`.
  *
  * \requires
  * `T` is `EqualityComparable`.
  *
  * \complexity
  * Linear.
  *
  * \cpp11ref
  * Table 96 (Container requirements).
  */
template <typename T>
bool operator==(maybe<T> const& a, maybe<T> const& b) noexcept
{
	using ::std::equal;

	return (a.size() == b.size()) &&
	       equal(a.begin(), a.end(), b.begin());
}

/** Inequality operator.
  *
  * `a != b` is equivalent to `!(a == b)`.
  *
  * \complexity
  * Linear.
  *
  * \cpp11ref
  * Table 96 (Container requirements).
  */
template <typename T>
bool operator!=(maybe<T> const& a, maybe<T> const& b) noexcept
{
	return !(a == b);
}

/** Exchanges the contents of two `maybe<T>` instances.
  *
  * `swap(a, b)` is equivalent to `a.swap(b)`.
  *
  * \complexity
  * Constant
  *
  * \cpp11ref
  * Table 96 (Container requirements).
  */
template <typename T>
void swap(maybe<T>& a, maybe<T>& b)
{
	a.swap(b);
}

/** Compares two `maybe<T>` instances lexicographically.
  *
  * `a < b` is equivalent to
  * `lexicographical_compare(a.begin(), a.end(), b.begin(), b.end())`.
  *
  * \complexity
  * Linear
  *
  * \cpp11ref
  * Table 98 (Optional container operations).
  */
template <typename T>
bool operator<(maybe<T> const& a, maybe<T> const& b) noexcept
{
	using ::std::lexicographical_compare;

	return lexicographical_compare(a.begin(), a.end(), b.begin(),
	                               b.end());
}

/** Compares two `maybe<T>` instances lexicographically.
  *
  * `a > b` is equivalent to `b < a`.
  *
  * \complexity
  * Linear
  *
  * \cpp11ref
  * Table 98 (Optional container operations).
  */
template <typename T>
bool operator>(maybe<T> const& a, maybe<T> const& b) noexcept
{
	return a > b;
}

/** Compares two `maybe<T>` instances lexicographically.
 *
 * `a <= b` is equivalent to `!(a > b)`.
 *
 * \complexity
 * Linear
 *
 * \cpp11ref
 * Table 98 (Optional container operations).
 */
template <typename T>
bool operator<=(maybe<T> const& a, maybe<T> const& b) noexcept
{
	return !(a > b);
}

/** Compares two `maybe<T>` instances lexicographically.
  *
  * `a >= b` is equivalent to `!(a < b)`.
  *
  * \complexity
  * Linear
  *
  * \cpp11ref
  * Table 98 (Optional container operations).
  */
template <typename T>
bool operator>=(maybe<T> const& a, maybe<T> const& b) noexcept
{
	return !(a < b);
}
}
}

#endif // VLINDE_DATA_MAYBE
