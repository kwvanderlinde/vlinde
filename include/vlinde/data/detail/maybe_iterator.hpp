#ifndef VLINDE_DATA_DETAIL_MAYBE_ITERATOR_HPP
#define VLINDE_DATA_DETAIL_MAYBE_ITERATOR_HPP

#include <boost/iterator/iterator_facade.hpp>

#include <cassert>
#include <iterator>
#include <type_traits>
#include <utility>

namespace vlinde
{
namespace data
{
namespace detail
{
enum class is_dereferencable : bool {
	no = false,
	yes = true
};

template <typename T>
class maybe_iterator;

/** A convenience alias for CRTP applied to `maybe_iterator`.
  *
  * If `T` is `const`, then we have a constant iterator. Otherwise, it
  * is immutable.
  */
template <typename T>
using maybe_iterator_base = ::boost::iterator_facade<
    maybe_iterator<T>, T, ::std::random_access_iterator_tag>;

template <typename T>
class maybe_iterator : public maybe_iterator_base<T>
{
	T* m_ptr;
	bool m_is_dereferencable;

public:
	maybe_iterator(T* t, is_dereferencable valid)
	: m_ptr{t}, m_is_dereferencable{valid == is_dereferencable::yes}
	{
	}

	maybe_iterator() : m_ptr{nullptr}, m_is_dereferencable{false}
	{
	}

	~maybe_iterator() = default;

	maybe_iterator(maybe_iterator const&) = default;

	maybe_iterator& operator=(maybe_iterator const&) = default;

	void swap(maybe_iterator& other) noexcept
	{
		using ::std::swap;
		swap(this->m_ptr, other.m_ptr);
		swap(this->m_is_dereferencable, other.m_is_dereferencable);
	}

	friend void swap(maybe_iterator& first,
	                 maybe_iterator& second) noexcept
	{
		first.swap(second);
	}

	typename maybe_iterator_base<T>::reference dereference() const
	    noexcept
	{
		assert(m_ptr);
		assert(m_is_dereferencable);

		return *m_ptr;
	}

	bool equal(maybe_iterator const& other) const noexcept
	{
		return m_ptr == other.m_ptr &&
		       m_is_dereferencable == other.m_is_dereferencable;
	}

	void increment() noexcept
	{
		assert(m_ptr);
		assert(m_is_dereferencable);

		m_is_dereferencable = false;
	}

	void decrement() noexcept
	{
		assert(m_ptr);
		assert(!m_is_dereferencable);

		m_is_dereferencable = true;
	}

	void
	advance(typename maybe_iterator_base<T>::difference_type n) noexcept
	{
		/* Note: these are the exact semantics required by the standard
		 * for random access iterators, but they're overkill for a
		 * maybe_iterator.
		 */
		auto m = n;
		if (m >= 0) {
			while (m--) {
				this->operator++();
			}
		} else {
			while (m++) {
				this->operator--();
			}
		}
	}

	typename maybe_iterator_base<T>::difference_type
	distance_to(maybe_iterator const& other) noexcept
	{
		assert(this->m_ptr != nullptr);
		assert(this->m_ptr == other.m_ptr);

		if (this->m_is_dereferencable == other.m_is_dereferencable)
			return 0;
		if (this->m_is_dereferencable) // *this is begin(),
			                           // other is end()
			return -1;
		return 1; // *this is end(), other is begin()
	}
};
}
}
}

#endif // VLINDE_DATA_DETAIL_MAYBE_ITERATOR_HPP
