#ifndef VLINDE_DATA_VECTOR_HPP
#define VLINDE_DATA_VECTOR_HPP

#include <vlinde/algorithm/modifying.hpp>
#include <vlinde/memory/default_allocator.hpp>
#include <vlinde/memory/block.hpp>
#include <vlinde/memory/typed_allocator.hpp>
#include <vlinde/memory/typed_memory.hpp>
#include <vlinde/range/iterator_range.hpp>

#include <cstddef>
#include <utility>

namespace vlinde
{
namespace data
{

/** \internal
 *
 * \todo Complete this implementation.
 */
template <typename T,
          typename Allocator = ::vlinde::memory::default_allocator>
class vector
{
public:
	using value_type = T;
	using size_type = ::std::size_t;
	using range_type = ::vlinde::range::iterator_range<T*>;
	using const_range_type = ::vlinde::range::iterator_range<T const*>;
	using allocator_type = Allocator;

private:
	using memory_block = ::vlinde::memory::block<void>;
	using typed_memory = ::vlinde::memory::typed_memory<value_type>;

	// TODO Use Empty Base Optimization for allocators.
	allocator_type m_allocator;
	size_type m_size;
	typed_memory m_backing_store;

public:
	vector(allocator_type allocator = allocator_type()) noexcept
	    : m_allocator(::std::move(allocator)),
	      m_size(0u),
	      m_backing_store(memory_block{nullptr, 0u})
	{
	}

	vector(vector<value_type, allocator_type> const& other)
	: m_allocator(other.m_allocator), m_size(other.m_size),
	  m_backing_store(m_size == 0
	                      ? memory_block{nullptr, 0u}
	                      : m_allocator.alloc(sizeof(T) * m_size))
	{
    ::vlinde::algorithm::copy(this->range(), other.range());
	}

	vector(vector<value_type, allocator_type> && other) noexcept
	: m_allocator(::std::move(other.m_allocator)), m_size(other),
	  m_backing_store(other.m_backing_store)
	{
    // Empty other
    other.m_allocator = allocator_type();
    other.m_size = 0;
    other.m_backing_store = memory_block{nullptr, 0u};
	}

	bool empty() const noexcept
	{
		return m_size == 0;
	}

  range_type range() noexcept
  {
    if (m_size == 0) {
      // No elements in range.
      return range_type{nullptr, nullptr};
    }

    return m_backing_store.range().slice(0, m_size);
  }

  const_range_type range() const noexcept
  {
    return this->crange();
  }

  const_range_type crange() const noexcept
  {
    if (m_size == 0) {
      // No elements in range.
      return range_type{nullptr, nullptr};
    }

    return m_backing_store.range().slice(0, m_size);
  }
};
}
}

#endif // VLINDE_DATA_VECTOR_HPP
