#ifndef VLINDE_RANGE_HPP
#define VLINDE_RANGE_HPP

namespace vlinde
{
/** Primitives for constructing ranges.
 *
 * The range library, like `::vlinde::algorithm`, relies heavily on the
 * various [range concepts](\ref range_concepts_page).
 */
namespace range
{
}
}

#include "range/concepts.hpp"
#include "range/drop.hpp"
#include "range/filter.hpp"
#include "range/generator.hpp"
#include "range/iota.hpp"
#include "range/map.hpp"
#include "range/random_range.hpp"
#include "range/retro.hpp"
#include "range/take.hpp"

#endif // VLINDE_RANGE_HPP
