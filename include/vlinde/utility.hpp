#ifndef VLINDE_UTILITY
#define VLINDE_UTILITY

namespace vlinde
{
/** Provides generic services.
  *
  * The services provided by the `utility` namespace are not related to
  * any particular kind of application. Instead, the members of this
  * namespace can be applied to any number of scenarios and paradigms.
  */
namespace utility
{
}
}

#include "utility/compare.hpp"
#include "utility/scope_guard.hpp"

#endif // VLINDE_UTILITY
