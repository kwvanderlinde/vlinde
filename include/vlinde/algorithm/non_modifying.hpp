#ifndef VLINDE_ALGORITHM_NON_MODIFYING_HPP
#define VLINDE_ALGORITHM_NON_MODIFYING_HPP

#include <cstddef>
#include <tuple>
#include <utility>

namespace vlinde
{
namespace algorithm
{

/** Check if `predicate` returns `true` for all elements of `range`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \tparam UnaryPredicate A unary predicate on the element type of
  * `Range`.
  *
  * \param range The range in which to search.
  *
  * \param predicate The predicate to use for the search.
  *
  * \returns `true` if `range` is empty, or if `predicate(e)` is `true`
  * for each element `e` in `range`, and `false` otherwise.
  *
  * \complexity At most `count(range)` applications of `predicate`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryPredicate>
bool all(Range range, UnaryPredicate predicate)
{
	for (; !range.empty(); range.pop_front()) {
		if (!predicate(range.front()))
			return false;
	}

	return true;
}

/** Check if `predicate` returns `true` for some element of `range`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \tparam UnaryPredicate A unary predicate on the element type of
  * `Range`.
  *
  * \param range The range in which to search.
  *
  * \param predicate The predicate to use for the search.
  *
  * \returns `false` if `range` is empty, or if `predicate(e)` is
  * `false` for each element `e` in `range`, and `true` otherwise.
  *
  * \complexity At most `count(range)` applications of `predicate`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryPredicate>
bool any(Range range, UnaryPredicate predicate)
{
	for (; !range.empty(); range.pop_front()) {
		if (predicate(range.front()))
			return true;
	}

	return false;
}

/** Check if `predicate` returns `true` for no element of `range`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \tparam UnaryPredicate A unary predicate on the element type of
  * `Range`.
  *
  * \param range The range in which to search.
  *
  * \param predicate The predicate to use for the search.
  *
  * \returns `true` if `range` is empty, or if `predicate(e)` is `false`
  * for each element `e` in `range`, and `false` otherwise.
  *
  * \complexity At most `count(range)` applications of `predicate`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryPredicate>
bool none(Range range, UnaryPredicate predicate)
{
	return !any(::std::move(range), predicate);
}

/** Applies `f` to each element of `range`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \tparam UnaryFunction A type callable with the value type of
  * `Range`.
  *
  * \param range The range to consume.
  *
  * \param func The function to apply.
  *
  * \returns `::%std::move(func)`
  *
  * \complexity `count(range)` applications of `func`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryFunction>
UnaryFunction for_each(Range range, UnaryFunction func)
{
	for (; !range.empty(); range.pop_front()) {
		func(range.front());
	}
	return ::std::move(func);
}

/** Counts the number of elements in `range`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \param range The range to enumerate.
  *
  * \returns The number of elements in `range`.
  *
  * \todo Return `Range`'s `size_type` if it exists.
  *
  * \todo Specialized for Sized Range.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range>
::std::size_t count(Range range)
{
	::std::size_t result{0};
	for (; !range.empty(); range.pop_front()) {
		++result;
	}
	return result;
}

/** Counts the number of elements in `range` equal to `value`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \tparam T The type of value to search for.
  *
  * \param range The range in which to search.
  *
  * \param value The value to search for.
  *
  * \returns The number of elements `e` in `range` for which
  * `e == value`.
  *
  * \complexity `count(range)` equality comparisons.
  *
  * \todo Return `Range`'s `size_type` if it exists.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename T>
::std::size_t count(Range range, T const& value)
{
	return count_if(::std::move(range), [&value](T const& other) {
		return value == other;
	});
}

/** Advances `range1` and `range2` in lockstep until `predicate` returns
  * `false`, or until either range is exhausted, whichever occurs first.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Readable][] [Forward][] [Range][].
  *
  * \tparam BinaryPredicate A binary predicate on the element types of
  * `Range1` and `Range2`.
  *
  * \param range1 The first range to compare.
  *
  * \param range2 The second range to compare.
  *
  * \param predicate The predicate to compare the ranges with.
  *
  * \return A pair of ranges `r1` and `r2` such that `r1` and `r2` are
  * the longest respective tails of `range1` and `range2` for which
  * `count(range1) - count(r1) == count(range2) - count(r2)` and
  * `!predicate(r1.front(), r2.front())`.
  *
  * \complexity At most `min(count(range1), count(range2))` applications
  * of `predicate`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2, class BinaryPredicate>
::std::tuple<Range1, Range2> mismatch(Range1 range1, Range2 range2,
                                      BinaryPredicate predicate)
{
	for (; !range1.empty() && !range2.empty();
	     range1.pop_front(), range2.pop_front()) {
		if (!predicate(range1.front(), range2.front())) {
			break;
		}
	}

	return ::std::make_tuple(::std::move(range1), ::std::move(range2));
}

/** Advances `range1` and `range2` in lockstep until a mismatch is
  * found, or until either range is exhausted, whichever occurs first.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Readable][] [Forward][] [Range][].
  *
  * \param range1 The first range to compare.
  *
  * \param range2 The second range to compare.
  *
  * \return A pair of ranges `r1` and `r2` such that `r1` and `r2` are
  * the longest respective tails of `range1` and `range2` for which
  * `count(range1) - count(r1) == count(range2) - count(r2)` and
  * `r1.front() != r2.front()`.
  *
  * \complexity At most `min(count(range1), count(range2))` equality
  * comparisions.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
::std::tuple<Range1, Range2> mismatch(Range1 range1, Range2 range2)
{
	for (; !range1.empty() && !range2.empty();
	     range1.pop_front(), range2.pop_front()) {
		if (range1.front() != range2.front()) {
			break;
		}
	}

	return ::std::make_tuple(::std::move(range1), ::std::move(range2));
}

/** Checks whether advancing `range1` and `range2` in lockstep does not
  * produce any mismatches.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Readable][] [Forward][] [Range][].
  *
  * \tparam BinaryPredicate A binary predicate on the element types of
  * `Range1` and `Range2`.
  *
  * \param range1 The first range to compare.
  *
  * \param range2 The second range to compare.
  *
  * \param predicate The predicate to compare the ranges with.
  *
  * \returns `true` if `range1` and `range2` have the same number of
  * elements, and `predicate(range1_i, range2_i)` for each `i` in
  * `[0, count(range1))`, where `range1_i` and `range2_i` denote the
  * `i`th element of `range1_i` and `range2_i`, respectively.
  *
  * \complexity At most `min(count(range1), count(range2))` applications
  * of `predicate`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2, class BinaryPredicate>
bool equal(Range1 range1, Range2 range2, BinaryPredicate predicate)
{
	auto result =
	    mismatch(::std::move(range1), ::std::move(range2), predicate);
	return ::std::get<0>(result).empty() &&
	       ::std::get<1>(result).empty();
}

/** Checks whether advancing `range1` and `range2` in lockstep does not
  * produce any mismatches.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Readable][] [Forward][] [Range][].
  *
  * \param range1 The first range to compare.
  *
  * \param range2 The second range to compare.
  *
  * \returns `true` if `range1` and `range2` have the same number of
  * elements, and `range1_i == range2_i` for each `i` in
  * `[0, count(range1))`, where `range1_i` and `range2_i` denote the
  * `i`th element of `range1_i` and `range2_i`, respectively.
  *
  * \complexity At most `min(count(range1), count(range2))` equality
  * comparisons.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
bool equal(Range1 range1, Range2 range2)
{
	auto result = mismatch(::std::move(range1), ::std::move(range2));
	return ::std::get<0>(result).empty() &&
	       ::std::get<1>(result).empty();
}

/** Advances `range` until `predicate` returns true.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][].
  *
  * \tparam UnaryPredicate A unary predicate on the element type of
  * `Range`.
  *
  * \param range The range in which to search.
  *
  * \param predicate The predicate to use for the search.
  *
  * \returns The longest tail `t` of `range` such that
  * `predicate(t.front())`.
  *
  * \complexity At most `count(range)` applications of `predicate`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryPredicate>
Range find_if(Range range, UnaryPredicate predicate)
{
	for (; !range.empty(); range.pop_front()) {
		if (predicate(range.front()))
			break;
	}
	return range;
}

/** Advances `range` to the last occurrence of `subsequence`, as
  * determined by `predicate`.
  *
  * \tparam Range1 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam BinaryPredicate A binary predicate on the value types of
  * `Range1` and `Range2`.
  *
  * \param range The range to search.
  *
  * \param subsequence The subsequence of elements to search for.
  *
  * \param predicate The predicate to compare elements with.
  *
  * \returns The smallest tail `t` of `range`, containing at least
  * `count(subsequence)` elements such that
  * `predicate(t_i, subsequence_i)` for each `i` in
  * `[0, count(subsequence))`, where `_i` indicates the `i`th element of
  * a range. If no such tail exists, then the empty tail is returned.
  *
  * \complexity At most
  * `count(subsequence) * (count(range) - count(subsequence) + 1)`
  * applications of `predicate`.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2, typename BinaryPredicate>
Range1 search_last(Range1 range, Range2 subsequence,
                   BinaryPredicate predicate)
{
	for (auto temp = range;; temp.pop_front()) {
		temp = search(temp, subsequence, predicate);

		if (temp.empty())
			break;

		range = temp;
	}
	return range;
}

/** Advances `range` to the last occurrence of `subsequence`, as
  * determined by `==`.
  *
  * \tparam Range1 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \param range The range to search.
  *
  * \param subsequence The subsequence of elements to search for.
  *
  * \returns The smallest tail `t` of `range`, containing at least
  * `count(subsequence)` elements such that `t_i == subsequence_i` for
  * each `i` in `[0, count(subsequence))`, where `_i` indicates the
  * `i`th element of a range. If no such tail exists, then the empty
  * tail is returned.
  *
  * \complexity At most
  * `count(subsequence) * (count(range) - count(subsequence) + 1)`
  * equality comparisons.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
Range1 search_last(Range1 range, Range2 subsequence)
{
	for (auto temp = range;; temp.pop_front()) {
		temp = search(temp, subsequence);

		if (temp.empty())
			break;

		range = temp;
	}
	return range;
}

/** Advances `range1` until an element is found for which `predicate`
  * return `true` for some element in `range2`.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam BinaryPredicate A binary predicate on the element types of
  * `Range1` and `Range2`.
  *
  * \param range1 The range in which to search.
  *
  * \param range2 The range of elements to search for.
  *
  * \param predicate The predicate to compare elements with.
  *
  * \returns The largest tail `t` of `range1` such that for some `e` in
  * `range2`, `predicate(t.front(), e)`. If no such tail exists, the
  * empty tail is returned.
  *
  * \complexity At most `count(range1) * count(range2)` applications of
  * `predicate`.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2, typename BinaryPredicate>
Range1 find_first_of(Range1 range1, Range2 const range2,
                     BinaryPredicate predicate)
{
	for (; !range1.empty(); range1.pop_front()) {
		for (auto r = range2; !r.empty(); r.pop_front()) {
			if (predicate(range1.front(), r.front()))
				break;
		}
	}

	return range1;
}

/** Advances `range1` until an element in `range2` is found.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \param range1 The range in which to search.
  *
  * \param range2 The range of elements to search for.
  *
  * \returns The largest tail `t` of `range1` such that for some `e` in
  * `range2`, `t.front() == e`. If no such tail exists, the empty tail
  * is returned.
  *
  * \complexity At most `count(range1) * count(range2)` equality
  * comparisions.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
Range1 find_first_of(Range1 range1, Range2 const range2)
{
	for (; !range1.empty(); range1.pop_front()) {
		for (auto r = range2; !r.empty(); r.pop_front()) {
			if (range1.front() == r.front())
				break;
		}
	}

	return range1;
}

/** Advances `range` until two consecutive elements are found satisfying
  * `predicate`.
  *
  * \tparam Range A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam BinaryPredicate A binary predicate on the value type of
  * `Range`.
  *
  * \param range The range to search.
  *
  * \param predicate The predicate with which to compare elements.
  *
  * \returns The largest tail `t` of `range` such that
  * `predicate(t_0, t_1)` where `t_0` and `t_1` are the first and second
  * elements of `t`. If no such tail exists, the empty tail is returned.
  *
  * \complexity For non-empty `range`, exactly
  * `min(count(range) - count(result) + 1, count(result) - 1)`
  * applications of `predicate`, where `result` is the returned range.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename BinaryPredicate>
Range adjacent_find(Range range, BinaryPredicate predicate)
{
	auto ahead = range;
	ahead.pop_front();

	for (; !ahead.empty(); range.pop_front(), ahead.pop_front()) {
		if (predicate(range.front(), ahead.front()))
			break;
	}

	return range;
}

/** Advances `range` until two consecutive equal elements are found.
  *
  * \tparam Range A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \param range The range to search.
  *
  * \returns A copy of `range`, advanced such that the first two
  * elements satisfy `predicate`. If no such elements are found, then
  * the copy is exhausted.
  *
  * \complexity For non-empty `range`, exactly
  * `min(count(range) - count(result) + 1, count(result) - 1)` equality
  * comparisons, where `result` is the returned range.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range>
Range adjacent_find(Range range)
{
	auto ahead = range;
	ahead.pop_front();

	for (; !ahead.empty(); range.pop_front(), ahead.pop_front()) {
		if (range.front() == ahead.front())
			break;
	}

	return range;
}

/** Advances `range` until `subsequence` is found as a subrange, as
  * determined by `predicate`.
  *
  * \tparam Range1 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam BinaryPredicate A binary predicate on the element types of
  * `Range1` and `Range2`.
  *
  * \param range The range in which to search.
  *
  * \param subsequence The range of elements to search for.
  *
  * \param predicate The predicate to compare elements with.
  *
  * \returns The largest tail `t` of `range`, containing at least
  * `count(subsequence)` elements such that `pred(t_i, subsequence_i)`
  * for each `i` in `[0, count(subsequence))`, where `_i` indicates the
  * `i`th element of a range. If no such tail exists, then the empty
  * tail is returned.
  *
  * \complexity At most `count(range) * count(subsequence)` applications
  * of `predicate`.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2, typename BinaryPredicate>
Range1 search(Range1 range, Range2 const subsequence,
              BinaryPredicate predicate)
{
	for (; !range.empty(); range.pop_front()) {
		// Check for a mismatch.
		auto result = mismatch(range, subsequence, predicate);

		// If the advance second range is empty, then it is a subrange
		// of the first.
		if (::std::get<1>(result).empty())
			break;
	}

	return range;
}

/** Advances `range` until `subsequence` is found as a subrange.
  *
  * \tparam Range1 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \param range The range in which to search.
  *
  * \param subsequence The range of elements to search for.
  *
  * \returns The largest tail `t` of `range`, containing at least
  * `count(subsequence)` elements such that `t_i == subsequence_i`
  * for each `i` in `[0, count(subsequence))`, where `_i` indicates the
  * `i`th element of a range. If no such tail exists, then the empty
  * tail is returned.
  *
  * \complexity At most `count(range) * count(subsequence)` equality
  * comparisons.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
Range1 search(Range1 range, Range2 const subsequence)
{
	for (; !range.empty(); range.pop_front()) {
		// Check for a mismatch.
		auto result = mismatch(range, subsequence);

		// If the advance second range is empty, then it is a subrange
		// of the first.
		if (::std::get<1>(result).empty())
			break;
	}

	return range;
}
}
}

#endif // VLINDE_ALGORITHM_NON_MODIFYING_HPP
