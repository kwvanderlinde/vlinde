#ifndef VLINDE_ALGORITHM_MIN_MAX_HPP
#define VLINDE_ALGORITHM_MIN_MAX_HPP

namespace vlinde
{
namespace algorithm
{
namespace detail
{
struct generic_equals
{
  template<typename T, typename U>
  bool operator()(T const& lhs, U const& rhs) const noexcept
  {
    return lhs == rhs;
  }
};
}

/** Checks if `lhs` is lexicographically less than `rhs`.
  *
  * \tparam Lhs A [Readable][] [Forward][] [Range][].
  * \tparam Rhs A [Readable][] [Forward][] [Range][].
  *
  * \tparam Compare A comparison on the elements types of `Lhs` and `Rhs`.
  *
  * \param lhs The left hand side of the comparison.
  * \param rhs The left hand side of the comparison.
  *
  * \param comp The comparison object to use on individual elements.
  *
  * \returns `true` if `lhs` is lexicographically less than `rhs`.
  *
  * \complexity At most `2 * min(count(lhs), count(rhs))` applications of
  * `comp`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */

template <typename Lhs, typename Rhs, typename Compare>
bool lexicographical_compare(Lhs lhs, Rhs rhs, Compare comp)
{
	for (; !(lhs.empty() || rhs.empty()); lhs.pop_front(), rhs.pop_front()) {
		if (comp(lhs.front(), rhs.front()))
			return true;
		if (comp(rhs.front(), lhs.front()))
			return false;
	}

	return lhs.empty() && !rhs.empty();
}
/** Checks if `lhs` is lexicographically less than `rhs`.
  *
  * \tparam Lhs A [Readable][] [Forward][] [Range][].
  * \tparam Rhs A [Readable][] [Forward][] [Range][].
  *
  * \tparam Compare A comparison on the elements types of `Lhs` and `Rhs`.
  *
  * \param lhs The left hand side of the comparison.
  * \param rhs The left hand side of the comparison.
  *
  * \param comp The comparison object to use on individual elements.
  *
  * \returns `true` if `lhs` is lexicographically less than `rhs`.
  *
  * \complexity At most `2 * min(count(lhs), count(rhs))` applications of
  * `comp`.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */

template <typename Lhs, typename Rhs>
bool lexicographical_compare(Lhs lhs, Rhs rhs)
{
	return lexicographical_compare(lhs, rhs, detail::generic_equals{});
}
}
}

#endif // VLINDE_ALGORITHM_MIN_MAX_HPP
