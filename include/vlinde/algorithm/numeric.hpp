#ifndef VLINDE_ALGORITHM_NUMERIC_HPP
#define VLINDE_ALGORITHM_NUMERIC_HPP

#include <functional>
#include <utility>

namespace vlinde
{
namespace algorithm
{
template <typename ForwardWritableRange, typename T>
void iota(ForwardWritableRange range, T value)
{
	for (; !range.empty(); range.pop_front()) {
		range.front() = value++;
	}
}

template <typename Range, typename T, typename BinaryOperation>
T accumulate(Range range, T init, BinaryOperation op)
{
	for (; !range.empty(); range.pop_front()) {
		init = op(init, range.front());
	}

	return init;
}

template <typename Range, typename T>
T accumulate(Range range, T init)
{
	for (; !range.empty(); range.pop_front()) {
		init += range.front();
	}

	return init;
}

/** Computes the inner product of two ranges.
  *
  * If the ranges are not the same size, then the summation terminates when one
  * of the ranges is exhausted.
  */
template <typename Lhs, typename Rhs, typename T, typename BinaryOperation1,
          typename BinaryOperation2>
T inner_product(Lhs lhs, Rhs rhs, T value, BinaryOperation1 summer,
                BinaryOperation2 multiplier)
{
	for (; !(lhs.empty() || rhs.empty()); lhs.pop_front(), rhs.pop_front()) {
		value = summer(value, multiplier(lhs.front(), rhs.front()));
	}

	return value;
}

/** Computes the inner product of two ranges.
  *
  * If the ranges are not the same size, then the summation terminates when one
  * of the ranges is exhausted.
  */
template <typename Lhs, typename Rhs, typename T>
T inner_product(Lhs lhs, Rhs rhs, T value)
{
	return inner_product(lhs, rhs, value, ::std::plus<T>{},
	                     ::std::multiplies<T>{});
}

template <typename ReadRange, typename WriteRange>
WriteRange adjacent_difference(ReadRange input, WriteRange output)
{
	using value_type = typename ReadRange::value_type;

	value_type last = input.front();
	input.pop_front();
	output.front() = last;
	output.pop_front();

	while (!(input.empty() || output.empty())) {
		auto current = input.front();
		input.pop_front();

		output.front() = last - current;
		output.pop_front();

		last = ::std::move(current);
	}

	return output;
}

template <typename ReadRange, typename WriteRange, typename BinaryOperation>
WriteRange adjacent_difference(ReadRange input, WriteRange output,
                               BinaryOperation op)
{
	auto last = input.front();
	input.pop_front();
	output.front() = last;
	output.pop_front();

	while (!(input.empty() || output.empty())) {
		auto current = input.front();
		input.pop_front();

		output.front() = op(last, current);
		output.pop_front();

		last = ::std::move(current);
	}

	return output;
}

template <typename ReadRange, typename WriteRange>
WriteRange partial_sum(ReadRange input, WriteRange output)
{
	if (input.empty())
		return output;

	auto sum = input.front();
	input.pop_front();

	output.front() = sum;
	output.pop_front();

	while (!(input.empty() || output.empty())) {
		sum += input.front();
		input.pop_front();

		output.front() = sum;
		output.pop_front();
	}

	return output;
}

template <typename ReadRange, typename WriteRange, typename BinaryOperation>
WriteRange partial_sum(ReadRange input, WriteRange output, BinaryOperation op)
{
	if (input.empty())
		return output;

	auto sum = input.front();
	input.pop_front();

	output.front() = sum;
	output.pop_front();

	while (!(input.empty() || output.empty())) {
		sum = op(sum, input.front());
		input.pop_front();

		output.front() = sum;
		output.pop_front();
	}

	return output;
}
}
}

#endif // VLINDE_ALGORITHM_NUMERIC_HPP
