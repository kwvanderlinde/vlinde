#ifndef VLINDE_ALGORITHM_MODIFYING_HPP
#define VLINDE_ALGORITHM_MODIFYING_HPP

#include "non_modifying.hpp"

#include <vlinde/range/take.hpp>

#include <cassert>
#include <random>
#include <utility>

namespace vlinde
{
namespace algorithm
{
/** Copies elements from `source` into `destination` until either range
  * is exhausted.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range1 A [Forward][] [Range][] which is [Writable][] from
  * `Range1::reference`.
  *
  * \param source The source of the copy.
  *
  * \param destination The destination of the copy.
  *
  * \complexity Exactly `min(count(source), count(destination))` copy
  * operations.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [writable]: \ref writable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
Range2 copy(Range1 source, Range2 destination)
{
	for (; !source.empty() && !destination.empty();
	     source.pop_front(), destination.pop_front()) {
		destination.front() = source.front();
	}
	return destination;
}

/** Moves elements from `source` into `destination` until either range
  * is exhausted.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range1 A [Forward][] [Range][] which is [Writable][] from
  * rvalues of type `Range1::reference`.
  *
  * \param source The source of the move.
  *
  * \param destination The destination of the move.
  *
  * \complexity Exactly `min(count(source), count(destination))` move
  * operations.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [writable]: \ref writable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
Range2 move(Range1 source, Range2 destination)
{
	for (; !source.empty() && !destination.empty();
	     source.pop_front(), destination.pop_front()) {
		destination.front() = ::std::move(source.front());
	}
	return destination;
}

/** Fills `range` with the values returned successively from
  * `generator`.
  *
  * \tparam Range A [Forward][] [Range][], [Writable][] from
  * `decltype(generator())`.
  *
  * \tparam Generator A nullary function type whose result type can be
  * output to `Range`.
  *
  * \param range The range to fill.
  *
  * \param generator The function used to fill `range`.
  *
  * \todo Replace `generate` with
  * `copy(generator_range(generator), range)`.
  *
  * [forward]: \ref forward_range
  * [writable]: \ref writable_range
  * [range]: \ref range
  */
template <typename Range, typename Generator>
void generate(Range range, Generator generator)
{
	for (; !range.empty(); range.pop_front()) {
		range.front() = generator();
	}
}

/** Rearranges the elements of `range` such that all elements in `range`
  * for which `predicate` returns `false` appear at the front of
  * `range`.
  *
  * The rearrangement is performed by moving elements.
  *
  * \tparam Range A [Multi Pass][] [Readable][] [Forward][] [Range][]
  * where `Range::reference` is Swappable.
  *
  * \tparam UnaryPredicate A unary predicate on the element type of
  * `Range`.
  *
  * \param range The range to reorder.
  *
  * \param predicate The predicate to use.
  *
  * \returns The range which follows the initial range elements of
  * desired elements.
  *
  * \complexity Exactly `count(range)` applications of `predicate`.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryPredicate>
Range remove_if(Range range, UnaryPredicate predicate)
{
	// Advance range to the first occurence. This avoids moving an
	// initial sequence of good elements into themeselves.
	range = find_if(range, predicate);

	// Continue advance and copying elements.
	for (auto probe = range; !probe.empty(); probe.pop_front()) {
		if (!predicate(probe.front())) {
			range.front() = ::std::move(probe.front());
			range.pop_front();
		}
	}

	return range;
}

/** Replaces all elements of `range` for which `predicate` return `true`
  * by `value`.
  *
  * \tparam Range A [Readable][] [Forward][] [Range][] which is also
  * [Writable][] from `T`.
  *
  * \tparam UnaryPredicate A unary predicate on the element type of
  * `Range`.
  *
  * \tparam T The type of value to insert. Must be outputable to
  *`range`.
  *
  * \param range The range to modify.
  *
  * \param predicate The predicate to apply to elements of `range`.
  *
  * \param value The value used to replace elements.
  *
  * \complexity Exactly `count(range)` applications of the predicate.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [writable]: \ref writable_range
  * [range]: \ref range
  */
template <typename Range, typename UnaryPredicate, typename T>
void replace_if(Range range, UnaryPredicate predicate, T const & value)
{
	for (; !range.empty(); range.pop_front()) {
		if (predicate(range.front()))
			range.front() = value;
	}
}

/** Swaps the fronts of `range1` and `range2`.
  *
  * \requires `!range1.empty() && !range2.empty()`.
  *
  * \requires `Range1::reference` and `Range2::%reference` are
  * `Swappable`.
  *
  * \tparam Range1 A [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Readable][] [Forward][] [Range][].
  *
  * \param range1 The first range.
  *
  * \param range2 The second range.
  *
  * \complexity One swap.
  *
  * \todo Add support for non-`Swappable` types when `Range1` and
  * `Range2` are also `OutputRange`s.
  *
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
::std::tuple<Range1, Range2> swap_front(Range1 range1, Range2 range2)
{
	using std::swap;
	swap(range1.front(), range2.front());
}

/** Swaps values in `range1` and `range2` until either is exhausted.
  *
  * \requires `Range1::%reference` and `Range2::%reference` are
  * `Swappable`.
  *
  * \tparam Range1 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \param range1 The first range to swap.
  *
  * \param range2 The second range to swap.
  *
  * \returns The remaining unswapped ranges. One of these ranges will be
  * empty.
  *
  * \complexity Exactly `min(count(range1), count(range2))` swaps.
  *
  * \todo Add support for non-`Swappable` types when `Range1` and
  * `Range2` are also `OutputRange`s.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
::std::tuple<Range1, Range2> swap_ranges(Range1 range1, Range2 range2)
{
	using std::swap;

	for (; !range1.empty() && !range2.empty();
	     range1.pop_front(), range2.pop_front()) {
		swap_front(range1, range2);
	}
	return ::std::make_tuple(range1, range2);
}

/** Swaps values in `range` such that `range` is reversed.
  *
  * \tparam Range A [Readable][] [Bidirectional][] [Range][], where
  * `Range::reference` is Swappable.
  *
  * \param range The range to reverse.
  *
  * \complexity `count(range) / 2` swaps.
  *
  * [multi pass]: \ref multi_pass_range
  * [bidirectional]: \ref bidirectional_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range>
void reverse(Range range)
{
	using std::swap;

	while (true) {
		swap(range.front(), range.back());

		range.pop_front();
		if (range.empty())
			break;

		range.pop_back();
		if (range.empty())
			break;
	}
}

/** Swaps values in `range1` and `range2` such that the elements in
  * `range2` come before any other elements in `range1`.
  *
  * The relative order of elements within each range is not changed.
  *
  * \requires `Range1::%reference` and `Range2::%reference` are
  * `Swappable`.
  *
  * \tparam Range1 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \tparam Range2 A [Multi Pass][] [Readable][] [Forward][] [Range][].
  *
  * \param front The front range to be moved to the back.
  *
  * \param back The back range to be moved to the front.
  *
  * \complexity `O(count(front) + count(back))`.
  *
  * \todo Specialize this algorithm for `BidirectionRange`.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range1, typename Range2>
void rotate(Range1 front, Range2 back)
{
	while (!front.empty() && !back.empty()) {
		// We need to remember the back range.
		auto back0 = back;

		// Swap the ranges to start.
		// We don't use `swap_ranges` here since we need to count the
		// number of swaps performed.
		::std::size_t num_swaps{0};
		for (num_swaps = 0; !front.empty() && !back.empty();
		     front.pop_front(), back.pop_front(), ++num_swaps) {
			swap_front(front, back);
		}

		// Touch up the rest.
		if (back.empty()) {
			// All the elements from the back were brought to the front.
			if (front.empty()) {
				// There's nothing more to do.
				return;
			}

			// Next time through, we'll rotate the new front with the
			// original back.
			back = back0;
		} else {
			assert(front.empty());

			// `front` was too short. We'll have to do some rotations on
			// the original `back` to finish.
			rotate(::vlinde::range::take(back0, num_swaps), back);
			return;
		}
	}
}

/** Reorders the elements in `range` such that each permutation has
  * equal probability of appearance.
  *
  * \tparam Range A [Readable][] [Sized][] [Random Access][] [Range][]
  * where `Range::reference` is Swappable.
  *
  * \tparam URNG A `UniformRandomNumberGenerator`.
  *
  * \param range The range to shuffle.
  *
  * \param generator The entropy source.
  *
  * \complexity Exactly `count(range) - 1` swaps.
  *
  * [sized]: \ref sized_range
  * [random access]: \ref random_access_range
  * [readable]: \ref readable_range
  * [range]: \ref range
  */
template <typename Range, typename URNG>
void shuffle(Range range, URNG && generator)
{
	using size_type = typename Range::index_type;
	using distribution_type = ::std::uniform_int_distribution<size_type>;
	using param_type = typename distribution_type::param_type;
	using ::std::swap;

	distribution_type distribution;
	for (size_type n = range.size(), i = n - 1; i > 0; --i) {
		auto const index = distribution(generator, param_type(0, i));
		swap(range[i], range[index]);
	}
}

/** Rearranges the elements of `range` such that all consecutive
  * duplicate elements are moved to the back of `range`.
  *
  * Duplicity is determined by `predicate`.
  *
  * \tparam Range A [Multi Pass][] [Readable][] [Forward][] [Range][]
  * which is [Writable][] from rvalues of type `Range::reference`.
  *
  * \tparam BinaryPredicate A binary predicate on the element type of
  * `Range`.
  *
  * \param range The range to reorder.
  *
  * \param predicate The predicate with which to compare elements.
  *
  * \returns The tail range of moved from elements.
  *
  * \complexity Exactly `count(range) - 1` applications of `predicate`.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [writable]: \ref writable_range
  * [range]: \ref range
  */
template <typename Range, typename BinaryPredicate>
Range unique(Range range, BinaryPredicate predicate)
{
	// Advance range to the first occurence. This avoids moving an
	// initial sequence of good elements into themeselves.
	range = adjacent_find(range, predicate);

	// Continue advance and copying elements.
	for (auto probe = range; !probe.empty(); probe.pop_front()) {
		if (!predicate(range.front(), probe.front())) {
			range.pop_front();
			range.front() = ::std::move(probe.front());
		}
	}

	return range;
}

/** Rearranges the elements of `range` such that all consecutive
  * duplicate elements are moved to the back of `range`.
  *
  * \tparam Range A [Multi Pass][] [Readable][] [Forward][] [Range][]
  * which is [Writable][] from rvalues of type `Range::reference`.
  *
  * \param range The range to reorder.
  *
  * \returns The tail range of moved from elements.
  *
  * \complexity Exactly `count(range) - 1` equality comparisons.
  *
  * [multi pass]: \ref multi_pass_range
  * [forward]: \ref forward_range
  * [readable]: \ref readable_range
  * [writable]: \ref writable_range
  * [range]: \ref range
  */
template <typename Range>
Range unique(Range range)
{
	// Advance range to the first occurence. This avoids moving an
	// initial sequence of good elements into themeselves.
	range = adjacent_find(range);

	// Continue advance and copying elements.
	for (auto probe = range; !probe.empty(); probe.pop_front()) {
		if (!(range.front() == probe.front())) {
			range.pop_front();
			range.front() = ::std::move(probe.front());
		}
	}

	return range;
}
}
}

#endif // VLINDE_ALGORITHM_MODIFYING_HPP
