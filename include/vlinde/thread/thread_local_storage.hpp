#ifndef VLINDE_THREAD_THREAD_LOCAL_STORAGE_HPP
#define VLINDE_THREAD_THREAD_LOCAL_STORAGE_HPP

#include "detail/accumulating_buffer.hpp"
#include "detail/tls_control_block.hpp"
#include "detail/tls_map.hpp"

#include <boost/iterator/transform_iterator.hpp>
#include <vlinde/utility/scope_guard.hpp>

#include <type_traits>

namespace vlinde
{
namespace thread
{
/** An instantiable implementation of Thread Local Storage (TLS).
  *
  * Unlike objects with thread local storage duration (i.e., those
  * declared with the `thread_local` keyword), a `thread_local_storage`
  * instance does not have to be static. Instead, `thread_local_storage`
  * is a map from threads to `T` instances. In this regard, a
  * `thread_local_storage<T>` behaves much like a collection of
  * `::%std::pair<::%std::thread::id const, T>` values.
  *
  * This analogy with a map structure continues with resource management
  * `thread_local_storage` adopts a lazy construction strategy whereby
  * an instance of `T` is allocated only when a thread first requests a
  * `T` instance from the `thread_local_storage`. This is analogous to
  * auto insertion of the standard map types. Similarly, destruction of
  * the `T` values is deterministic: all `T` instances managed by the
  * `thread_local_storage` object are destroyed once the
  * `thread_local_storage` object is destroyed.
  *
  * \tparam
  * T The type of object stored in the `thread_local_storage`
  * object.
  */
template <typename T>
class thread_local_storage
{
	/** The type of buffer used to store control blocks.
	  *
	  * `block_buffer_type` provides a lock-free `push` method which
	  * accepts `tls_control_block<T> *` values. Additionally,
	  * `block_buffer_type::const_iterator` must be a constant iterator
	  * satisfying the `ForwardIterator` requirements. Finally,
	  * `block_buffer_type` defines the usual `cbegin` and `cend` member
	  * functions which return `const_iterator`s.
	  */
	using block_buffer_type =
	    detail::accumulating_buffer<detail::tls_control_block<T>*>;

	/** Adds `const`-ness to `T` depending on `is_const`.
	  *
	  * \tparam is_const
	  * Whether or not `const` should be applied to `T`.
	  */
	template <bool is_const>
	using consted_type = typename ::std::conditional<
	    is_const, typename ::std::add_const<T>::type, T>::type;

	/** A functor for extracting values from `tls_control_block`
	 * objects.
	  *
	  * \tparam is_const
	  * Whether the result type should be a reference to a value, or a
	  * reference to a `const` value.
	  */
	template <bool is_const>
	struct transformer
	{
		/** Maps a `tls_control_block` to its value.
		  *
		  * \param arg
		  * A pointer to a `tls_control_block`.
		  *
		  * \return
		  * A reference to the `tls_control_block`'s value, with
		  * appropriate `const`-ness.
		  *
		  * \exsafety
		  * No throw.
		  */
		consted_type<is_const>&
		operator()(detail::tls_control_block<T>* arg) const noexcept
		{
			return arg->value;
		}
	};

	/** A `ForwardIterator` over the `T` values in `*this`.
	  *
	  * Depending on `is_const`, `tls_iterator` will model either a
	  * constant iterator or a mutable iterator.
	  *
	  * \tparam is_const
	  * If `true`, `tls_iterator<is_const>` is a constant iterator.
	  * Otherwise, `tls_iterator<is_const>` is a mutable iterator.
	  */
	template <bool is_const>
	using tls_iterator = ::boost::transform_iterator<
	    transformer<is_const>,
	    typename block_buffer_type::const_iterator>;

	/** Relinquish ownership of a control block.
	  *
	  * This method handles destruction of the value stored in the given
	  * block, and cleans up the block itself if it has unique ownership
	  * of the block.
	  *
	  * \param block
	  * A pointer to the control block to release.
	  *
	  * \exsafety No throw.
	  */
	static void
	s_release_block(detail::tls_control_block<T>* block) noexcept
	{
		assert(block);

		// Destroy the value.
		block->value.~T();

		// Mark `block` as dead.
		if (block->is_dead.test_and_set()) {
			/* `block` was already marked as dead by a `tls_map`. It's
			 * our responsibility to clean it up.
			 */
			delete block;
		}
	}

	/** The per-thread storage of `T` instances.
	  *
	  * Each `T` instance which is owned by a `thread_local_storage<T>`
	  * instance is stored in some `t_map`, and every `T` instance
	  * stored in a `t_map` is owned by some `thread_local_storage<T>`
	  * instance.
	  *
	  * \remark
	  * The use of a thread local map from `thread_local_storage<T>`
	  * objects to `tls_control_block`s may seem counterintuitive. The
	  * inuitive alternative is to have an instance member which maps
	  * `::%std::thread::id` to `T *`. However, the former requires no
	  * synchronization on the maps, while the latter requires
	  * synchronizing operations on the map, whether by locks or by
	  * using a lock-free map. Either way, the current approach has less
	  * implementation overhead.
	  */
	static thread_local detail::tls_map<T> t_map;

	/** Maintains a set of control blocks owned `*this`.
	  *
	  * Maintaining this set of control blocks allows iterating over all
	  * the `T` values owned by `*this`. Additionally, when a
	  * `thread_local_storage<T>` is destroyed, it may signal each
	  * `tls_map<T>` that it no longer needs to store the memory for the
	  * corresponding `T` value.
	  */
	block_buffer_type m_control_blocks;

public:
	/** A mutable iterator over the `T` instances managed by the
	  * `thread_local_storage` object.
	  */
	using iterator = tls_iterator<false>;

	/** A constant iterator over the `T` instances managed by the
	  * `thread_local_storage` object.
	  */
	using const_iterator = tls_iterator<true>;

	/** Default constructor.
	  *
	  * Initializes a `thread_local_storage` which does not manage any
	  * values.
	  */
	thread_local_storage() = default;

	thread_local_storage(thread_local_storage const&) = delete;

	thread_local_storage(thread_local_storage&&) = delete;

	/** Destructor.
	  *
	  * Destroys all `T` instances managed by `*this`.
	  *
	  * \exsafety
	  * No throw.
	  */
	~thread_local_storage() noexcept
	{
		/* Destroy all our values, and clean up any control blocks we
		 * have unique ownership of.
		 */
		for (auto block : m_control_blocks) {
			s_release_block(block);
		}
	}

	thread_local_storage& operator=(thread_local_storage const&) =
	    delete;

	thread_local_storage& operator=(thread_local_storage&&) = delete;

	/** Retrieves a `T` instance specific to the current thread.
	  *
	  * If no `T` instance exists for the current thread, then one
	  * is created.
	  *
	  * \return
	  * A reference to the `T` instance for the current thread.
	  *
	  * \exsafety
	  * Strong exception safety.
	  */
	T& get()
	{
		// Get a control block from the map.
		auto result = t_map.get(this);

		// Was this block just inserted?
		if (result.second) {
			auto block = result.first;

			// Rollback in case this next operation throws.
			auto guard = ::vlinde::utility::scope_guard([&]() {
				// Remove the block from the map.
				t_map.erase(this);
				// Cleanup the block.
				s_release_block(block);
			});

			// Add the control block to our buffer.
			m_control_blocks.push(block);

			// Operation succeeded. No need to rollback.
			guard.dismiss();
		}

		return result.first->value;
	}

	/** An `iterator` to the first element in the `thread_local_storage`
	  * object, or `this->end()` if it is empty.
	  *
	  * \exsafety
	  * No throw.
	  */
	iterator begin() noexcept
	{
		return iterator{m_control_blocks.cbegin()};
	}

	/** An `iterator` pointing past the last element in the
	  * `thread_local_storage` object.
	  *
	  * \exsafety
	  * No throw.
	  */
	iterator end() noexcept
	{
		return iterator{m_control_blocks.cend()};
	}

	/** A `const_iterator` to the first element in the
	  * `thread_local_storage` object, or `this->cend()` if it is empty.
	  *
	  * \exsafety
	  * No throw.
	  */
	const_iterator begin() const noexcept
	{
		return this->cbegin();
	}

	/** A `const_iterator` pointing past the last element in the
	  * `thread_local_storage` object.
	  *
	  * \exsafety
	  * No throw.
	  */
	const_iterator end() const noexcept
	{
		return this->cend();
	}

	/** A `const_iterator` to the first element in the
	  * `thread_local_storage` object, or `this->cend()` if it is empty.
	  *
	  * `a.cbegin()` is equivalent to
	  * `const_cast<thread_local_storage<T> const &>(a).begin()`.
	  *
	  * \exsafety No throw.
	  */
	const_iterator cbegin() const noexcept
	{
		return const_iterator{m_control_blocks.cbegin()};
	}

	/** A `const_iterator` pointing past the last element in the
	  * `thread_local_storage` object.
	  *
	  * `a.cend()` is equivalent to
	  * `const_cast<thread_local_storage<T> const &>(a).end()`.
	  *
	  * \exsafety No throw.
	  */
	const_iterator cend() const noexcept
	{
		return const_iterator{m_control_blocks.cend()};
	}
};

template <typename T>
thread_local detail::tls_map<T> thread_local_storage<T>::t_map{};
}
}

#endif // VLINDE_THREAD_THREAD_LOCAL_STORAGE_HPP
