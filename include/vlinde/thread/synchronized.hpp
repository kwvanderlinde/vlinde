#ifndef VLINDE_THREAD_SYNCHRONIZED_HPP
#define VLINDE_THREAD_SYNCHRONIZED_HPP

#include <mutex>
#include <type_traits>

namespace vlinde
{
namespace thread
{
/** Wraps an object with access synchronized via a `Mutex`.
  *
  * \tparam T
  * The type of object to which access needs to be synchronized.
  *
  * \tparam Mutex
  * An implementation of the `Mutex` concept, which is used to guard
  * access to the wrapped object.
  */
template <typename T, typename Mutex = ::std::mutex>
class synchronized
{
public:
	/** The type of value which is synchronized.
	  */
	using value_type = T;

	/** The mutex type used to synchronize the `value_type`.
	  */
	using mutex_type = Mutex;

private:
	/** Indicates whether the parameter list `Arg&&, Args&&...` is
	  * suitable for the copy constructor.
	  *
	  * A parameter list is suitable for a copy constructor if it has
	  * only one parameter, and that parameter is a
	  *`synchronized<value_type, mutex_type>`, or a reference to a
	  * possibly `const` qualified
	  * `synchronized<value_type, mutex_type>`.
	  *
	  * \return
	  * `true` if the parameter list is suitable for the copy
	  * constructor, else `false`.
	  */
	template <typename Arg, typename... Args>
	static constexpr bool s_is_copy_constructor_arg_list()
	{
		using self = synchronized<value_type, mutex_type>;
		using other = typename ::std::decay<Arg>::type;
		return (sizeof...(Args) == 0) &&
		       ::std::is_same<self, other>::value;
	}

	/** The mutex which is used to synchronize access to `m_value`.
	  */
	mutable Mutex m_mutex;

	/** The value to be synchronized.
	  */
	T m_value;

public:
	/** Forwarding constructor.
	  *
	  * Forwards `args` to the constructor of the wrapped
	  * `value_type`.
	  *
	  * \param args
	  * The arguments to `value_type`'s constructor.
	  */
	template <typename... Args,
	          typename = typename ::std::enable_if<
	              !s_is_copy_constructor_arg_list<Args...>()>::type>
	synchronized(Args&&... args)
	: m_mutex{}, m_value{ ::std::forward<Args>(args)...}
	{
	}

	/** Copy constructor.
	  *
	  * \param other
	  * A `synchronized<T>` from which `*this` will be constructed.
	  */
	synchronized(synchronized const& other)
	: m_mutex{},
	  m_value{other([](value_type const& value) { return value; })}
	{
	}

	/** Move constructor.
	  */
	synchronized(synchronized&& other)
	: m_mutex{},
	  m_value{
	      other([](value_type& value) { return ::std::move(value); })}
	{
	}

	/** Destructor.
	  */
	~synchronized() = default;

	/** Copy assignment operator.
	  */
	synchronized& operator=(synchronized const& other)
	{
		/* Note: we don't take both locks at once, so no deadlock will
		 * occur due to our actions.
		 */

		// Get the value from other.
		auto result =
		    other([](value_type const& value) { return value; });

		// Set our value.
		(*this)([&result](value_type& value) {
			value = ::std::move(result);
		});

		return *this;
	}

	/** Move assignment operator.
	  */
	synchronized& operator=(synchronized&& other)
	{
		/* Note: we don't take both locks at once, so no deadlock will
		 * occur due to our actions.
		 */

		// Get the value from other.
		auto result =
		    other([](value_type& value) { return ::std::move(value); });

		// Set our value.
		(*this)([&result](value_type& value) {
			value = ::std::move(result);
		});

		return *this;
	}

	/** Accesses the stored `value_type` object, synchronized using
	  * a `mutex_type` instance.
	  *
	  * If `func` causes a race condition, then result are undefined.
	  * This may happen, for example, if `func` returns or otherwise
	  * stores a reference to the wrapped object. Any read or write to
	  * such a reference may race unless it occurs during a call to
	  * operator().
	  *
	  * \param func
	  * The critical section.
	  *
	  * \tparam Func
	  * A (reference to a) function object type which is callable with
	  * an lvalue of type `value_type`.
	  *
	  * \return
	  * The value returned by invoking `func` on the wrapped
	  * `value_type` object.
	  */
	template <typename Func>
	auto operator()(Func func) -> decltype(func(m_value))
	{
		::std::lock_guard<Mutex> _{m_mutex};
		return func(m_value);
	}

	/** Access into a `const synchronized`.
	 *
	 * \param func
	 * The critical section.
	 *
	 * \tparam Func
	 * A (reference to a) function object type which is callable with an
	 * lvalue of type `const value_type`.
	 *
	 * \return
	 * The value returned by invoking `func` on the wrapped `value_type`
	 * object.
	 */
	template <typename Func>
	auto operator()(Func func) const -> decltype(func(m_value))
	{
		::std::lock_guard<Mutex> _{m_mutex};
		return func(m_value);
	}
};
}
}

#endif // VLINDE_THREAD_SYNCHONIZED_HPP
