#ifndef VLINDE_THREAD_NULL_MUTEX_HPP
#define VLINDE_THREAD_NULL_MUTEX_HPP

#include <atomic>

namespace vlinde
{
namespace thread
{
/** An implementation of the `Mutex` concept which never blocks.
  *
  * A `null_mutex` may be owned by any number of execution agents at
  * once. Indeed, every call to `lock` will succeed without blocking.
  */
class null_mutex
{
public:
	/** Default constructor.
	  *
	  * \cpp11ref
	  * 30.4.1.2/3
	  */
	null_mutex() = default;

	/** Copy constructor.
	  *
	  * `null_mutex` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	null_mutex(null_mutex const&) = delete;

	/** Move constructor.
	  *
	  * `null_mutex` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	null_mutex(null_mutex&&) = delete;

	/** Copy assignment operator.
	  *
	  * `null_mutex` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	null_mutex& operator=(null_mutex const&) = delete;

	/** Move assignment operator.
	  *
	  * `null_mutex` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	null_mutex& operator=(null_mutex&&) = delete;

	/** Destructor.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	~null_mutex() = default;

	/** Obtains a lock of the `null_mutex` for the current agent.
	  *
	  * The call to `lock` will never block the current execution agent.
	  *
	  * \remark
	  * Prior calls to `unlock` synchronize with this call to `lock`.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §30.2.5.2/2;
	  * §30.4.1.2/6-13
	  */
	void lock() noexcept
	{
		// the fence is to satisfy the synchronization requirement.
		::std::atomic_thread_fence(
		        ::std::memory_order::memory_order_acquire);
	}

	/** Releases the lock on the `null_mutex`.
	  *
	  * This operation synchronizes with subsequent `lock` operations
	  * that obtain ownership on the same object.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §30.2.5.2/3-4;
	  * §30.4.1.2/21-26
	  */
	void unlock() noexcept
	{
		// the fence is to satisfy the synchronization requirement.
		::std::atomic_thread_fence(
		        ::std::memory_order::memory_order_release);
	}

	/** Attempts to lock the `null_mutex`.
	  *
	  * Always succeeds.
	  *
	  * \returns
	  * `true`.
	  *
	  * \remark
	  * Prior `unlock` operations on the same object synchonize with
	  * this operation.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §30.2.5.3/2-4;
	  * §30.4.1.2/14-20
	  */
	bool try_lock() noexcept
	{
		lock();
		return true;
	}
};
}
}

#endif // VLINDE_THREAD_NULL_MUTEX_HPP
