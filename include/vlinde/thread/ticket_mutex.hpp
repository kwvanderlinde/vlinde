#ifndef VLINDE_THREAD_TICKET_MUTEX_HPP
#define VLINDE_THREAD_TICKET_MUTEX_HPP

#include <atomic>
#include <cstdint>

namespace vlinde
{
namespace thread
{
/** A fair analogue to the `spin_mutex`.
  *
  * A `ticket_mutex` is may be locked by only one execution agent at a
  * time. `ticket_mutex` is not a recursive mutex.
  *
  * A `ticket_mutex` uses a simple method to guarantee fairness. Each
  * agent which calls `lock` is assigned a unique number called a
  * *ticket*. The agent then busy waits for its ticket to become
  * current. When an agent completes, it increments the current ticket,
  * allowing the next agent to progress.
  *
  * \note A ticket is implemented as an unsigned integer type. Since
  * this places a bound on the number of available tickets, and thus the
  * number of agents blocked on a `ticket_mutex`, this means that the
  * algorithm is theoretically flawed. However, in practice, no issue
  * should arise since even a 32-bit ticket type allows for almost 4.3
  * billion tickets. Needless to say, that many agents should not be
  * banging on one `mutex`!
  */
class ticket_mutex
{
	/** The next ticket to be acquired.
	  */
	::std::atomic<::std::uintmax_t> m_enqueue_ticket;

	/** The current ticket.
	 */
	::std::atomic<::std::uintmax_t> m_dequeue_ticket;

public:
	/** Default constructor.
	  *
	  * The `ticket_mutex` is initially not locked.
	  *
	  * \cpp11ref
	  30.4.1.2/3
	  */
	ticket_mutex() : m_enqueue_ticket{0u}, m_dequeue_ticket{0u}
	{
	}

	/** Copy constructor.
	  *
	  * `ticket_mutex` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	ticket_mutex(ticket_mutex const&) = delete;

	/** Move constructor.
	  *
	  * `ticket_mutex` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	ticket_mutex(ticket_mutex&&) = delete;

	/** Copy assignment operator.
	  *
	  * `ticket_mutex` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	ticket_mutex& operator=(ticket_mutex const&) = delete;

	/** Move assignment operator.
	  *
	  * `ticket_mutex` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	ticket_mutex& operator=(ticket_mutex&&) = delete;

	/** Destructor.
	  *
	  * \pre
	  * The `ticket_mutex` is not locked by any thread.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	~ticket_mutex() = default;

	/** Obtains a lock of the `ticket_mutex` for the current agent.
	  *
	  * The call to `lock` will block the current agent until ownership
	  * of a lock on the `ticket_mutex` is acquired. If the current
	  * agent already owns a lock on the `ticket_mutex`, then deadlock
	  * has occurred.
	  *
	  * \remark
	  * Prior calls to `unlock` synchronize with this call to `lock`.
	  *
	  * \cpp11ref
	  * §30.2.5.2/2;
	  * §30.4.1.2/6-13
	  */
	void lock() noexcept
	{
		auto ticket = m_enqueue_ticket++;

		// Busy wait until our ticket comes up.
		while (ticket != m_dequeue_ticket)
			;
	}

	/** Releases the lock on the `ticket_mutex`.
	  *
	  * \pre
	  * The current execution agent shall hold a lock on the
      * `ticket_mutex`.
	  *
	  * \remark
	  * This operation synchronizes with subsequent `lock` operations
	  * that obtain ownership on the same object.
	  *
	  * \cpp11ref
	  * §30.2.5.2/3-4;
	  * §30.4.1.2/21-26
	  */
	void unlock() noexcept
	{
		// Signal that the next ticket is up.
		++m_dequeue_ticket;
	}

	/** Attempts to lock the `ticket_mutex`.
	  *
	  * If locking the `ticket_mutex` requires blocking, then does not
	  * lock the `ticket_mutex`.
	  *
	  * \returns
	  * `true` if the lock was acquired, `false` otherwise.
	  *
	  * \remark
	  * If the return value is `true`, then prior `unlock` operations on
	  * the same object synchonize with this operation.
	  *
	  * \cpp11ref
	  * §30.2.5.3/2-4;
	  * §30.4.1.2/14-20
	  */
	bool try_lock() noexcept
	{
		/* Obtain a torn snapshot of the state.
		 *
		 * Since `m_dequeue_ticket` "chases" `m_enqueue_ticket`, these
		 * are equal only if the snapshot is not torn (thanks to the
		 * particular order of the loads).
		 */
		auto dequeue_ticket = m_dequeue_ticket.load();
		auto enqueue_ticket = m_enqueue_ticket.load();

		/* Taking the lock can succeed only if the tickets are equals
		 * In this case, we know the snapshot is not torn (see above).
		 * Thus, we know that no thread owned the lock at the point of
		 * the `load` calls.
		 */
		if (dequeue_ticket != enqueue_ticket)
			return false;

		/* The lock can only succeed if the current state of the
		 * `ticket_mutex` is the same as we observed before. Note that
		 * `m_dequeue_ticket` can only have changed since the `load`s if
		 * `m_enqueue_ticket` has also changed, since we observed them
		 * to be equal, and `m_dequeue_ticket` is bounded above by
		 * `m_enqueue_ticket`. Thus, a state change has occurred if and
		 * only if `m_enqueue_ticket` is not the same as our local
		 * `enqueue_ticket`.
		 */

		/* Attempt to take the lock by grabbing a ticket, only if things
         * haven't changed since the snapshot.
		 *
		 * The spurious failure afforded to`compare_exchange_weak` is
		 * allowable behaviour for `try_lock`.
		 */
		return m_enqueue_ticket.compare_exchange_weak(
		    enqueue_ticket, enqueue_ticket + 1);
	}
};
}
}

#endif // VLINDE_THREAD_TICKET_MUTEX_HPP
