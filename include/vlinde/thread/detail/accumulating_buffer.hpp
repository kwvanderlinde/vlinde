#ifndef VLINDE_THREAD_DETAIL_ACCUMULATING_BUFFER_HPP
#define VLINDE_THREAD_DETAIL_ACCUMULATING_BUFFER_HPP

#include "slist_node.hpp"
#include "slist_node_iterator.hpp"

#include <atomic>
#include <cassert>
#include <cstdint>
#include <iterator>
#include <memory>
#include <utility>

namespace vlinde
{
namespace thread
{
namespace detail
{
/** A simple buffer with insertion as the only modifying operation.
  *
  * An `accumulating_buffer<T>` is a buffer which supports two basic
  * operations: `push`ing `T` values into the buffer; and iterating over
  * previously inserted values. `push` is a lock-free operation, while
  * the `begin` and `end` methods require no synchronization.
  *
  * `accumulating_buffer<T>` provides an alternative to a lock-free
  * queue, which seems impossible without a CAS2 operation. The tradeoff
  * is that the inserted values are kept alive until the buffer is
  * destroyed.
  *
  * \tparam T
  * The type of value stored in the buffer.
  */
template <typename T>
class accumulating_buffer
{
public:
	/** A mutable ForwardIterator over the values in the buffer.
	  */
	using iterator = slist_node_iterator<T, false>;

	/** A constant ForwardIterator over the values in the buffer.
	  */
	using const_iterator = slist_node_iterator<T, true>;

private:
	/** The dummy node at the end of the list.
	  *
	  * This pointer must never change since it identifies the end of
	  * the list.
	  */
	slist_node<T>* const m_head;

	/** Always points to the newest node in the list.
	  *
	  * A node is not considered part of the list unless an iterator
	  * pointing to the node is reachable from an iterator pointing to
	  * `m_tail`. Thus, a node is not in the list until `m_tail` is
	  * updated to point to it at some time.
	  */
	::std::atomic<slist_node<T>*> m_tail;

	/** Allocates and constructs a `slist_node<T>` object.
	  *
	  * \param value
	  * The `T` value which the returned node should store.
	  *
	  * \return
	  * A fresh node with the given value.
	  */
	static slist_node<T>* s_make_node(T const& value = {})
	{
		return new slist_node<T>{value, nullptr};
	}

	/** Adds a new node to the list to the buffer.
	  *
	  * \param new_node
	  * A pointer to the node to insert.
	  *
	  * \pre
	  * `new_node` is not a node in any list of an
	  * `accumulating_buffer`.
	  *
	  * \post
	  * `new_node` is in the list.
	  *
	  * \note
	  * `push` has no ABA problem since no node recycling occurs.
	  *
	  * \exsafety
	  * No throw.
	  */
	void m_push_node(slist_node<T>* new_node) noexcept
	{
		// Observe the current tail.
		slist_node<T>* tail = m_tail.load();

		// Repeat until `new_node` was successfully inserted.
		do {
			// Link the node to the observed tail..
			new_node->next = tail;

			// Attempt to set `new_node` as the new tail.
			// Observes `tail` again in case of failure.
		} while (!m_tail.compare_exchange_strong(tail, new_node));
	}

public:
	/** Default constructor.
	  *
	  * Constructs an empty `accumulating_buffer`.
	  *
	  * \post
	  * `this->begin() == this->end()`.
	  */
	accumulating_buffer() : m_head{s_make_node()}, m_tail{m_head}
	{
	}

	/** Destructor.
	  *
	  * Dismantles the internal buffer and destroys all values stored
	  * in the buffer.
	  *
	  * \remark
	  * If an operation on an `accumulating_buffer` executes
	  * concurrently to its destructor, then behaviour is undefined.
	  *
	  * \exsafety
	  * No throw.
	  */
	~accumulating_buffer() noexcept
	{
		slist_node<T>* current = m_tail.load();
		while (current != nullptr) {
			auto node = current;
			current = current->next;
			delete node;
		}
	}

	/** Adds a value to the buffer.
	  *
	  * \param
	  * value The value to add to the buffer.
	  *
	  * \exsafety
	  * Strong exception safety.
	  */
	void push(T const& value)
	{
		auto new_node = new slist_node<T>{value, nullptr};
		// No more exceptions thrown beyond this point.
		m_push_node(new_node);
	}

	/** An `iterator` pointing to the most recently inserted value.
	  *
	  * \return
	  * An `iterator` to the most recently inserted value, or
	  * `this->end()` if the buffer is empty.
	  *
	  * \exsafety
	  * No throw.
	  */
	iterator begin() noexcept
	{
		return iterator{m_tail.load()};
	}

	/** An `iterator` pointing past the end of the buffer.
	  *
	  * \return
	  * An `iterator` pointing past the end of the buffer.
	  *
	  * \exsafety
	  * No throw.
	  */
	iterator end() noexcept
	{
		return iterator{m_head};
	}

	/** A `const_iterator` pointing to the most recently inserted value.
	  *
	  * \return
	  * A `const_iterator` to the most recently inserted value, or
	  * `this->end()` if the buffer is empty.
	  *
	  * \exsafety
	  * No throw.
	  */
	const_iterator begin() const noexcept
	{
		return this->cbegin();
	}

	/** A `const_iterator` pointing past the end of the buffer.
	  *
	  * \return
	  * A `const_iterator` pointing past the end of the buffer.
	  *
	  * \exsafety
	  * No throw.
	  */
	const_iterator end() const noexcept
	{
		return this->cend();
	}

	/** A `const_iterator` pointing to the most recently inserted value.
	  *
	  * `a.cbegin()` is equivalent to
	  * `const_cast<accumulating_buffer<T> const &>(a).begin()`.
	  *
	  * \return
	  * A `const_iterator` to the most recently inserted value, or
	  * `this->end()` if the buffer is empty.
	  *
	  * \exsafety
	  * No throw.
	  */
	const_iterator cbegin() const noexcept
	{
		return const_iterator{m_tail.load()};
	}

	/** A `const_iterator` pointing to the end of the buffer.
	  *
	  * `a.cend()` is equivalent to
	  * `const_cast<accumulating_buffer<T> const &>(a).end()`.
	  *
	  * \return
	  * A `const_iterator` pointing past the end of the buffer.
	  *
	  * \exsafety
	  * No throw.
	  */
	const_iterator cend() const noexcept
	{
		return const_iterator{m_head};
	}
};
}
}
}
#endif // VLINDE_THREAD_DETAIL_ACCUMULATING_BUFFER_HPP
