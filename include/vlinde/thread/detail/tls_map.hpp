#ifndef VLINDE_THREAD_DETAIL_TLS_MAP_HPP
#define VLINDE_THREAD_DETAIL_TLS_MAP_HPP

#include "tls_control_block.hpp"

#include <vlinde/utility/scope_guard.hpp>

#include <memory>
#include <unordered_map>
#include <utility>

namespace vlinde
{
namespace thread
{
template <typename T>
class thread_local_storage;

namespace detail
{
/** A synchronous map from `thread_local_storage<T> *` to
  * `tls_control_block<T> *`.
  *
  * `tls_map` is the basic data structure on which
  * `thread_local_storage<T>` is implemented. Since it's instances are
  * intended to have thread local storage duration, the implementation
  * is not thread safe.
  *
  * \tparam T
  * The type to store in thread local storage.
  */
template <typename T>
class tls_map
{
public:
	/** The type of key for the domain of `tls_map`.
	  */
	using key_type = thread_local_storage<T>*;

	/** The type of value for the codomain of `tls_map`.
	  */
	using mapped_type = tls_control_block<T>*;

private:
	/** Relinquished ownership of a control block.
	  *
	  * If the block was already released by the
	  * `thread_local_storage<T>`, then this function will destroy and
	  * deallocate the block.
	  *
	  * \param block
	  * A pointer to the control block to release.
	  */
	static void s_release_block(tls_control_block<T>* block) noexcept
	{
		// Relinquish ownership of the control block.
		if (block->is_dead.test_and_set()) {
			/* The `thread_local_storage` no longer owns the block, so
			 * we have to clean it up.
			 *
			 * Note that `thread_local_storage<T>` always cleans up the
			 * value before releasing ownership, so we must only destroy
			 * the block itself.
			 */
			delete block;
		}
	}

	/** The mapping between `thread_local_storage<T>` instances and
	  * `tls_control_block<T>` objects.
	  */
	::std::unordered_map<key_type, mapped_type> m_map;

public:
	/** Default constructor.
	  *
	  * Initializes a `tls_map` with no entries.
	  */
	tls_map() = default;

	tls_map(tls_map const&) = delete;

	tls_map(tls_map&&) = delete;

	tls_map& operator=(tls_map const&) = delete;

	tls_map& operator=(tls_map&&) = delete;

	/** Destructor.
	  *
	  * Deallocates any `tls_control_block<T>` which are owned uniquely
	  * by `*this`.
	  *
	  * \exsafety
	  * No throw.
	  */
	~tls_map() noexcept
	{
		for (auto& entry : m_map) {
			s_release_block(entry.second);
		}
	}

	/** Lookup a key in the map.
	  *
	  * If the key is not already in the map, it is automatically
	  * inserted. This value of the inserted entry will be a pointer to
	  * a default constructed `tls_control_block<T>`.
	  *
	  * \param key
	  * The thread id to lookup.
	  *
	  * \return
	  * A pair consisting of the control block corresponding to `key`
	  * and a boolean which indicates whether a new entry had to be
	  * inserted for `key`.
	  *
	  * \exsafety
	  * Strong exception safety.
	  */
	::std::pair<mapped_type, bool> get(key_type const& key)
	{
		auto result = m_map.insert(::std::make_pair(key, nullptr));

		// Indicates whether `key` existed previously in the domain.
		bool inserted{false};

		if (result.second) {
			// The pair was actually inserted.

			// Rollback in case the next `new` operation throws.
			auto guard = ::vlinde::utility::scope_guard([&]() {
				m_map.erase(result.first);
			});

			// Replace the `nullptr` placeholder with an actual block.
			result.first->second = new tls_control_block<T>{};
			inserted = true;

			// No exception occured, so no need to rollback.
			guard.dismiss();
		} else {
			/* It is possible that a key was reused if one
			 * `thread_local_storage<T>` was destroyed and another one
			 * took its place. In this case, we still need to do an auto
			 * insert, though the map entry already exists.
			 */
			auto entry = result.first->second;

			/* If the block is marked as dead, then `*key` is a new
			 * `thread_local_storage<T>` object, but the memory location
			 * has been reused.
			 *
			 * Note that it's okay to speculatively set the bit, since
			 * we'll clear it anyways (no matter what, the node will be
			 * alive by the time this function exits). The node won't be
			 * cleaned up in the interim since the
			 * `thread_local_storage<T>` which owns it (if it still
			 * exists) is the one calling this function in use, so it's
			 * destructor is not allowed to be called.
			 */
			if (entry->is_dead.test_and_set()) {
				/* The flag was already set, so the control block is
				 * currently dead. Resurrect it and stuff a value back
				 * into it.
				 */
				new (::std::addressof(entry->value)) T{};
				inserted = true;
			}

			/* This resurrects the control block, or just reverts the
			 * bit if the block was never dead in the first place.
			 */
			entry->is_dead.clear();
		}

		/* `result.first` is an iterator to the desired control block.
		 */
		return ::std::make_pair(result.first->second, inserted);
	}

	/** Removes an entry from the map.
	  *
	  * \param key
	  * The key of the entry to remove.
	  *
	  * \exsafety
	  * No except.
	  */
	void erase(key_type const& key) noexcept
	{
		// First, find the entry
		auto it = m_map.find(key);

		// Get the control block.
		auto block = it->second;

		// Remove the entry.
		m_map.erase(key);

        // Clean up the block.
		s_release_block(block);
	}
};
}
}
}

#endif // VLINDE_THREAD_DETAIL_TLS_MAP_HPP
