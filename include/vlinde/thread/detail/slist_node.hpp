#ifndef VLINDE_THREAD_DETAIL_SLIST_NODE_HPP
#define VLINDE_THREAD_DETAIL_SLIST_NODE_HPP

namespace vlinde
{
namespace thread
{
namespace detail
{
/** A singly linked list node.
  *
  * \remarks
  * `slist_node` is not thread safe on its own. Concurrent operations
  * should not be performed on an `slist_node` if at least one of the
  * operations is modifying.
  *
  * \tparam T
  * The type of value stored in an `slist_node<T>`.
  */
template <typename T>
struct slist_node
{
	/** \brief The value stored in the node.
	  */
	T payload;

	/** \brief The next node in the list.
	  */
	slist_node* next;
};
}
}
}

#endif // VLINDE_THREAD_DETAIL_SLIST_NODE_HPP
