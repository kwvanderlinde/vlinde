#ifndef VLINDE_THREAD_DETAIL_SLIST_NODE_ITERATOR_HPP
#define VLINDE_THREAD_DETAIL_SLIST_NODE_ITERATOR_HPP

#include "slist_node.hpp"

#include <boost/iterator/iterator_facade.hpp>

#include <cassert>
#include <iterator>
#include <type_traits>
#include <utility>

namespace vlinde
{
namespace thread
{
namespace detail
{
template <typename T, bool IsConstant>
class slist_node_iterator;

/** A convenience alias for CRTP applied to `slist_node_iterator`.
  */
template <typename T, bool IsConstant>
using slist_node_iterator_base = ::boost::iterator_facade<
    slist_node_iterator<T, IsConstant>,
    typename ::std::conditional<
        IsConstant, typename ::std::add_const<T>::type, T>::type,
    ::std::forward_iterator_tag>;

/** A ForwardIterator over the values in a singly linked list.
  *
  * The core functionality of `slist_node_iterator` is implemented in
  * `slist_node_iterator` itself. `::%boost::iterator_facade` uses this
  * functionality to provide the `ForwardIterator` interface.
  *
  * `slist_node_iterator<T, IsConstant>` iterates over `slist_node<T>`
  * nodes. Whether the iterator is constant or mutable depends on the
  * `IsConstant` parameter. Note that if `T` is `const`, then the
  * iterator will be constant regardless of `IsConstant`.
  *
  * \tparam T
  * Indicates that `slist_node_iterator<T, IsConstant>` iterates over
  * `slist_node<T>` nodes.
  *
  * \tparam IsConstant
  * If `true`, then `slist_node_iterator<T, IsConstant>` is a constant
  * iterator. Otherwise, `slist_node_iterator<T, IsConstant>` is
  * constant if and only if `T` is `const`.
  */
template <typename T, bool IsConstant>
class slist_node_iterator
    : public slist_node_iterator_base<T, IsConstant>
{
	/** Indicates whether the iterator is constant.
	  *
	  * An `slist_node_iterator<T, IsConstant>` is a constant iterator
	  * if `IsConstant` is true or `T` is `const`.
	  */
	static constexpr bool s_is_constant =
	    ::std::is_const<typename ::std::remove_reference<
	        typename slist_node_iterator_base<
	            T, IsConstant>::reference>::type>::value;

	/** The type which `slist_node_iterator<T, IsConstant>` points to.
	  *
	  * This type will be `slist_node<T>`, possibly `const`-qualified,
	  * depending on whether this is a constant iterator type.
	  */
	using node_type = typename ::std::conditional<
	    s_is_constant, slist_node<T> const, slist_node<T>>::type;

	/** The node which `*this` current points to.
	  */
	node_type* m_current;

public:
	/** Constructs an `slist_node_iterator` which points to the given
	  * node.
	  *
	  * \param initial
	  * The `slist_node` which the new `slist_node_iterator` will point
	  * to.
	  *
	  * \exsafety
	  * No throw.
	  */
	explicit slist_node_iterator(node_type* initial) noexcept
	    : m_current{initial}
	{
	}

	/** Constructs a singular `slist_node_iterator`.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.5/1 (forward.iterators).
	  */
	slist_node_iterator() noexcept : slist_node_iterator{nullptr}
	{
	}

	/** Copy constructor.
	  *
	  * The copy constructor respects equality. That is, if `a` is
	  * an `slist_node_iterator`, then
	  * `slist_node_iterator{a} == a`.
	  *
	  * \param other
	  * An `slist_node_iterator` to copy construct `*this` from.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.5/2 (iterator.iterators).
	  */
	slist_node_iterator(slist_node_iterator const& other) noexcept =
	    default;

	/** Destructor.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.2/2 (iterator.iterators).
	  */
	~slist_node_iterator() noexcept = default;

	/** Copy assignment operator.
	  *
	  * Like the copy constructor, the copy assignment operator respects
	  * equality.
	  *
	  * \param other
	  * An `slist_node_iterator` to assign to `*this`.
	  *
	  * \return
	  * `*this`.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.2/2 (iterator.iterators).
	  */
	slist_node_iterator&
	operator=(slist_node_iterator const& other) noexcept = default;

	/** Swaps two `slist_node_iterator` objects.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.2/2 (iterator.iterators).
	  */
	friend void swap(slist_node_iterator& first,
	                 slist_node_iterator& second) noexcept
	{
		using ::std::swap;
		swap(first.m_current, second.m_current);
	}

	/** Accesses the `value_type` to which `*this` is pointing.
	  *
	  * \pre
	  * `*this` is dereferencable.
	  *
	  * \post
	  * `this->dereference()` yields the same result.
	  *
	  * \return
	  * The `value_type` of the current position of `*this`.
	  *
	  * \remark
	  * Dereference respects equality, so that if `a == b`, then
	  * `*a` is equivalent to `*b`.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.2/2 (iterator.iterators);
	  * §24.2.3/1 (input.iterators);
	  * §24.2.5/1 (forward.iterators).
	  */
	typename slist_node_iterator_base<T, IsConstant>::reference
	dereference() const noexcept
	{
		assert(m_current);

		return m_current->payload;
	}

	/** Compares `*this` and `other` for equality.
	  *
	  * Two `slist_node_iterator`s are equal if and only if they refer
	  * to the same node.
	  *
	  * \return
	  * `true` if `*this` is equal to `other`. Otherwise, `false`.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.3/1 (input.iterators).
	  */
	bool equal(slist_node_iterator const& other) const noexcept
	{
		return m_current == other.m_current;
	}

	/** Increments `*this` to point to the next node.
	  *
	  * Increment respects equality. That is, if `a == b`, where `a` and
	  * `b` are (references to) two *different* `slist_node_iterator`
	  * instances, and then both `a` and `b` are incremented, it is
	  * still the case that `a == b`.
	  *
	  * \pre
	  * `*this` is dereferencable.
	  *
	  * \post
	  * `*this` is dereferencable or past-the-end.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §24.2.2/2 (iterator.iterators);
	  * §24.2.3/1 (input.iterators);
	  * §24.2.5/1 (forward.iterators).
	  */
	void increment() noexcept
	{
		assert(m_current && m_current->next);
		m_current = m_current->next;
	}
};
}
}
}

#endif // VLINDE_THREAD_DETAIL_SLIST_NODE_ITERATOR_HPP
