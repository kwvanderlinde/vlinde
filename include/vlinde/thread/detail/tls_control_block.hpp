#ifndef VLINDE_THREAD_DETAIL_TLS_CONTROL_BLOCK_HPP
#define VLINDE_THREAD_DETAIL_TLS_CONTROL_BLOCK_HPP

#include <atomic>

namespace vlinde
{
namespace thread
{
namespace detail
{
/** A primitive for communication between the global and local portion
  * of the thread local storage implementation.
  *
  * A `tls_control_block<T>` allows a `tls_map<T>` and a
  * `thread_local_storage<T>` to communicate about managing the entries
  * stored in the `tls_map<T>`. Specifically, it allows a
  * `thread_local_storage<T>` complete access to all of its per-thread
  * values, while not intruding on the synchronous operation of the
  * `tls_map<T>`. Additionally, it provides a flag which allows either
  * party to recognize the other as destroyed, which is useful for
  * removing dead entries in the `tls_map<T>`, as well as for managing
  * the `tls_control_block<T>` resource itself.
  *
  * \tparam T
  * The type of value stored in the control block.
  */
template <typename T>
struct tls_control_block
{
	union
	{
		/** The value refered to by this control block.
		  *
		  * Note that `value` must be destroyed only by
		  * `thread_local_storage<T>`. Thus, the destructor of `value`
		  * will not be implicitly called by `~tls_control_block()`.
		  * Instead, `value` must be destroyed explicitly by
		  * `thread_local_storage<T>`.
		  *
		  * \remark
		  * To ensure that a `tls_control_block<T>` is not destroyed
		  * before its `value`, `thread_local_storage<T>` must destroy
		  * `value` before releasing ownership.
		  */
		T value{};
	};

	/** Communicates whether one of the parties has relinquished
	  * ownership of the control block.
	  *
	  * If set, then either the `tls_map<T>` or the
	  * `thread_local_storage<T>` no longer owns the control block. The
	  * party which achieves sole ownership of the control block is
	  * responsible for destroying is and deallocating its memory.
	  */
	::std::atomic_flag is_dead;

	/** Default constructor.
	  *
	  * This is the only means of creating a `tls_control_block`. This
	  * ensures that each pair of a `tls_map<T>` and a
	  * `thread_local_storage<T>` corresponds to precisely one
	  * `tls_control_block<T>`, with no accidental copying.
	  */
	tls_control_block() = default;

	tls_control_block(tls_control_block const&) = delete;

	tls_control_block(tls_control_block&&) = delete;

	/** Destructor.
	  *
	  * Explicitly defining the destructor is required in case `T` does
	  * not have a trivial destructor, even though we don't even want
	  * want to call `T`'s destructor.
	  */
	~tls_control_block() noexcept
	{
	}

	tls_control_block& operator=(tls_control_block const&) = delete;

	tls_control_block& operator=(tls_control_block&&) = delete;
};
}
}
}

#endif // VLINDE_THREAD_DETAIL_TLS_CONTROL_BLOCK_HPP
