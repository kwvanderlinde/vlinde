#ifndef VLINDE_THREAD_DETAIL_HPP
#define VLINDE_THREAD_DETAIL_HPP

namespace vlinde
{
namespace thread
{
/** \internal
 *
  * \brief
  * Components for implementing `::vlinde::thread` facilities.
  */
namespace detail
{
}
}
}

#endif // VLINDE_THREAD_DETAIL_HPP
