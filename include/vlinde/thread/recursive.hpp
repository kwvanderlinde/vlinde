#ifndef VLINDE_THREAD_RECURSIVE_HPP
#define VLINDE_THREAD_RECURSIVE_HPP

#include <vlinde/thread/thread_local_storage.hpp>

#include <atomic>
#include <cassert>
#include <cstdint>
#include <system_error>
#include <thread>

namespace vlinde
{
namespace thread
{
/** A recursive analogue to `Mutex`.
  *
  * A recursive mutex is a mutex allows a thread to lock the same mutex
  * several times without unlocking. A thread releases the mutex by
  * bunlocking the mutex the same number of times that the thread locked
  * the mutex.
  *
  * `recursive<Mutex>` has the same ownership semantics as `Mutex`. That
  * is, if a `Mutex` has exlusive ownership semantics, then so does
  * `recursive<Mutex>`. If `Mutex` allows `N` threads to lock a `Mutex`
  * at once, then so does `recursive<Mutex>`. If any number of threads
  * may lock a `Mutex`, then any number of threads may lock
  * `recursive<Mutex>`. If the ownership semantics of `Mutex` change
  * over time, than `recursive<Mutex>` changes it ownership semantics in
  * kind.
  *
  * If `Mutex` already gives recursive semantics, then
  * `recursive<Mutex>` is essentially a pessimization, since it adds
  * additional overhead without providing extra functionality. The
  * recursive functionality of the `Mutex` is simply not used in the
  * `recursive<Mutex>`.
  *
  * \remarks
  * There is an implementation imposed limit on the number of times a
  * `recursive<Mutex>` may be locked without being unlocked. This is
  * exposed in the `max_recursion` constant. In practice, this limit
  * should be rather high, so that the event of a thread exceeding the
  * maximum recursion limit is quite unlikely.
  *
  * \tparam Mutex
  * A `Mutex` implementation.
  *
  * \cpp11ref
  * 30.4.1 (thread.mutex.requirements)
  */
template <typename Mutex>
class recursive
{
	Mutex m_mutex;
	::vlinde::thread::thread_local_storage<::std::uintmax_t>
	m_lock_count;

public:
	/** The maximum ownership level a thread may obtain.
	  */
	static constexpr ::std::uintmax_t max_recursion =
	    ::std::numeric_limits<::std::uintmax_t>::max() - 1;

	/** Default constructor.
	  *
	  * \cpp11ref
	  * 30.4.1.2/3
	  */
	recursive() : m_mutex{}, m_lock_count{}
	{
	}

	/** Copy constructor.
	  *
	  * `recursive<Mutex>` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	recursive(recursive const&) = delete;

	/** Move constructor.
	  *
	  * `recursive<Mutex>` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	recursive(recursive&&) = delete;

	/** Destructor.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	~recursive() = default;

	/** Copy assignment operator.
	  *
	  * `recursive<Mutex>` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	recursive& operator=(recursive const&) = delete;

	/** Move assignment operator.
	  *
	  * `recursive<Mutex>` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	recursive& operator=(recursive&&) = delete;

	/** Obtains a lock of the `recursive<Mutex>`.
	  *
	  * The call to `lock` will block the current thread until ownership
	  * of a lock on the `recursive<Mutex>` is acquired. If the thread
	  * already owns a lock on the `recursive<Mutex>`, and the maximum
	  * ownership level has not yet been reached, then the lock is
	  * acquired once more. On the other hand, if the maximum ownership
	  * level has been reached, then `system_error` is thrown with error
	  * condition `device_or_resource_busy`.
	  *
	  * \remarks
	  * Prior calls to `unlock` synchronize with this call to `lock`.
	  *
	  * \throws system_error
	  * If the lock is taken recursively too many times.
	  *
	  * \cpp11ref
	  * §30.2.5.2/2;
	  * §30.4.1.2/6-13
	  */
	void lock() noexcept
	{
		// Get the lock count for this thread.
		auto& lock_count = m_lock_count.get();

		switch (lock_count) {
		default:
			// We are allowd to recursively lock the mutex.
			++lock_count;
			return;

		case 0:
			// The thread does not yet own the mutex. Take ownership.
			m_mutex.lock();
			// Update the lock count.
			++lock_count;
			return;

		case max_recursion:
			// The thread has taken too many recursive locks.
			throw ::std::system_error{ ::std::make_error_code(
			    ::std::errc::device_or_resource_busy)};
		}
	}

	/** Releases the lock on the `recursive<Mutex>`.
	  *
	  * `unlock` must be called as many times as `lock` before ownership
	  * is actually released.
	  *
	  * \pre
	  * The current execution agent shall hold a lock on the
	  * `recursive<Mutex>`.
	  *
	  * \remark
	  * This operation synchronizes with subsequent `lock` operations
	  * that obtain ownership on the same object.
	  *
	  * \exsafety
	  * No throw.
	  *
	  * \cpp11ref
	  * §30.2.5.2/3-4;
	  * §30.4.1.2/21-26
	  */
	void unlock() noexcept
	{
		auto& lock_count = m_lock_count.get();
		assert(lock_count > 0);

		--lock_count;
		if (lock_count == 0) {
			// The thread released its last lock, so release the
			// underlying mutex.
			m_mutex.unlock();
		}
	}

	/** Attempts to lock the `recursive<Mutex>`.
	  *
	  * If the thread already owns a lock on the `recursive<Mutex>`, and
	  * the maximum ownership level has not yet been reached, then the
	  * lock is acquired once more. Otherwise, the lock is not acquired.
	  *
	  * \returns
	  * `true` if the lock was acquired, `false` otherwise.
	  *
	  * \remark
	  * If the return value is `true`, then prior `unlock` operations on
	  * the same object synchonize with this operation.
	  *
	  * \exsafety
	  * Strong exception safety.
	  *
	  * \cpp11ref
	  * §30.2.5.3/2-4;
	  * §30.4.1.2/14-20
	  */
	bool try_lock()
	{
		/* This operation may throw, but it doesn't change anything so
		 * strong exception safety is not jeopardized if an exception is
		 * thrown later on.
		 */
		auto& lock_count = m_lock_count.get();

		switch (lock_count) {
		default:
			// We are allowed another lock.
			++lock_count;
			return true;

		case 0:
			/* The current thread does not yet own the mutex. Try to
			 * take ownership. If this throws, the lock is not acquired.
			 */
			if (m_mutex.try_lock()) {
				// Lock successful.
				++lock_count;
				return true;
			}

			// We failed to get the lock.
			return false;

		case max_recursion:
			// We've taken locks recursively too many times.
			return false;
		}
	}
};
}
}

#endif // VLINDE_THREAD_RECURSIVE_HPP
