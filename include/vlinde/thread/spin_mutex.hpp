#ifndef VLINDE_THREAD_SPIN_MUTEX_HPP
#define VLINDE_THREAD_SPIN_MUTEX_HPP

#include <atomic>

namespace vlinde
{
namespace thread
{
/** A spin-lock implementation adhering to the `Mutex` concept.
  *
  * A `spin_mutex` is a regular mutex in that each instance may be
  * locked by only one execution agent at a time. `spin_mutex` is not a
  * recursive mutex.
  */
class spin_mutex
{
	/** The flag to control the spin lock.
	  */
	std::atomic_flag m_locked;

public:
	/** Default constructor.
	  *
	  * The `spin_mutex` is initially not locked.
	  *
	  * \cpp11ref
	  * 30.4.1.2/3
	  */
	spin_mutex() : m_locked{}
	{
		m_locked.clear();
	}

	/** Copy constructor.
	  *
	  * `spin_mutex` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	spin_mutex(spin_mutex const&) = delete;

	/** Move constructor.
	  *
	  * `spin_mutex` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	spin_mutex(spin_mutex&&) = delete;

	/** Copy assignment operator.
	  *
	  * `spin_mutex` is not copyable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	spin_mutex& operator=(spin_mutex const&) = delete;

	/** Move assignment operator.
	  *
	  * `spin_mutex` is not movable.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	spin_mutex& operator=(spin_mutex&&) = delete;

	/** Destructor.
	  *
	  * \pre
	  * The `spin_mutex` is not locked by any thread.
	  *
	  * \cpp11ref
	  * §30.4.1.2/3
	  */
	~spin_mutex() = default;

	/** Obtains a lock of the `spin_mutex` for the current agent.
	  *
	  * The call to `lock` will block the current agent until ownership
	  * of a lock on the `spin_mutex` is acquired. If the current agent
	  * already owns a lock on the `spin_mutex`, then deadlock has
	  * occurred.
	  *
	  * \remark
	  * Prior calls to `unlock` synchronize with this call to `lock`.
	  *
	  * \cpp11ref
	  * §30.2.5.2/2;
	  * §30.4.1.2/6-13
	  */
	void lock() noexcept
	{
		while (m_locked.test_and_set(std::memory_order_acquire))
			;
	}

	/** Releases the lock on the `spin_mutex`.
	  *
	  * \pre
	  * The current execution agent shall hold a lock on the
	  * `spin_mutex`.
	  *
	  * \remark
	  * This operation synchronizes with subsequent `lock` operations
	  * that obtain ownership on the same object.
	  *
	  * \cpp11ref
	  * §30.2.5.2/3-4;
	  * §30.4.1.2/21-26
	  */
	void unlock() noexcept
	{
		m_locked.clear(std::memory_order_release);
	}

	/** Attempts to lock the `spin_mutex`.
	  *
	  * If locking the `spin_mutex` requires blocking, then does not
	  * lock the `spin_mutex`.
	  *
	  * \returns
	  * `true` if the lock was acquired, `false` otherwise.
	  *
	  * \remark
	  * If the return value is `true`, then prior `unlock` operations on
	  * the same object synchonize with this operation.
	  *
	  * \cpp11ref
	  * §30.2.5.3/2-4;
	  * §30.4.1.2/14-20
	  */
	bool try_lock() noexcept
	{
		return !m_locked.test_and_set(std::memory_order_acquire);
	}
};
}
}

#endif // VLINDE_THREAD_SPIN_MUTEX_HPP
