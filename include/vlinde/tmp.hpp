#ifndef VLINDE_TMP_HPP
#define VLINDE_TMP_HPP

namespace vlinde
{
/** Utilities to facilitate template metaprogramming (TMP).
 */
namespace tmp
{
}
}

#include "tmp/logic.hpp"
#include "tmp/typelist.hpp"
#include "tmp/utility.hpp"

#endif // VLINDE_TMP_HPP
