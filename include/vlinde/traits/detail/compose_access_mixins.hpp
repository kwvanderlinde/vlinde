#ifndef VLINDE_TRAITS_DETAIL_COMPOSE_ACCESS_MIXINS_HPP
#define VLINDE_TRAITS_DETAIL_COMPOSE_ACCESS_MIXINS_HPP

#include <type_traits>

namespace vlinde
{
namespace traits
{
namespace detail
{
// I need to avoid conflicts on the Range::reference type when composing
// the different kinds of access. Each mixin should surely define the
// type, but doing so directly in each mixin will cause ambiguities if
// two of the mixins are composed. Actually, we can define the types
// directly but we can't just compose them by inheriting each
// individually. Instead, I can define a compose_access_mixins mixin
// which will inherit from each provided mixin. Then I can define the
// reference type in compose_access_mixins to be the same as the
// reference type in each individual mixin. This can be statically
// asserted by applying ::std::is_same on the list of reference types
// from the base mixins.

template <typename Range>
auto has_reference_test(typename Range::reference*)
    -> ::std::true_type{};

template <typename Range>
auto has_reference_test(...) -> ::std::false_type{};

template <typename Range>
using has_reference = decltype(has_reference_test<Range>());

// Used to skip mixins without a reference type.
struct ignore
{
};

template <typename Mixin, typename T = void>
struct get_reference_base;

template <typename Mixin>
struct get_reference_base<Mixin, ::std::true_type>
{
	using type = typename Mixin::reference;
};

template <typename Mixin>
struct get_reference_base<Mixin, ::std::false_type>
{
	using type = ignore;
};

template <typename Mixin>
struct get_reference
    : public get_reference_base<Mixin, has_reference<Mixin>>
{
};

template <typename Reference1, typename Reference2>
struct compose
{
	static_assert(::std::is_same<Reference1, Reference2>::value,
	              "A range may not have different reference type for "
	              "its members.");
};

template <typename Reference>
struct compose<Reference, ignore>
{
	using type = Reference;
};

template <typename Reference>
struct compose<ignore, Reference>
{
	using type = Reference;
};

template <>
struct compose<ignore, ignore>
{
	using type = ignore;
};

template <typename... Mixins>
struct compose_reference_types
{
	using type = ignore;
};

template <typename Mixin, typename... Mixins>
struct compose_reference_types<Mixin, Mixins...>
{
	using lhs = typename get_reference<Mixin>::type;
	using rhs = typename compose_reference_types<Mixins...>::type;

	using type = typename compose<lhs, rhs>::type;
};

template <typename Reference, typename... Mixins>
struct compose_access_mixins_base : public Mixins...
{
	using reference = Reference;
};

template <typename... Mixins>
struct compose_access_mixins_base<ignore, Mixins...> : public Mixins...
{
	// No reference type.
};

template <typename... Mixins>
struct compose_access_mixins
    : public compose_access_mixins_base<
          typename compose_reference_types<Mixins...>::type, Mixins...>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_COMPOSE_ACCESS_MIXINS_HPP
