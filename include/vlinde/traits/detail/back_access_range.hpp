#ifndef VLINDE_TRAITS_DETAIL_BACK_ACCESS_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_BACK_ACCESS_RANGE_HPP

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range>
auto has_back_test(Range&& range)
    -> decltype(range.back(), ::std::true_type{});

template <typename Range>
auto has_back_test(Range const&) -> ::std::false_type;

template <typename Range>
using has_back = decltype(has_back_test(::std::declval<Range>()));

template <typename Range, typename T>
struct back_mixin_base
{
};

template <typename Range>
struct back_mixin_base<Range, ::std::true_type>
{
	using reference = decltype(::std::declval<Range const&>().back());

	static reference back(Range const& range)
	{
		return range.back();
	}
};

template <typename Range>
struct back_mixin : public back_mixin_base<Range, has_back<Range>>
{
};

template <typename Range>
struct back_access_range_mixin : public back_mixin<Range>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_BACK_ACCESS_RANGE_HPP
