#ifndef VLINDE_TRAITS_DETAIL_SINGLE_PASS_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_SINGLE_PASS_RANGE_HPP

#include "pop_front_concept.hpp"

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range>
struct single_pass_range_mixin : public pop_front_concept<Range>::mixin
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_SINGLE_PASS_RANGE_HPP
