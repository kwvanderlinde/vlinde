#ifndef VLINDE_TRAITS_DETAIL_FRONT_ACCESS_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_FRONT_ACCESS_RANGE_HPP

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range>
auto has_front_test(Range&& range)
    -> decltype(range.front(), ::std::true_type{});

template <typename Range>
auto has_front_test(Range const&) -> ::std::false_type;

template <typename Range>
using has_front = decltype(has_front_test(::std::declval<Range>()));

template <typename Range, typename T>
struct front_mixin_base
{
};

template <typename Range>
struct front_mixin_base<Range, ::std::true_type>
{
	using reference = decltype(::std::declval<Range const&>().front());

	static reference front(Range const& range)
	{
		return range.front();
	}
};

template <typename Range>
struct front_mixin : public front_mixin_base<Range, has_front<Range>>
{
};

template <typename Range>
struct front_access_range_mixin : public front_mixin<Range>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_FRONT_ACCESS_RANGE_HPP
