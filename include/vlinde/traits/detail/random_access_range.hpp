#ifndef VLINDE_TRAITS_DETAIL_RANDOM_ACCESS_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_RANDOM_ACCESS_RANGE_HPP

#include "front_access_range.hpp"

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range, typename Index>
auto has_index_test(Range&& range, Index&& index)
    -> decltype(range[index], ::std::true_type{});

template <typename Range, typename Index>
auto has_index_test(Range const&, Index&&) -> ::std::false_type;

// We use constant 0 here since zero should convert to any index type.
template <typename Range>
using has_index = decltype(has_index_test(::std::declval<Range>(), 0));

template <typename Range, typename HasFront, typename HasIndex>
struct index_mixin_base
{
};

// If the range has `front`, use it to determine the reference type.
template <typename Range>
struct index_mixin_base<
    Range, ::std::true_type,
    ::std::true_type> : public front_mixin_base<Range, ::std::true_type>
{
};

// If the range has no `front`, define it as `range[0]`.
template <typename Range>
struct index_mixin_base<Range, ::std::false_type, ::std::true_type>
{
	using reference = decltype(::std::declval<Range const&>()[0]);

	static reference front(Range const& range)
	{
		return range[0];
	}

	template <typename Index>
	static reference at(Range const& range, Index&& index)
	{
		return range[ ::std::forward<Index>(index)];
	}
};

template <typename Range>
struct index_mixin
    : public index_mixin_base<Range, has_front<Range>, has_index<Range>>
{
};

template <typename Range>
struct random_access_range_mixin : public index_mixin<Range>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_RANDOM_ACCESS_RANGE_HPP
