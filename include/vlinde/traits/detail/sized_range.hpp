#ifndef VLINDE_TRAITS_DETAIL_SIZED_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_SIZED_RANGE_HPP

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range>
decltype(::std::declval<Range>().size(), ::std::true_type{})
    has_size_test(Range&&);

template <typename Range>
::std::false_type has_size_test(Range const&);

template <typename Range>
using has_size = decltype(has_size_test(::std::declval<Range>()));

template <typename R, typename T>
struct sized_range_mixin_base
{
};

template <typename Range>
struct sized_range_mixin_base<Range, ::std::true_type>
{
	using size_type = decltype(::std::declval<Range const&>().size());

	static size_type size(Range const& range)
	{
		return range.size();
	}
};

template <typename Range>
struct sized_range_mixin
    : public sized_range_mixin_base<Range, has_size<Range>>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_SIZED_RANGE_HPP
