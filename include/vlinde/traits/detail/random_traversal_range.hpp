#ifndef VLINDE_TRAITS_DETAIL_RANDOM_TRAVERSAL_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_RANDOM_TRAVERSAL_RANGE_HPP

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range, typename Arg1, typename Arg2>
auto has_slice_test(Range&& range, Arg1&& arg1, Arg2&& arg2)
    -> decltype(range.slice(arg1, arg2), ::std::true_type{});

template <typename Range, typename Arg1, typename Arg2>
auto has_slice_test(Range const&, Arg1&&, Arg2&&) -> ::std::false_type;

template <typename Range, typename Arg1, typename Arg2>
using has_slice = decltype(has_slice_test(::std::declval<Range>(
    ::std::declval<Arg1>(), ::std::declval<Arg2>())));

template <typename Range, typename Arg1, typename Arg2>
auto slice_impl(::std::true_type, Range const& range, Arg1&& arg1,
                Arg2&& arg2) -> decltype(range.slice(arg1, arg2))
{
	return range.slice(arg1, arg2);
}

// No default implementation for slice.

template <typename Range>
struct slice_mixin
{
	template <typename Arg1, typename Arg2>
	static auto slice(Range const& range, Arg1&& arg1, Arg2&& arg2)
	    -> decltype(slice_impl(has_slice<Range, Arg1, Arg2>{}, range,
	                           arg1, arg2))
	{
		return slice_impl(has_slice<Range, Arg1, Arg2>{}, range, arg1,
		                  arg2);
	}
};

template <typename Range>
struct random_traversal_range_mixin : public pop_back_mixin<Range>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_RANDOM_TRAVERSAL_RANGE_HPP
