#ifndef VLINDE_TRAITS_DETAIL_POP_FRONT_CONCEPT_HPP
#define VLINDE_TRAITS_DETAIL_POP_FRONT_CONCEPT_HPP

#include <type_traits>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range>
auto has_pop_front_test(Range&& range)
    -> decltype(range.pop_front(), ::std::true_type{});

template <typename Range>
auto has_pop_front_test(Range const&) -> ::std::false_type;

template <typename Range>
using has_pop_front =
    decltype(has_pop_front_test(::std::declval<Range>()));

template <typename Range, typename T>
struct pop_front_concept_base;

template <typename Range>
struct pop_front_concept_base<
    Range, ::std::false_type> : public ::std::false_type
{
};

template <typename Range>
struct pop_front_concept_base<
    Range, ::std::true_type> : public ::std::true_type
{
	struct mixin
	{
		static void pop_front(Range& range)
		{
			range.pop_front();
		}
	};
};

template <typename Range>
struct pop_front_concept
    : public pop_front_concept_base<Range, has_pop_front<Range>>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_POP_FRONT_CONCEPT_HPP
