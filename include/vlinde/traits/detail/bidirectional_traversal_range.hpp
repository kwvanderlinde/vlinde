#ifndef VLINDE_TRAITS_DETAIL_BIDIRECTIONAL_TRAVERSAL_RANGE_HPP
#define VLINDE_TRAITS_DETAIL_BIDIRECTIONAL_TRAVERSAL_RANGE_HPP

#include <type_traits>
#include <utility>

namespace vlinde
{
namespace traits
{
namespace detail
{
template <typename Range>
auto has_pop_back_test(Range&& range)
    -> decltype(range.pop_back(), ::std::true_type{});

template <typename Range>
auto has_pop_back_test(Range const&) -> ::std::false_type;

template <typename Range>
using has_pop_back =
    decltype(has_pop_back_test(::std::declval<Range>()));

template <typename Range, typename T>
struct pop_back_mixin_base
{
};

template <typename Range>
struct pop_back_mixin_base<Range, ::std::true_type>
{
	static void pop_back(Range& range)
	{
		range.pop_back();
	}
};

template <typename Range>
struct pop_back_mixin
    : public pop_back_mixin_base<Range, has_pop_back<Range>>
{
};

template <typename Range>
struct bidirectional_traversal_range_mixin
    : public pop_back_mixin<Range>
{
};
}
}
}

#endif // VLINDE_TRAITS_DETAIL_BIDIRECTIONAL_TRAVERSAL_RANGE_HPP
