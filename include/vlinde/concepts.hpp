#ifndef VLINDE_CONCEPTS_HPP
#define VLINDE_CONCEPTS_HPP

#include <vlinde/tmp/typelist.hpp>

#include <cstddef>
#include <type_traits>

namespace vlinde
{
/** A modern concept checking library.
 *
 * It has become apparent that this is similar to the concept checking
 * in Eric Niebler's [range-v3][] library.
 *
 * \todo Document the Concept concept!
 *
 * [range-v3]: https://github.com/ericniebler/range-v3
 */
namespace concepts
{
}
}

#include "concepts/utility.hpp"
#include "concepts/basic.hpp"
#include "concepts/iterator.hpp"
#include "concepts/numeric.hpp"

#endif // VLINDE_CONCEPTS_HPP
