#ifndef VLINDE_UTILITY_COMPARE_HPP
#define VLINDE_UTILITY_COMPARE_HPP

#include <functional>

namespace vlinde
{
namespace utility
{
enum class order { less_than, equal_to, greater_than };

template <typename T, typename Compare = ::std::less<T>>
order compare(T const & a, T const & b,
              Compare const & comp = Compare{}) noexcept
{
	if (comp(a, b))
		return order::less_than;
	else if (comp(b, a))
		return order::greater_than;
	else
		return order::equal_to;
}
}
}

#endif // VLINDE_UTILITY_COMPARE_HPP
