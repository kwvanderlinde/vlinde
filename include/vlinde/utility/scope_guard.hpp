#ifndef SCOPEGUARD_HPP
#define SCOPEGUARD_HPP

#include "detail/scope_guard_impl.hpp"

#include <utility>

namespace vlinde
{
namespace utility
{
template <typename Func>
detail::scope_guard_impl<Func> scope_guard(Func func)
{
	return detail::scope_guard_impl<Func>(::std::move(func));
}
}
}

#include "detail/scope_exit.hpp"

#endif // SCOPEGUARD_HPP
