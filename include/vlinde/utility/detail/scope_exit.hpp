#ifndef VLINDE_UTILITY_DETAIL_SCOPE_EXIT_HPP
#define VLINDE_UTILITY_DETAIL_SCOPE_EXIT_HPP

#include <utility>

namespace vlinde
{
namespace utility
{
namespace detail
{
enum class scope_guard_on_exit {};

template <typename Func>
::vlinde::utility::detail::scope_guard_impl<Func>
operator+(scope_guard_on_exit, Func&& func)
{
	return ::vlinde::utility::detail::scope_guard_impl<Func>(
	    std::forward<Func>(func));
}
}
}
}

#define CONCATENATE_IMPL(s1, s2) s1##s2
#define CONCATENATE(s1, s2) CONCATENATE_IMPL(s1, s2)

#ifndef __COUNTER__
#define ANONYMOUS_VARIABLE(str) CONCATENATE(str, __COUNTER__)
#else
#define ANONYMOUS_VARIABLE(str) CONCATENATE(str, __LINE__)
#endif

#define SCOPE_EXIT                                                     \
	auto ANONYMOUS_VARIABLE(SCOPE_EXIT_STATE) =                        \
	    ::vlinde::utility::detail::scope_guard_on_exit                 \
	{                                                                  \
	}                                                                  \
	+[&]()

#endif // VLINDE_UTILITY_DETAIL_SCOPE_EXIT_HPP
