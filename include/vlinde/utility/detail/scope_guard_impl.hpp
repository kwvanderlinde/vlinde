#ifndef VLINDE_UTILITY_DETAIL_SCOPE_GUARD_IMPL_HPP
#define VLINDE_UTILITY_DETAIL_SCOPE_GUARD_IMPL_HPP

#include <utility>

namespace vlinde
{
namespace utility
{
namespace detail
{
template <typename Func>
class scope_guard_impl
{
	Func func_;
	bool run_;

public:
	scope_guard_impl() = delete;
	scope_guard_impl(scope_guard_impl const&) = delete;
	scope_guard_impl& operator=(scope_guard_impl const&) = delete;

	scope_guard_impl(scope_guard_impl&& other)
	: func_(::std::move(other.func_)), run_(other.run_)
	{
		other.dismiss();
	}

	scope_guard_impl& operator=(scope_guard_impl&&) = default;

	explicit scope_guard_impl(Func func, bool run = true)
	: func_(::std::move(func)), run_(run)
	{
	}

	~scope_guard_impl()
	{
		if (run_)
			func_();
	}

	void dismiss() noexcept
	{
		run_ = false;
	}

	friend void swap(scope_guard_impl& first, scope_guard_impl& second)
	{
		swap(first.func_, second.func_);
		swap(first.run_, second.run_);
	}
};
}
}
}

#endif // VLINDE_UTILITY_SCOPE_GUARD_IMPL_HPP
