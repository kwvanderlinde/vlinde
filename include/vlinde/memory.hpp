#ifndef VLINDE_MEMORY
#define VLINDE_MEMORY

namespace vlinde
{
/** Some additions to the C++ standard `<memory>` header.
  */
namespace memory
{
}
}

#include "memory/aligned_allocator.hpp"
#include "memory/allocator_concepts.hpp"
#include "memory/allocator_traits.hpp"
#include "memory/block.hpp"
#include "memory/default_allocator.hpp"
#include "memory/make_unique.hpp"
#include "memory/typed_allocator.hpp"
#include "memory/typed_memory.hpp"
#include "memory/utility.hpp"

#endif // VLINDE_MEMORY
