#ifndef VLINDE_FUNCTIONAL_EQUALS_HPP
#define VLINDE_FUNCTIONAL_EQUALS_HPP

#include "detail/equals_functor.hpp"

namespace vlinde
{
namespace functional
{
template <typename T>
auto equals(T&& t)
  -> detail::equals_functor<typename ::std::remove_const<
          typename ::std::remove_reference<T>::type>::type>
{
	return detail::equals_functor<T>(::std::forward<T>(t));
}
}
}

#endif // VLINDE_FUNCTIONAL_EQUALS_HPP
