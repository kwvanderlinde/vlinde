#ifndef VLINDE_FUNCTIONAL_DETAIL_EQUALS_FUNCTOR_HPP
#define VLINDE_FUNCTIONAL_DETAIL_EQUALS_FUNCTOR_HPP

#include <functional>

namespace vlinde
{
namespace functional
{
namespace detail
{
template <typename T>
class equals_functor
{
	T m_value;

public:
	equals_functor() = delete;

	explicit equals_functor(T const& value) : m_value(value)
	{
	}

	template <typename U>
	bool operator()(U const& other) const
	{
		return m_value == other;
	}
};

template <typename T>
class equals_functor<::std::reference_wrapper<T>>
{
	::std::reference_wrapper<T> m_value_ref;

public:
	explicit equals_functor(
	    ::std::reference_wrapper<T> const& value_ref)
	: m_value_ref(value_ref)
	{
	}

	template <typename U>
	bool operator()(U const& other) const
	{
		return m_value_ref.get() == other;
	}
};
}
}
}

#endif // VLINDE_FUNCTIONAL_DETAIL_EQUALS_FUNCTOR_HPP
